<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', 'LoginController@login_View');
Route::get('/logout', 'LoginController@logout');
Route::post('/login', 'LoginController@check_login');

Route::get('/absensi', 'LoginController@absensi_check');
Route::post('/absensi', 'LoginController@submit_absen');

Route::group(['middleware' => 'authlogin'], function () {
    Route::get('/view_inventory/{id}', 'AdminController@view_inventory');

    Route::group(['middleware' => 'check_role:admin,teknisi,admin_assist'], function () {
        Route::prefix('Spbu')->group(function () {
            Route::get('/update_wo/{id}', 'TeknisiController@update_data');
            Route::post('/update_wo/{id}', 'TeknisiController@submit_update_dt');
        });
    });

    Route::group(['middleware' => 'check_role:admin,admin_assist'], function () {
        Route::get('/get/table/{tpe}', 'AdminController@show_table_ajax');
    });

    Route::prefix('Spbu')->group(function () {
        Route::group(['middleware' => 'check_role:admin,admin_assist,teknisi'], function () {
            //input data biasa
            Route::get('{id}', 'AdminController@input_data');
            Route::post('{id}', 'AdminController@save_data');
            //input data perangkat rusak
            Route::get('perangkat_rusak/input', 'TeknisiController@input_broken_part');
            Route::post('perangkat_rusak/input', 'TeknisiController@save_broken_part');
            //delete
            Route::get('/delete_wo/{tiket}/{id}', 'AdminController@delete_data');
        });
        Route::get('/download_BA/broken_part/{id}', 'TeknisiController@download_BA_broken_part');
    });

    Route::prefix('repairing')->group(function () {
        Route::group(['middleware' => 'check_role:admin,admin_assist,teknisi'], function () {
            //input data biasa
            Route::get('{id}', 'AdminController@repair_input_data');
            Route::post('{id}', 'AdminController@repair_save_data');

            Route::get('/delete_wo/{tiket}/{id}', 'AdminController@delete_data');
        });
    });

    Route::get('/brokenpart/get_tipe', 'TeknisiController@get_tipe_barang');

    Route::get('/', function () {
        return redirect("/home");
    });

    Route::get('/home', 'AdminController@show_dash');
    Route::get('/dashboard/broken_part', 'AdminController@dash_broken_part');

    Route::get('/input/{val}/search', 'AdminController@alamat_search');
    // Inventori
    Route::get('/list/Inventory', 'AdminController@show_inventory_list');

    Route::get('/update_inventory/{id}', 'AdminController@edit_inventory');
    Route::post('/update_inventory/{id}', 'AdminController@update_inventory');

    Route::prefix('Spbu')->group(function () {
        //update data perangkat rusak
        Route::get('update_perangkatRusk/{id}', 'TeknisiController@edit_broken_part');
        Route::post('update_perangkatRusk/{id}', 'TeknisiController@update_broken_part');
    });

    //download
    Route::get('/list/download', 'AdminController@list_download');
    Route::post('/list/download', 'AdminController@submit_download');

    //Admin
    Route::group(['middleware' => 'admin'], function () {
        //alamat
        Route::get('/list/alamat', 'AdminController@show_alamat_list');
        Route::get('/input/alamat', 'AdminController@input_alamat');
        Route::post('/input/alamat', 'AdminController@save_alamat');

        Route::get('/edit/alamat/{id}', 'AdminController@edit_alamat');
        Route::post('/edit/alamat/{id}', 'AdminController@update_alamat');
        //download
        Route::get('/excel/download/{m1}/{m2}', 'AdminController@download');
        //matrix
        Route::get('/matrix', 'AdminController@show_matrix');
        Route::get('/grab/{data}/graph', 'AdminController@get_graph_data');
        //ambil sdms
        // Route::get('/sdms/dsh', 'AdminController@dashboard_sdms');
        // Route::get('/sdms/table', 'AdminController@table_sdms');
        // Route::get('/sdms_matrix/data/{id}', 'AdminController@sdms_matrix_list');
        // Route::get('/sdms/table/detail', 'AdminController@table_sdms_dtl');
        //absen
        Route::get('/list/absen', 'AdminController@list_absen');
        Route::get('/verify/absen/{id}', 'AdminController@verify_absen');
        Route::get('/why_reject/{id}', 'AdminController@form_reject_absen');
        Route::post('/why_reject/{id}', 'AdminController@submit_reject_Absen');

        //TEKNISI
        Route::get('/list/teknisi', 'AdminController@list_teknisi');

        // Route::get('/teknisi/input', 'AdminController@input_teknisi');
        // Route::post('/teknisi/input', 'AdminController@save_register');

        Route::get('/teknisi/update/{id}', 'AdminController@edit_teknisi');
        Route::post('/teknisi/update/{id}', 'AdminController@save_update_register');

        Route::get('/get/cek_user', 'AdminController@register_check');
    });

    Route::group(['middleware' => 'teknisi'], function () {
	//teknisi
        Route::get('/teknisi/table', 'TeknisiController@get_table');
    });
});
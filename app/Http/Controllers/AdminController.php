<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\TeknisiModel;
use App\Http\Controllers\TeknisiController;
use App\Http\Controllers\LoginController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set("Asia/Makassar");
class AdminController extends Controller
{
  public $foto_inventory = [
    'UPS' => [
      'Tipe' => [
        'Ablerex',
        'Schneider'
      ],
      'Evidence' => 0
    ],
    'PC_Pos' => [
      'Tipe' => [
        'Bestens',
        'Nec'
      ],
      'Evidence' => 0
    ],
    'Monitor' => [
      'Tipe' => [
        'Bestens',
        'Nec'
      ],
      'Evidence' => 0
    ],
    'FCC' => [
      'Tipe' => [
        'FCC'
      ],
      'Evidence' => 0
    ],
    'Router' => [
      'Tipe' => [
        'Cisco'
      ],
      'Evidence' => 0
    ],
    'Switch' => [
      'Tipe' => [
        'Cisco'
      ],
      'Evidence' => 0
    ],
    'Modem' => [
      'Tipe' => [
        'Rebustel',
        'Astinet',
        'Mangosky',
      ],
      'Series' => [
        'Alcatel',
        'Zte',
        'Huawei'
      ],
      'Evidence' => 0
    ],
    'FDM' => [
      'Tipe' => [
        'FDM 192-Active(Sanki)',
        'FDM 082-Passive(Tatsuno,SOMO,ENE,Prime)',
        'FDM-071-Active(Gilbarco,Wayne)',
        'FDM-104-Active(TOKHEIM)',
        'FDM-236-Active(Premier)',
      ],
      'Evidence' => 3
    ],
    'Docking_EDC' => [
      'Tipe' => [
        'EDC A920',
        'EDC Z90',
      ],
      'Evidence' => 10
    ],
    'EDC' => [
      'Tipe' => [
        'EDC A920',
        'EDC Z90',
      ],
      'Evidence' => 10
    ],
    'ATG_Consule' => [
      'Tipe' => [
        'Windbell',
        'Veeder Root',
      ],
      'Evidence' => 0
    ],
    'Probe' => [
      'Tipe' => [
        'Windbell',
        'Veeder Root',
      ],
      'Evidence' => 9
    ],
    'Totem_SPBU' => [
      'Tipe' => [
        'Default',
      ],
      'Evidence' => 0
    ],
    'Rak_IT' => [
      'Tipe' => [
        'Default',
      ],
      'Evidence' => 0
    ],
  ];

  public $foto_repairing = ['Foto_SN_Perangkat', 'label_photo_2', 'label_photo_3', 'label_photo_4', 'label_photo_5'];


  public function show_dash(Request $req)
  {
    $sess = session('auth')->spbu_level;
    if (in_array($sess, [1, 3, 4])) {

      $session = session('auth');

      $get_data = AdminModel::find_absen_u($session->id_user);

      if ($sess == 4 && (!$get_data || $get_data && $get_data->approval != 1) )
      {
        return redirect('/absensi');
      }

      if($req->all()){
        $prev_data = $req->all();
      }else{
        $prev_data = '';
      }
      $table_kerja = $table_act = [];
        if($req->jenis_g){
          $jenis_g = "'" . implode("','", $req->jenis_g) ."'";
        }else{
          $jenis_g = 'all';
        }

        if($req->status){
          $status = implode(',', $req->status);
        }else{
          $status = 'all';
        }

        if($req->daterange){
          $raw_date = explode(' - ', $req->daterange);
          $date1 = str_replace('/', '-', $raw_date[0]);
          $date2 = str_replace('/', '-', $raw_date[1]);
        }else{
          $date1 = date('Y-m-d');
          $date2 = date('Y-m-d');
        }
      $data = AdminModel::table_dashboard($jenis_g, $status, $date1, $date2, $req->job);
        foreach ($data as $key => $dat) {
          $id_spbu = explode(', ', $dat->spbu_id);
          $hari = explode(', ', $dat->hari);
          $stts = explode(', ', $dat->stts);
          $jenis = explode(', ', $dat->jenis);
          $id_disptc = explode(', ', $dat->id_dispt);
          $table_kerja[$dat->kota][$dat->teknisi] = $dat;
          foreach ($id_spbu as $key_s => $i_s) {
            $table_kerja[$dat->kota][$dat->teknisi]->data[$key_s] = (object) ['id_spbu' => $i_s, 'id_dis' => $id_disptc[$key_s], 'hari' => $hari[$key_s], 'status' => $stts[$key_s], 'jenis' => $jenis[$key_s]];
          }
        }
      $table_act = AdminModel::table_act($jenis_g, $status, $date1, $date2);

      return view('Admin.dashboard', compact('table_kerja', 'table_act', 'prev_data'));
    } elseif ($sess == 2) {
      $session = session('auth');

      $get_data = AdminModel::find_absen_u($session->id_user);

      if(!$get_data || $get_data && $get_data->approval != 1){
        return redirect('/absensi');
      }else{
        return view('Teknisi.dashboard');
      }
    } else {
      dd('wait error');
    }
  }

  public function show_dashboard_broken(Request $req)
  {
    if($req->all()){
      $prev_data = $req->all();
    }else{
      $prev_data = '';
    }
    $table_kerja = $table_act = [];
      if($req->jenis_g){
        $jenis_g = "'" . implode("','", $req->jenis_g) ."'";
      }else{
        $jenis_g = 'all';
      }

      if($req->status){
        $status = implode(',', $req->status);
      }else{
        $status = 'all';
      }

      if($req->daterange){
        $raw_date = explode(' - ', $req->daterange);
        $date1 = str_replace('/', '-', $raw_date[0]);
        $date2 = str_replace('/', '-', $raw_date[1]);
      }else{
        $date1 = date('Y-m-d');
        $date2 = date('Y-m-d');
      }
    $data = AdminModel::table_dashboard($jenis_g, $status, $date1, $date2, $req->job ?: 'all');
      foreach ($data as $key => $dat) {
        $id_spbu = explode(', ', $dat->spbu_id);
        $hari = explode(', ', $dat->hari);
        $stts = explode(', ', $dat->stts);
        $id_disptc = explode(', ', $dat->id_dispt);
        $table_kerja[$dat->kota][$dat->teknisi] = $dat;
        foreach ($id_spbu as $key_s => $i_s) {
          $table_kerja[$dat->kota][$dat->teknisi]->data[$key_s] = (object) ['id_spbu' => $i_s, 'id_dis' => $id_disptc[$key_s], 'hari' => $hari[$key_s], 'status' => $stts[$key_s]];
        }
      }
    $table_act = AdminModel::table_act($jenis_g, $status, $date1, $date2);
    return view('Admin.dashboard', compact('table_kerja', 'table_act', 'prev_data'));
  }

  public function show_matrix()
  {
    return view('Report.matrix');
  }

  public function download($m1, $m2)
  {
    $main_data = AdminModel::show_data($m1, $m2);
    return Excel::download(new ExcelExport($main_data, 'laporan_idk'), 'Ini Excel nya Mas.xlsx');
  }

  public function show_alamat_list()
  {
    $data = AdminModel::show_list_alamat();
    return view('Admin.alamat', compact('data'));
  }

  public function update_alamat(Request $req, $id)
  {
    AdminModel::save_alamat($req, $id, 'update');
    return redirect('/list/alamat')->with('alerts', [['type' => 'success', 'text' => "Alamat Berhasil Disimpan!"]]);
  }

  public function edit_alamat($id)
  {
    $data = AdminModel::show_alamat_detail($id, 'data');
    $status = AdminModel::show_alamat_detail($id, 'status');
    $type = AdminModel::show_alamat_detail($id, 'type');
    $kota = AdminModel::get_kota();
    return view('Admin.edit_alamat', compact('data', 'status', 'type', 'kota'));
  }

  public function save_alamat(Request $req)
  {
    AdminModel::save_alamat($req, 0, 'insert');
    return redirect('/list/alamat')->with('alerts', [['type' => 'success', 'text' => "Alamat Berhasil Disimpan!"]]);
  }

  public function input_alamat()
  {
    $status = AdminModel::show_alamat_detail(0, 'status');
    $type = AdminModel::show_alamat_detail(0, 'type');
    $kota = AdminModel::get_kota();
    $data = [];
    return view('Admin.edit_alamat', compact('data', 'status', 'type', 'kota'));
  }

  public function show_table_ajax(Request $req, $type)
  {
    $TeknisiController = new TeknisiController();
    if ($type == 'detail_act') {
      $prime = AdminModel::table_act_detail($req);
      return \Response::json($prime);
    } elseif ($type == 'get_tekn_det') {
      $data = AdminModel::show_Single_id_data($req->id);
      $photodispatch = $TeknisiController->photodispatch;
      $new_chk= [];
      $checklist = $TeknisiController->checklist;
      $chk = array_chunk($checklist, 3, true);
      foreach($chk as $key_p => $dt){
        foreach($dt as $key => $d){
          foreach($d as $keyc => $new_d){
            $path = "/upload2/spbu/$req->id/chckbox/".$new_d['nama'];
            $th   = "$path-th.jpg";
            if (file_exists(public_path().$th)){
              $new_chk[$key_p][$key][$keyc] = $new_d;
            }
          }
        }
      }
      $detail_tim = TeknisiModel::show_detail_tim($req->id);
      return view('Admin.detail_inputTiket_ajax', ['id' => $req->id, 'chk' => $new_chk], compact('data', 'photodispatch', 'detail_tim'));
    } elseif ($type == 'get_table_broken') {
      $data = TeknisiModel::show_Single_id_data_broken($req->id);
      // dd($data);
      $part = $TeknisiController->part;
      return view('Admin.detail_inputTiketRus_ajax', ['id' => $req->id], compact('data', 'part'));
    }
  }

  public function input_data($id)
  {
    $sess = session('auth')->spbu_level;
    $data = AdminModel::show_Single_id_data($id);
    return view('Admin.input', ['data' => $data], compact('id'));
  }

  public function save_data(Request $req, $id)
  {
    AdminModel::save_dispatch($req, $id);
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Data Berhasil Disimpan!"]]);
  }

  public function alamat_search(Request $req, $jenis)
  {
    $search = strtolower($req->searchTerm);
    $data = [];

    if ($jenis == 'alamat_stts') {
      $id = strtolower($req->id_spbu);
      $sto = strtolower($req->sto);
      $check_data = AdminModel::alamatstts_search($id);
      $data = ['alamat' => $check_data->alamat, 'status' => $check_data->status, 'sto' => $check_data->kota, 'naker' => $check_data->naker, 'nama' => $check_data->nama];
    } elseif ($jenis == 'sto') {
      $check_data = AdminModel::sto_search($search);
      foreach ($check_data as $row) {
        $data[] = array("id" => $row->kota, "text" => $row->kota);
      }
    } elseif ($jenis == 'spbu_id') {
      $id = strtolower($req->value);
      $check_data = AdminModel::id_spbu_search($search, $id);
      foreach ($check_data as $row) {
        $data[] = array("id" => $row->id_spbu, "text" => $row->id_spbu);
      }
    } elseif ($jenis == 'teknisi') {
      $check_data = AdminModel::teknisi_search($search);
      foreach ($check_data as $row) {
        $data[] = array("id" => $row->id_user, "text" => $row->nama . ' (' . $row->id_user . ')');
      }
    } elseif ($jenis == 'teknisi_repair') {
      $check_data = AdminModel::teknisi_search($search, 'all');
      foreach ($check_data as $row) {
        $data[] = array("id" => $row->id_user, "text" => $row->nama . ' (' . $row->id_user . ')');
      }
    }
    return \Response::json($data);
  }

  public function get_graph_data(Request $req, $data)
  {
    if ($data == 'kenaikan') {
      $data = AdminModel::get_data_graph($req);
    } elseif ($data == 'kerajinan') {
      $data = AdminModel::get_rajin_graph($req);
    }
    return \Response::json($data);
  }

  public function dashboard_sdms()
  {
    $data_chart = exec("cd /srv/htdocs/puppet_rendy;nodejs grab_spbu.js off chart");
    $chart = json_decode($data_chart);
    dd($chart);
    // $data_per_canvas = exec("cd /srv/htdocs/puppet_rendy;nodejs grab_spbu.js off data EDC");
    // $data_canvas = json_decode($data_per_canvas);
    // foreach ($chart as $d) {
    //     unset($d->head[0]);
    // }
    return view('Report.sdms_matrix', compact('chart'));
  }

  public function sdms_matrix_list(Request $req)
  {
    $data_per_canvas = exec("cd /srv/htdocs/puppet_rendy;nodejs grab_spbu.js off data $req->id");
    $data_canvas = json_decode($data_per_canvas);
    if (!empty($data_canvas)) {
      foreach ($data_canvas as $d) {
        unset($d->head[0]);
      }
      $data = $data_canvas[0];
    } else {
      $data = null;
    }
    return view('Report.table_dsh_sdms', compact('data'));
  }

  public function table_sdms(Request $req)
  {
    if (empty($req->datepicker)) {
      $tgl = date('Y-m-d');
    } else {
      $tgl = $req->datepicker;
    }
    $data = exec("cd /srv/htdocs/puppet_rendy;nodejs grab_spbu.js perform $tgl");
    $table = json_decode($data);
    return view('Report.sdms_perform_witel', ['table' => $table[0]]);
  }

  public function table_sdms_dtl(Request $req)
  {
    // dd($req->all());
    $tgl = date('Y-m-d');
    if (empty($req->selectih)) {
      $jenis = 'edc';
    } else {
      $jenis = $req->selectih;
    }
    $data = exec("cd /srv/htdocs/puppet_rendy;nodejs grab_spbu.js detail_platform $tgl $jenis");
    $table = json_decode($data);
    $alamat = AdminModel::show_list_alamat();
    $array = ['EDC', 'Pump/Nozzle', 'SLA'];
    if (!empty($table)) {
      foreach ($table as $d) {
        if (in_array($d->nama, $array)) {
          unset($d->head[2]);
        }
      }
      foreach ($table as $k => $d) {
        $final_data['id'] = $d->id;
        $final_data['nama'] = $d->nama;
        foreach ($d->data as $k_c => $d_c) {
          $tes = [];
          foreach ($alamat as $key => $value) {
            $id_spbu = str_replace('-', '', $value->id_spbu);
            $nama = '';
            if (end($d_c) == '-') {
              $val = 0;
            } else {
              $val = floatval(end($d_c));
            }
            if ($d->nama == 'Penalty') {
              if ($id_spbu == $d_c[0]) {
                $nama = "$value->nama ($value->naker)";
                $final_data['data'][$value->naker][$nama][$key]['SPBU'] = $d_c[0];
                $final_data['data'][$value->naker][$nama][$key]['Average'] = $val;
              }
            } else {
              if ($id_spbu == $d_c[1]) {
                $nama = "$value->nama ($value->naker)";
                $final_data['data'][$value->naker][$nama][$key]['SPBU'] = $d_c[1];
                $final_data['data'][$value->naker][$nama][$key]['Average'] = $val;
              }
            }
          }
        }
      }
      ksort($final_data['data']);
      $aver['id'] = $final_data['id'];
      $aver['nama'] = $final_data['nama'];
      foreach ($final_data['data'] as $key_d => $raw_d) {
        $avg = 0;
        $jml = 0;
        foreach ($raw_d as $key => $value) {
          foreach ($value as $key3 => $value3) {
            $aver['data'][$key]['nama'] = $key;
            $aver['data'][$key]['detail_data'] = $value;
            $aver['data'][$key]['avg'] = $avg += $value3['Average'];
            $aver['data'][$key]['jml'] = ++$jml;
            $aver['data'][$key]['rata'] = round($avg / $jml, 2);
            $bar[$key]['nama'] = $key;
            $bar[$key]['rata'] = round($avg / $jml, 2);
          }
        }
      }
    } else {
      $aver = [];
      $bar = [];
    }
    usort($bar, function ($a, $b) {
      return $b['rata'] <=> $a['rata'];
    });
    usort($aver['data'], function ($a, $b) {
      return $b['rata'] <=> $a['rata'];
    });
    return view('Report.detail_perform_sdms', compact('aver', 'bar'));
  }

  public function list_absen(){
    $data = AdminModel::get_list_absen('absen');
    // dd($data);
    return view('Report.list_verify_absen', compact('data'));
  }

  public function form_reject_absen($id){
    $LoginController = new LoginController();
    $foto = $LoginController->foto;
    return view('Admin.alasan_reject', compact('foto', 'id'));
  }

  public function verify_absen($id){
    AdminModel::verify_absen($id);
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Teknisi Sudah Di Absen!"]]);
  }

  public function submit_reject_Absen(Request $req, $id){
    AdminModel::reject_absen($id, $req);
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Teknisi Absen Berhasil Ditolak!"]]);
  }

  public function show_inventory_list(){
    $data = AdminModel::get_all_alamat();
    $id_user = session('auth')->id_user;
    $data_inven = [];
    $foto = $this->foto_inventory;
    // dd($foto)
    // foreach ($data as $key => $val) {
    //   if (in_array(session('auth')->spbu_level, [1, 4, 3])) {
    //     $data_inven[$val->kota][$val->nama_naker][] = $val;
    //   }else{
    //     if($id_user == $val->naker){
    //       $data_inven[$val->kota][$val->nama_naker][] = $val;
    //     }
    //   }
    // }
    // dd($data);
    foreach ($data as $key => $val) {
      if (in_array(session('auth')->spbu_level, [1, 2, 4, 3])) {
        $data_inven[$val->kota][$val->nama_naker][] = $val;
      }
    }
    return view('Report.list_inventory', compact('data_inven', 'foto'));
  }

  public function edit_inventory($id){
    $id_user = session('auth')->id_user;
    $foto = $this->foto_inventory;
    $log = AdminModel::log_inven($id);
    return view('Admin.update_inventory', compact('id', 'foto', 'log'));
  }

  public function view_inventory($id){
    $id_user = session('auth')->id_user;
    $foto = $this->foto_inventory;
    $log = AdminModel::log_inven($id);

    foreach($foto as $key => $raw_data)
    {
      for ($x = 0; $x <= $raw_data['Evidence']; $x++)
      {
        $path = "/upload2/spbu/inventory/$id/".$key."_".$x;
        $th   = "$path-th.jpg";
        if (file_exists(public_path().$th))
        {
          $rekap_f[$key]['Evidence'] = $raw_data['Evidence'] + 1;
        }
      }
    }
    return view('Report.view_inventory', compact('id', 'log'), ['foto'=> $rekap_f]);
  }

  public function update_inventory(Request $req, $id){
    AdminModel::update_inventory($id, $req);
    return redirect('/list/Inventory')->with('alerts', [['type' => 'success', 'text' => "Data Inventory Berhasil Diupdate!"]]);
  }

  public function dash_broken_part(Request $req)
  {
    if($req->all()){
      $prev_data = $req->all();
    }else{
      $prev_data = '';
    }
    $table_kerja = $table_act = [];
      if($req->lokasi){
        $lokasi = "'" . implode("','", $req->lokasi) ."'";
      }else{
        $lokasi = 'all';
      }

      if($req->change_part){
        $change_part = "'" . implode("','", $req->change_part) ."'";
      }else{
        $change_part = 'all';
      }

      if($req->jenis){
        $jenis = "'" . implode("','", $req->jenis) ."'";
      }else{
        $jenis = 'all';
      }

      if($req->daterange){
        $raw_date = explode(' - ', $req->daterange);
        $date1 = str_replace('/', '-', $raw_date[0]);
        $date2 = str_replace('/', '-', $raw_date[1]);
      }else{
        $date1 = date('Y-m-d');
        $date2 = date('Y-m-d');
      }
    $data = AdminModel::dash_broken_part($lokasi, $date1, $date2, $change_part, $jenis);
      foreach ($data as $key => $dat) {
        $id_spbu = explode(', ', $dat->spbu_id);
        $hari = explode(', ', $dat->hari);
        $stts = explode(', ', $dat->stts);
        $jenis = explode(', ', $dat->jenis);
        $id_disptc = explode(', ', $dat->id_dispt);
        $table_kerja[$dat->kota][$dat->teknisi] = $dat;
        foreach ($id_spbu as $key_s => $i_s) {
          $table_kerja[$dat->kota][$dat->teknisi]->data[$key_s] = (object) ['id_spbu' => $i_s, 'id_dis' => $id_disptc[$key_s], 'hari' => $hari[$key_s], 'status' => $stts[$key_s], 'jenis' => $jenis[$key_s]];
        }
      }
      // dd($table_kerja);
    return view('Report.dashboard_brokenPart', compact('table_kerja', 'prev_data'));
  }

  public function list_download()
  {
    return view('Admin.download_excel');
  }

  public function submit_download(Request $req)
  {
    $raw_date = explode(' - ', $req->daterange);
    $date1 = str_replace('/', '-', $raw_date[0]);
    $date2 = str_replace('/', '-', $raw_date[1]);
    $content = [];
    $data= AdminModel::data_download($req->jenis, $date1, $date2);
    if ($req->jenis == 'Pengerjaan') {
      foreach($data as $key => $d){
        $diff = 'Belum Close';
        if(!empty($d->close) || $d->close <> ''){
          $t1 = date_create($d->open);
          $t2 = date_create($d->close);
          $diff = date_diff($t2, $t1);
          $diff = $diff->format("%d Hari %h Jam %i Menit");
        }

        $content[$key]['no'] = ++$key;
        $content[$key]['no_tiket'] = $d->no_tiket;
        $content[$key]['jenis_g'] = $d->jenis_g;
        $content[$key]['open'] = $d->open;
        $content[$key]['ggn_open'] = $d->ggn_open;
        $content[$key]['close'] = $d->close;
        $content[$key]['durasi'] = $diff;
        $content[$key]['teknisi'] = $d->teknisi .'('. $d->nama .')';
        $content[$key]['koor'] = $d->koor;
        $content[$key]['catatan'] = $d->note;
        $content[$key]['tipe'] = $d->type;
        $content[$key]['id_spbu'] = $d->id_spbu;
        $content[$key]['alamat'] = $d->alamat;
        $content[$key]['teknisi_responsible'] = $d->naker_id .'('. $d->naker_nama .')';
        $content[$key]['yang_buat'] = $d->created_by;
      }

      $head = [
        'No',
        'Nomor Tiket',
        'Jenis Gangguan',
        'Tanggal Open',
        'Detail Gangguan',
        'Tanggal Close',
        'Durasi',
        'Teknisi',
        'Lokasi',
        'Catatan',
        'Tipe',
        'ID SPBU',
        'Alamat',
        'Teknisi Sektor',
        'Dibuat Oleh'
      ];

    $judul = 'Laporan Pekerjaan '.$date1.' sampai '.$date2;
    }elseif ($req->jenis == 'Perangkat Rusak') {
      foreach($data as $key => $d){
        $diff = 'Belum Close';
        if(!empty($d->close) || $d->close <> ''){
          $t1 = date_create($d->open);
          $t2 = date_create($d->close);
          $diff = date_diff($t2, $t1);
          $diff = $diff->format("%d Hari %h Jam %i Menit");
        }

        $content[$key]['no'] = ++$key;
        $content[$key]['no_tiket'] = $d->no_tiket;
        $content[$key]['open'] = $d->open;
        $content[$key]['close'] = $d->close;
        $content[$key]['durasi'] = $diff;
        $content[$key]['broken_part'] = $d->broken_part;
        $content[$key]['jenis_part'] = $d->jenis_perangkat;
        $content[$key]['change_part'] = $d->change_part;
        $content[$key]['alamat_relok'] = $d->alamat_relok;
        $content[$key]['status_perang_relok'] = $d->status_perangkat_relok;
        $content[$key]['teknisi'] = $d->teknisi .'('. $d->nama .')';
        $content[$key]['koor'] = $d->lokasi;
        $content[$key]['catatan'] = $d->note;
        $content[$key]['tipe'] = $d->type;
        $content[$key]['id_spbu'] = $d->id_spbu;
        $content[$key]['alamat'] = $d->alamat;
        $content[$key]['teknisi_responsible'] = $d->naker_id .'('. $d->naker_nama .')';
        $content[$key]['yang_buat'] = $d->created_by;
      }

      $head = [
        'No',
        'Nomor Tiket',
        'Tanggal Open',
        'Tanggal Close',
        'Durasi',
        'Perangkat Rusak',
        'Jenis Perangkat Rusak',
        'Status Pergantian Perangkat',
        'Relokasi ID SPBU',
        'Status Perangkat Relokasi',
        'Teknisi',
        'Lokasi',
        'Catatan',
        'Tipe',
        'ID SPBU',
        'Alamat',
        'Teknisi Sektor',
        'Dibuat Oleh'
      ];

      $judul = 'Laporan Perangkat Rusak '.$date1.' sampai '.$date2;
    }
    // dd($content);
    return \Excel::download(new ExcelExport($content, 'laporan_biasa', $head), $judul.'.xlsx');
  }

  public function list_teknisi()
  {
    $data = AdminModel::list_teknisi();
    return view('Report.list_teknisi', compact('data'));
  }

  // public function input_teknisi()
  // {
  //   $mitra = AdminModel::list_mitra();
  //   return view('Admin.register', ['data' => null], compact('mitra'));
  // }

  // public function edit_teknisi($id)
  // {
  //   $mitra = AdminModel::list_mitra();
  //   $data = AdminModel::cek_user($id);
  //   return view('Admin.register', compact('mitra', 'data'));
  // }

  public function register_check(Request $req)
	{
		return \Response::json(AdminModel::cek_user($req->data, $req->jenis));
	}

  // public function save_update_register(Request $req, $id)
	// {
	// 	AdminModel::update_user($req, $id);
	// 	return back()->with('alerts', [['type' => 'success', 'text' => "Berhasil Di Update!"]]);
	// }

	// public function save_register(Request $req)
	// {
	// 	AdminModel::save_user($req);
	// 	return back()->with('alerts', [['type' => 'success', 'text' => "Berhasil Ditambah!"]]);
	// }

  public function delete_data($jenis, $id){
    AdminModel::delete_data($jenis, $id);
    return back()->with('alerts', [['type' => 'success', 'text' => "Pekerjaan berhasil dihapus!"]]);
  }

	public function repair_input_data($id)
	{
    $sess = session('auth')->spbu_level;
    $data = AdminModel::show_Single_id_data($id);
    $witel = AdminModel::get_all_witel();
    $foto = $this->foto_repairing;
    return view('Admin.repairing_input', ['data' => $data], compact('id', 'foto', 'witel') );
	}

	public function repair_save_data(Request $req)
	{
		TeknisiModel::save_repair_item($req);
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Pekerjaan Perangkat Rusak Berhasil dibuat!"]]);
	}

}

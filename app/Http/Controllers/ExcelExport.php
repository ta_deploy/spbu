<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use DB;
class ExcelExport implements ShouldAutoSize, WithEvents, WithHeadings, FromCollection
{
    use Exportable;

    public function __construct($data, $jenis, $head = null){
        $this->data = $data;
        $this->jenis = $jenis;
        $this->head = $head;
    }

    public function collection()
    {
        $data = [];
        if($this->jenis == 'laporan_biasa'){
            $data = $this->data;
        }
        return collect($data);
    }

    public function headings(): array
    {
        $head = [];
        // dd('tes2');
        if($this->jenis == 'laporan_biasa')
        {
            $head[] = $this->head;
        }
        return $head;
    }

    public function convert_month($date){
        if($date == null){
            return null;
        }
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $result = date('d m Y', strtotime($date));
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];

        return $hasilnya;
    }

    public function time_calculate_elapsed($time1, $time2){
        $date1 = strtotime($time1); 
        if($time2 == null){
            return null;
        }
        $date2 = strtotime($time2);  

        $diff = abs($date2 - $date1);  
        $years = floor($diff / (365*60*60*24));  
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
        $days = floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24)); 
        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
        $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);  
        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
        $result = ($hours > 9 ? $hours : '0'.$hours).':'.($minutes > 9 ? $minutes : '0'.$minutes);
        return $result;
    }

    public function registerEvents(): array
    {   
        $point_event = [];
        if($this->jenis != 'laporan_biasa'){
            $point_event = [
                BeforeExport::class  => function(BeforeExport $event) {
                    $border_Style = [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                            ],
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        ]
                    ];
    
                    $reader = new Xlsx();
                    \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                    $spreadSheet = $reader->load(public_path().'/template_doc/template_excel.xlsx');
                    $nomor = 1;
                    $ss =$spreadSheet->setActiveSheetIndex(0);
                    foreach($this->data as $key => $data){
                        $ss->getCell("A".(6+$key))->setValue($nomor++);
                        $ss->getCell("B".(6+$key))->setValue($data->no_tiket);
                        $ss->getCell("C".(6+$key))->setValue($data->id_spbu);
                        $ss->getCell("D".(6+$key))->setValue($data->alamat);
                        $ss->getCell("E".(6+$key))->setValue($data->type);
                        $ss->getCell("F".(6+$key))->setValue($data->jenis_g);
                        $ss->getCell("G".(6+$key))->setValue($this->convert_month($data->open));
                        $ss->getCell("H".(6+$key))->setValue($this->convert_month($data->close));
                        $ss->getCell("I".(6+$key))->setValue($this->time_calculate_elapsed($data->open, $data->close));
                        $ss->getCell("J".(6+$key))->setValue($data->nama);
                        $ss->getCell("K".(6+$key))->setValue($data->act == 1 ? 'Vcall' : "Kunjungan");
                        $ss->getCell("L".(6+$key))->setValue($data->note);
                        $ss->getCell("M".(6+$key))->setValue($data->note_rusak != null ? 'Nok' : '');
                        $ss->getCell("N".(6+$key))->setValue($data->note_rusak != null ? 'Nok' : '');
                        $ss->getCell("O".(6+$key))->setValue($data->note_rusak);
                        $ss->getStyle("A6:N".(6+$key))->applyFromArray($border_Style);
                    }
                    foreach(range('A','N') as $columnID) {
                        $ss->getColumnDimension($columnID)
                        ->setAutoSize(true);
                    }
                    $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="Excel SPBU.xlsx"');
                    $writer->save('php://output');
                    die;
                }
            ];
        }
        return $point_event;
    }

}
<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\LoginModel;

date_default_timezone_set("Asia/Makassar");
class LoginController extends Controller
{
    public $foto = ['Foto_Kehadiran', 'Foto_My_Tracker', 'Foto_My_SuperApps'];

    public function login_View()
    {
        return view('login');
    }

    public function check_login(Request $req, LoginModel $Loginmodel)
    {
        $user = $req->input('username');
        $pass = $req->input('password');
        $result = $Loginmodel->login($user, $pass);
        if (count($result)>0) {
            $session = $req->session();
            $session->put('auth', $result[0]);
            $this->ensureLocalUserHasRememberToken($result[0], $Loginmodel);
            return $this->successResponse($result[0]->psb_remember_token);
        } else {
            return redirect('/login')->with('alerts', [
                ['type' => 'danger', 'text' => 'Login Gagal']
            ]);
        }
    }

    public function logout()
    {
        Session::forget('auth');
        Session::forget('badges');
        return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
    }

    private function ensureLocalUserHasRememberToken($localUser, LoginModel $Loginmodel)
    {
        $token = $localUser->psb_remember_token;
        if (!$localUser->psb_remember_token) {
            $token = $this->generateRememberToken($localUser->id_user);
            $Loginmodel->remembertoke($localUser, $token);
            $localUser->psb_remember_token = $token;
        }

        return $token;
    }

    private function generateRememberToken($nik)
    {
        return md5($nik . microtime());
    }

    private function successResponse($rememberToken)
    {
        if (Session::has('auth-originalUrl')) {
            $url = Session::pull('auth-originalUrl');
        } else {
            $url = '/';
        }
        $response = redirect($url);
        if ($rememberToken) {
            $response->withCookie(cookie()->forever('presistent-token', $rememberToken));
        }
        return $response;
    }

    public function absensi_check()
    {
        $session = session('auth');
        if(in_array(@$session->spbu_level, [2, 4])){

            $get_data = AdminModel::find_absen_u($session->id_user);

            if(!$get_data || $get_data && $get_data->approval != 1)
            {
                $foto = $this->foto;
                $get_log = LoginModel::log_user($session->id_user);
                return view('absen', compact('session', 'foto', 'get_log'));
            }elseif($get_data && $get_data->approval == 1)
            {
                return redirect('/home')->with('alerts', [
                    ['type' => 'danger', 'text' => 'Anda Sudah Absen!']
                ]);
            }
        }else{
            return redirect('/login');
        }
    }

    public function submit_absen(Request $req){
        LoginModel::absen_teknsi($req);
        return back();
    }
}
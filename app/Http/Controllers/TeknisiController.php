<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ExcelExport;
use App\DA\TeknisiModel;
use Illuminate\Http\Request;

date_default_timezone_set("Asia/Makassar");
class TeknisiController extends Controller
{
  public $photodispatch = ['Empty_Photo_1', 'Empty_Photo_2', 'Empty_Photo_3', 'Empty_Photo_4', 'Empty_Photo_5', 'Empty_Photo_6', 'Empty_Photo_7', 'Empty_Photo_8', 'SN_Rusak', 'Pengganti', 'Cover SPBU', 'Berita_Acara'];

  public $checklist = [
    'Integrasi Grounding' => [
      ['id' => '1a', 'nama' => 'Ground Rod - Grounding Bar ACPDB'],
      ['id' => '1b', 'nama' => 'Grounding Bar ACPDB - Grounding Point Rak IT'],
      ['id' => '1c', 'nama' => 'Grounding Point Rak IT - ke Body: Router SDWan - Modem'],
      ['id' => '1d', 'nama' => 'Grounding Point Rak IT - ke Body: Switch'],
      ['id' => '1f', 'nama' => 'Grounding Point Rak IT - ke Body: PC POS'],
      ['id' => '1g', 'nama' => 'Grounding Point Rak IT - ke Body: UPS'],
      ['id' => '1h', 'nama' => 'Grounding Point Rak IT - ke Body: FDM Box'],
    ],
    'Instalasi FDM' => [
      ['id' => '2a', 'nama' => 'Grounding Point Rak IT - GND Kabel Screen Dispenser'],
      ['id' => '2b', 'nama' => 'GND Pin DB25 - GND output FDM (RS232 Atau RS485)'],
      ['id' => '2c', 'nama' => 'GND output FDM (RS232 Atau RS485) - Grounding Point FDM Box'],
      ['id' => '2d', 'nama' => 'GND PCB FDM V2 - Grounding Point FDM Box'],
      ['id' => '2e', 'nama' => 'Lubang box FDM'],
    ],
    'Instalasi Console ATG ' => [
      ['id' => '3a', 'nama' => 'Grounding Point Rak IT - Grounding Bar Console ATG'],
      ['id' => '3b', 'nama' => 'Grounding Bar Console ATG - GND Kabel Screen ATG, atau Grounding tangki pendam - GND Kabel Screen ATG'],
    ],
    'Grounding SPBU' => [
      ['id' => '4a', 'nama' => 'Grounding Rod ada'],
    ],
    'Proteksi Petir Internal' => [
      ['id' => '5a', 'nama' => 'Lightning current & surge arrester di ACPDB Telkom (Tipe 1)'],
      ['id' => '5b', 'nama' => 'Lightning current & surge arrester di ruang IT (Tipe 2)'],
    ],
    'Proteksi Petir Eksternal' => [
      ['id' => '6a', 'nama' => 'Area Gedung (Rak IT)'],
      ['id' => '6b', 'nama' => 'Area Tangki Pendam'],
      ['id' => '6c', 'nama' => 'Area Dispenser'],
      ['id' => '6d', 'nama' => 'Area yang dilewati Kabel Screen ATG'],
      ['id' => '6e', 'nama' => 'Area yang dilewati Kabel Screen Dispenser'],
    ],
  ];

  public $part = ['pergantian_perangkat', 'perangkat_rusak', 'perangkat_baru', 'berita_acara_kerusakan', 'berita_acara_perangkat_relokasi', 'pergantian_perangkat_relokasi', 'berita_acara_perangkat_rusak', 'berita_acara_pergantian_perangkat', 'perangkat_relokasi', 'berita_acara_pergantian_perangkat_relokasi'];

  public function get_table(Request $req)
  {
    $sess = session('auth')->id_user;
    $data = TeknisiModel::get_table_pekerjaan_ajax($sess, $req->type);
    return \Response::json($data);
  }

  public function input_data_sdms($spbu_id)
  {
    $photodispatch = $this->photodispatch;
    return view('Teknisi.input_sdms', compact('photodispatch', 'spbu_id'));
  }

  public function update_data($id){
    $chk = array_chunk($this->checklist, 3, true);
    $photodispatch = $this->photodispatch;
    $data = TeknisiModel::show_Single_id_data($id);
    $detail_tim = TeknisiModel::show_detail_tim($id);
    return view('Teknisi.update_wo', compact('id', 'data', 'chk', 'photodispatch', 'detail_tim') );
  }

  public function submit_update_dt(Request $req, $id){
    return TeknisiModel::update_dispatch($req, $id);
    dd('stop');
  }

  public function input_broken_part(){
    $part = $this->part;
    $data = [];
    $id = 'input';
    $AdminController = new AdminController();
    $broken_part = $AdminController->foto_inventory;
    $broken_part = array_keys($broken_part);
    return view('Teknisi.input_perangkatRusak', compact('data', 'part', 'id', 'broken_part'));
  }

  public function save_broken_part(Request $req){
    TeknisiModel::save_broken_part($req);
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Pekerjaan Perangkat Rusak Berhasil dibuat!"]]);
  }

  public function edit_broken_part($id){
      $part = $this->part;
      $data = TeknisiModel::show_Single_id_data_broken($id);
      $AdminController = new AdminController();
      $broken_part = $AdminController->foto_inventory;
      $broken_part = array_keys($broken_part);
      // dd($data);
      return view('Teknisi.input_perangkatRusak', compact('data', 'part', 'id', 'broken_part'));
  }

  public function get_tipe_barang(Request $req){
    $search = trim($req->data);
    $data = [];

    $check_data = TeknisiModel::get_detail_partbroken($search);

    foreach ($check_data as $row) {
      $data[] = array("id" => $row, "text" => $row);
    }

    return \Response::json($data);
  }

  public function update_broken_part(Request $req, $id){
    TeknisiModel::save_broken_part($req, $id);
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Pekerjaan Perangkat Rusak Berhasil diUpdate!"]]);
  }

  public function sdms_off_tknis(Request $req)
  {
    if (!empty($req->selectih)) {
      $data_per_canvas = exec("cd /srv/htdocs/puppet_rendy;nodejs grab_spbu.js off data $req->selectih");
      $data_canvas = json_decode($data_per_canvas);
      if (!empty($data_canvas)) {
        foreach ($data_canvas as $d) {
          unset($d->head[0]);
        }
        $raw_data = $data_canvas[0];
        $head = $raw_data->head;
        $nama = $raw_data->nama;
        $sess = session('auth')->id_user;
        foreach ($raw_data->head as $key_p => $value_p) {
          foreach ($raw_data->data as $key_c => $value_c) {
            foreach ($value_c as $key_c2 => $value_c2) {
              if ($key_c2 + 1 == $key_p) {
                $data_need[$key_c][$value_p] = $value_c2;
              }
            }
          }
        }
        $sess = session('auth')->id_user;
        $final_data = [];
        $get_data_alamat = AdminModel::spbu_search_by_naker($sess);
        foreach ($get_data_alamat as $key_p => $value_p) {
          $id_spbu = str_replace('-', '', $value_p->id_spbu);
          foreach ($data_need as $key_c => $value_c) {
            if ($value_c['Site'] == $id_spbu) {
              $final_data['data'][$key_c] = $value_c;
            }
          }
        }
        $final_data['head'] = $head;
        $final_data['nama'] = $nama;
      } else {
        $final_data = null;
      }
    } else {
      $final_data = null;
    }
    return view('Teknisi.sdms_dsh_pertim', compact('final_data'));
  }

  public function download_BA_broken_part($id)
  {
    $path = public_path() . '/upload2/spbu/broken_part/' . $id . '/BA/';
    if (file_exists($path)) {
        $files = preg_grep('~^File BA.*$~', scandir($path));
        $files = array_values($files);
        $real_path = $path.''.$files[0];
    }

    $headers = ['Content-Type: application/pdf'];

    return \Response::download($real_path, $files[0], $headers);

  }

}

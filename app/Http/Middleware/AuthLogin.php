<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthLogin
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    $rememberToken = $request->cookie('presistent-token');
    if ($rememberToken) {

      $SQL = 'SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.spbu_level, u.psb_remember_token, r.mainsector, u.maintenance_level
        FROM user u
        LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
        LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
        WHERE psb_remember_token = "' . $rememberToken . '"
        GROUP BY u.id_user
        ';
      $user = DB::select($SQL);
      if (count($user) > 0) {
        Session::put('auth', $user[0]);
        // $valid_user = ['1' => 'admin', '2' => 'teknisi', '3' => 'monitor', '4' => 'admin_assist'];
        // $level = $user[0]->spbu_level;

        // if(in_array($level, [2, 4]) && (date('Y-m-d', strtotime($user[0]->spbu_verify)) != date('Y-m-d') || date('Y-m-d', strtotime($user[0]->spbu_absen)) != date('Y-m-d') && !empty($user[0]->spbu_absen)) ){
        //   return redirect('/absensi');
        // }

        return $next($request);
      }
    }

    Session::put('auth-originalUrl', $request->fullUrl());
    if ($request->ajax()) {
      return response('UNAUTHORIZED', 401);
    } else {
      return redirect('login');
    }
  }
}

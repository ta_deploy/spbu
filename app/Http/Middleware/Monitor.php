<?php

namespace App\Http\Middleware;

use App\DA\AdminModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ReportController;
use DB;
use Closure;

class Monitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $rememberToken = $request->cookie('presistent-token');
      if ($rememberToken) {
        $id = Session('auth')->id_user;
        $SQL = "SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.spbu_level, u.psb_remember_token, r.mainsector, u.maintenance_level
        FROM user u
        LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
        LEFT JOIN (SELECT id_user FROM user WHERE id_user = 18940469) as user2 ON u.id_user = user2.id_user
        LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
        WHERE psb_remember_token = '$rememberToken' AND spbu_level = 3 OR user2.id_user = '$id'
        GROUP BY u.id_user
        ";

        $user = DB::select($SQL);

        $get_data = AdminModel::find_absen_u($id);

        if (count($user)>0 || $get_data && $get_data->approval == 1){
          return $next($request);
        } else {
          return redirect('/absensi');
        }
      }

      Session::put('auth-originalUrl', $request->fullUrl());
      if ($request->ajax()) {
        return response('UNAUTHORIZED', 401);
      } else {
        return redirect('login');
      }
    }
  }

<?php

namespace App\Http\Middleware;

use App\DA\AdminModel;
use Closure;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CheckRole
{
    public function handle($request, Closure $next, ...$roles)
    {
        $valid_user = ['1' => 'admin', '2' => 'teknisi', '3' => 'monitor', '4' => 'admin_assist'];
        $level = Session('auth')->spbu_level;
        // dd($roles, empty($roles), $level);
        if(empty($roles)){
            return redirect('login');
        }
        if (in_array($valid_user[$level], $roles)) {
            $session = session('auth');

            $get_data = AdminModel::find_absen_u($session->id_user);

            if(in_array($level, [2, 4]) && (!$get_data || $get_data && $get_data->approval != 1) ){
                return redirect('/absensi');
            }
            return $next($request);
        }

        Session::put('auth-originalUrl', $request->fullUrl());
        if ($request->ajax()) {
            return response('UNAUTHORIZED', 401);
        } else {
            Session::put('auth-originalUrl', '');
            return redirect('login');
        }
    }

}

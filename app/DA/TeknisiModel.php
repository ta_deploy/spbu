<?php

namespace App\DA;

use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\TeknisiController;
use App\Http\Controllers\AdminController;
use Telegram;

date_default_timezone_set("Asia/Makassar");

class TeknisiModel
{

  public $foto_inventory = [
    'UPS' => [
      'Tipe' => [
        'Ablerex',
        'Schneider'
      ],
      'Evidence' => 0
    ],
    'PC_Pos' => [
      'Tipe' => [
        'Bestens',
        'Nec'
      ],
      'Evidence' => 0
    ],
    'Monitor' => [
      'Tipe' => [
        'Bestens',
        'Nec'
      ],
      'Evidence' => 0
    ],
    'FCC' => [
      'Tipe' => [
        'FCC'
      ],
      'Evidence' => 0
    ],
    'Router' => [
      'Tipe' => [
        'Cisco'
      ],
      'Evidence' => 0
    ],
    'Switch' => [
      'Tipe' => [
        'Cisco'
      ],
      'Evidence' => 0
    ],
    'Modem' => [
      'Tipe' => [
        'Rebustel',
        'Astinet',
        'Mangosky',
      ],
      'Series' => [
        'Alcatel',
        'Zte',
        'Huawei'
      ],
      'Evidence' => 0
    ],
    'FDM' => [
      'Tipe' => [
        'FDM 192-Active(Sanki)',
        'FDM 082-Passive(Tatsuno,SOMO,ENE,Prime)',
        'FDM-071-Active(Gilbarco,Wayne)',
        'FDM-104-Active(TOKHEIM)',
        'FDM-236-Active(Premier)',
      ],
      'Evidence' => 3
    ],
    'EDC' => [
      'Tipe' => [
        'EDC A920',
        'EDC Z90',
      ],
      'Evidence' => 9
    ],
    'ATG_Consule' => [
      'Tipe' => [
        'Windbell',
        'Veeder Root',
      ],
      'Evidence' => 0
    ],
    'Probe' => [
      'Tipe' => [
        'Windbell',
        'Veeder Root',
      ],
      'Evidence' => 9
    ],
  ];

  public static function get_table_pekerjaan_ajax($id, $jenis)
  {
    return DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->select('sd.*', 'k.nama', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.id as id_adds')
      ->where([
        ['teknisi', $id],
        ['sd.active', 1],
        ['sd.stts', $jenis]
      ])
      ->get();
  }

  public static function show_Single_id_data($id)
  {
    return DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.status', 'sa.id as id_adds', 'sd.teknisi as id_user', 'k.nama')
      ->where([
        ['sd.id', $id],
        ['sd.active', 1]
      ])
      ->first();
  }

  public static function show_detail_tim($id)
  {
    return DB::table('spbu_detail_tim')->where('id_spbu_dispatch', $id)->get();
  }

  public static function show_Single_id_data_broken($id)
  {
    return DB::TABLE('spbu_perangkat_rusak as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('spbu_perangkat_rusak as spr', 'spr.id', '=', 'sd.parent')
      ->leftjoin('spbu_alamat as sas', 'spr.id_spbu_add', '=', 'sas.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->leftjoin('1_2_employee as k2', 'k.nik', '=', 'sa.naker')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sas.id_spbu as relok_spbu', 'sa.alamat', 'sa.kota', 'sa.status', 'sa.id as id_adds', 'sas.id as id_adds_relok', 'sa.naker as naker_id', 'k2.nama as naker_nama', 'sd.teknisi as id_user', 'k.nama')->where([
        ['sd.id', $id],
        ['sd.active', 1]
      ])
      ->first();
  }

  public static function collect_child_broken($id)
  {
    return DB::TABLE('spbu_perangkat_rusak as sd')
    ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
    ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.status', 'sa.id as id_adds', 'sd.teknisi as id_user', 'k.nama')->where([
      ['sd.parent', $id],
      ['sd.active', 1]
    ])
    ->get();
  }

  // public static function save_dispatch_sdms($req)
  // {
  //   DB::transaction(function () use ($req) {
  //     $session = session('auth');
  //     $check_alamat = DB::table('spbu_alamat')
  //       ->where('id_spbu', 'like', '%' . $req->id_spbu . '%')
  //       ->where('alamat', 'like', '%' . $req->id_alamat . '%')
  //       ->where('kota', 'like', '%' . $req->sto . '%')
  //       ->first();
  //     $that = new AdminModel();
  //     $sess = session('auth')->id_user;
  //     $check_teknisi = DB::table('user as u')
  //       ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
  //       ->select('u.id_user', 'k.nama')
  //       ->where('id_user', $sess)
  //       ->first();
  //     $no_tiket = 'INT-' . date('dH') . '' . $check_alamat->id . '-' . substr($req->id_spbu, 0, 2) . '-' . substr(strtotime("now"), 0, -4);
  //     $message = '';

  //     $detail = null;
  //     if ($req->act == 1) {
  //       $detail = $req->detail;
  //     }

  //     $id = DB::table('spbu_dispatch')->insertGetId([
  //       'id_spbu_add' => $check_alamat->id,
  //       'no_tiket' => $no_tiket,
  //       'jenis_g' => $req->jenis_g,
  //       'open' => date('Y-m-d H:i:s'),
  //       'teknisi' => $check_teknisi->id_user,
  //       'close' => date('Y-m-d H:i:s'),
  //       'act' => $req->act,
  //       'detail_knjgn' => $detail,
  //       'berita_ac_sel' => $req->berita_ac_sel,
  //       'note_rusak' => $req->note_rusak,
  //       'koor' => $req->koor,
  //       'note' => $req->note,
  //       'stts' => $req->stts,
  //       'updated_by' => $session->id_user,
  //     ]);
  //     $that->handleFileUpload($req, $id);

  //     $get_data = DB::table('spbu_dispatch')->where('id', $id)->first();

  //     if ($req->berita_ac == 1) {
  //       $BA = 'OK';
  //     } else {
  //       $BA = 'NOK';
  //     }

  //     if ($req->act == 1) {
  //       $act = 'Kunjungan';
  //     } else {
  //       $act = 'Vcall';
  //     }

  //     if ($req->stts == 1) {
  //       $stts = 'Open';
  //       $daeme = date('Y-m-d H:i:s');
  //     } elseif ($req->stts == 0) {
  //       $stts = 'Closed';
  //       $daeme = $get_data->close;
  //     } elseif ($req->stts == 3) {
  //       $stts = 'Pending';
  //       $daeme = $get_data->close;
  //     }

  //     $t1 = date_create($get_data->open);
  //     $t2 = date_create($daeme);
  //     $diff = date_diff($t2, $t1);

  //     $cari_nama = DB::table('1_2_employee')->where('nik', $session->id_user)->first();

  //     $message .= "Orderan dari NIK: " . $cari_nama->nama . '(' . $session->id_user . ')' . " \n";
  //     $message .= "Nomor Tiket: " . $no_tiket . " \n";
  //     $message .= "<b>ID SPBU</b> : $req->id_spbu \n";
  //     $message .= "<b>Tanggal Open</b> : " . $get_data->open . "\n";
  //     $message .= "<b>Tanggal Closed</b> : " . $daeme . "\n";
  //     $message .= "<b>Durasi</b> : " . $diff->format("%d Hari %h Jam %i Menit") . "\n";
  //     $message .= "======================== \n";
  //     $message .= "<b>Kota</b> : $req->sto\n";
  //     $message .= "<b>Jenis Gangguan</b> : $req->jenis_g\n";
  //     $message .= "<b>Catatan Gangguan</b> : $req->ggn_open\n";
  //     $message .= "<b>Teknisi Pengerjaan</b> : $req->teknisi\n";
  //     $message .= "<b>Action</b> : $act\n";
  //     $message .= "<b>Berita Acara</b> : $BA\n";
  //     if ($req->berita_ac == 0) {
  //       $message .= "<b>Catatan Kerusakan</b> : $req->note_rusak\n";
  //     }
  //     $message .= "<a href=\"https://www.google.com/maps/place/$req->koor\">$req->koor </a>\n";
  //     $message .= "<b>Catatan</b> : $req->note\n";
  //     $message .= "<b>Status</b> : $stts\n";
  //     if ($req->jenis_g == 'Grounding'){
  //       $message .= "Checklist Instalasi Grounding BA\n";
  //       foreach ($checklist as $name => $rdata) {
  //         $message .= "$name\n";
  //         foreach ($rdata as $data) {
  //           if (isset($req->$data['id'])) {
  //             $name = 'txt_' . $data['id'];
  //             $text = $req->$name;
  //             $chk = '✅';
  //           } else {
  //             $text = 'Tidak ada keterangan';
  //             $chk = '❌';
  //           }
  //           $nama = $data['nama'];
  //           $message .= "$nama : $chk\n";
  //           $message .= "Keterangan : $text\n";
  //         }
  //       }
  //     }

  //     $oddeven_chk = array_chunk($that->checklist, 7);
  //     foreach ($oddeven_chk as $rdata) {
  //       foreach ($rdata as $phototelegram_s) {
  //         for ($i = 0; $i < count($phototelegram_s); $i++) {
  //           $name = $phototelegram_s[$i]['nama'];
  //           $public = public_path() . '/upload/spbu/'.$id.'/chckbox/';
  //           $file = $public . "/" . $name . ".jpg";
  //           $photo_caption = str_replace('_', ' ', $name);
  //           if (file_exists($file)) {
  //             Telegram::sendPhoto([
  //               'chat_id' => '--1001152815513',
  //               // 'chat_id' => '-1001152815513' atau 519446576
  //               'caption' => "Label Checklist $photo_caption \n",
  //               'photo' => $file,
  //             ]);
  //           }
  //         }
  //       }
  //     }

  //     $oddeven = array_chunk($that->photodispatch, 7);
  //     foreach ($oddeven as $phototelegram_s) {
  //       for ($i = 0; $i < count($phototelegram_s); $i++) {
  //         $public = public_path() . '/upload/spbu/' . $id;
  //         $file = $public . "/" . $phototelegram_s[$i] . ".jpg";
  //         if (file_exists($file)) {
  //           $file_text = "$public/$phototelegram_s[$i]-catatan.txt";
  //           if (file_exists($file_text)) {
  //             $note = File::get("$public/$phototelegram_s[$i]-catatan.txt");
  //           } else {
  //             $note = '';
  //           }
  //         }
  //         $photo_caption = str_replace('_', ' ', $phototelegram_s[$i]);
  //         if (file_exists($file)) {
  //           Telegram::sendPhoto([
  //             'chat_id' => '--1001152815513',
  //             // 'chat_id' => '-1001152815513' atau 519446576
  //             'caption' => "Label $photo_caption \n $note",
  //             'photo' => $file,
  //           ]);
  //         }
  //       }
  //     }
  //   });
  // }

  public static function update_dispatch($req, $id)
  {
    $matches = [];

      foreach (array_keys($req->all()) as $str){
        if (preg_match ('/^photo-(\w+)/i', $str, $m))
        {
          $matches[] = $m[1];
        }
      }
      // dd($matches);
      if($matches)
      {
        foreach($matches as $val)
        {
          $messages[] = ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan foto '.$val.' batasnya adalah 3MB'];
          $rules['photo-'.$val] = 'image|mimes:jpeg,png,jpg,gif,svg|max:3000';
        }

        $validator = \Validator::make($req->all(), $rules);
        // dd($messages, $rules, $validator->fails());

        if ($validator->fails()) {
          return redirect()->back()
          ->withInput($req->all())
          ->withErrors($validator)
          ->with('alerts', $messages);
        }
      }
      // dd('stop');
    DB::transaction(function () use ($id, $req) {
      $session = session('auth');
      $botToken="1868997982:AAF90Uw8q0_qWRYXC-gFBGJQose-Nbb3FOk";
      $website="https://api.telegram.org/bot".$botToken;

      $check_alamat = DB::table('spbu_alamat')
        ->where('id_spbu', 'like', '%' . $req->id_spbu . '%')
        ->where('alamat', 'like', '%' . $req->id_alamat . '%')
        ->where('kota', 'like', '%' . $req->sto . '%')
        ->first();

      $that = new TeknisiModel();

      $get_data = self::show_Single_id_data($id);

      $check_teknisi = DB::table('user as u')
        ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
        ->select('u.id_user', 'k.nama')
        ->where('id_user', $session->id_user)
        ->first();

      $check_user = DB::table('user as u')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
      ->select('u.id_user', 'k.nama')
      ->where('id_user', $get_data->created_by)
      ->first();

      $that->handleFileUpload($req, $id);

      if($get_data->jenis_g == 'Grounding'){
        $that->handleFileUpload_chk($req, $id);
      }

      DB::table('spbu_dispatch')->where('id', $id)->update([
        'close' => date('Y-m-d H:i:s'),
        'berita_ac_sel' => $req->berita_ac_sel,
        'note_rusak' => $req->note_rusak,
        'koor' => $req->koor,
        'note' => $req->note,
        'stts' => $req->stts,
        'act' => $req->act,
        'updated_by' => $session->id_user,
      ]);

      $collect_detail = json_decode($req->collect_detail);
      $data_detail_tim = [];

      foreach ($collect_detail as $k => $v)
      {
        $data_detail_tim[$k]['id_spbu_dispatch'] = $id;
        $data_detail_tim[$k]['jenis_detail']     = $v->detail;
        $data_detail_tim[$k]['sn']               = $v->sn;
        $data_detail_tim[$k]['no_tiket']         = $v->no_tiket;
        $data_detail_tim[$k]['created_date']     = date('Y-m-d');
        $data_detail_tim[$k]['created_time']     = date('H:i:s');
        $data_detail_tim[$k]['created_by']       = $session->id_user;
      }

      DB::table('spbu_detail_tim')->where('id_spbu_dispatch', $id)->delete();

      if($data_detail_tim)
      {
        DB::table('spbu_detail_tim')->insert($data_detail_tim);
      }

      if ($req->berita_ac_sel == 1) {
        $BA = 'OK';
      } else {
        $BA = 'NOK';
      }

      if ($req->act == 1) {
        $act = 'Kunjungan';
      } else {
        $act = 'Vcall';
      }

      if ($req->stts == 1) {
        $stts = 'Open';
        $daeme = date('Y-m-d H:i:s');
      } elseif ($req->stts == 0) {
        $stts = 'Closed';
        $daeme = $get_data->close;
      } elseif ($req->stts == 3) {
        $stts = 'Pending';
        $daeme = $get_data->close;
      }

      $t1 = date_create($get_data->open);
      $t2 = date_create($daeme);
      $diff = date_diff($t2, $t1);

      $nama_user = $check_user->nama ? "$check_user->id_user ($check_user->nama)" : $check_user->id_user;

      $message = "Orderan dari NIK: $nama_user \n";

      $message .= "Nomor Tiket: " . $get_data->no_tiket . " \n";
      $message .= "<b>ID SPBU</b>: $get_data->id_spbu \n";
      $message .= "<b>Tanggal Open</b>: " . $get_data->open .  "s/d" .$daeme. "\n";
      $message .= "<b>Durasi</b>: " . $diff->format("%d Hari %h Jam %i Menit") . "\n";
      $message .= "======================== \n";
      $message .= "<b>Kota</b>: $get_data->kota\n";
      $message .= "<b>Alamat</b>: $get_data->alamat\n";
      $message .= "<b>Jenis Gangguan</b>: $get_data->jenis_g ($act)\n";
      $message .= "<b>Catatan Gangguan</b>: $get_data->ggn_open\n";

      $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

      $message .= "<b>Teknisi Pengerjaan</b>: $teknisi_nm\n";
      $message .= "<b>Berita Acara</b>: $BA\n";

      if($collect_detail)
      {
        $message .= "<b>Tiket</b>:\n";
        foreach ($collect_detail as $k => $v)
        {
          $detail = $v->detail;
          $sn = $v->sn;
          $no_tiket = $v->no_tiket;

          $message .= "$sn<b>($no_tiket)</b>: $detail\n";
        }
      }


      if ($req->berita_ac == 0 && $req->note_rusak) {
        $message .= "<b>Catatan Kerusakan</b>: $req->note_rusak\n";
      }

      $message .= "<a href=\"https://www.google.com/maps/place/$req->koor\">$req->koor </a>\n";
      $message .= "<b>Catatan</b>:\n$req->note\n";
      $message .= "<b>Status</b>: $stts\n";
      $message .= '#TEKNISI'.preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);

      $params=[
        'chat_id' => '-1001152815513',
         // 'chat_id' => '-1001152815513' atau 519446576
         'parse_mode' => 'html',
         'text'=> $message,
      ];
      $ch = curl_init();
      $optArray = array(
        CURLOPT_URL => $website.'/sendMessage',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $params,
      );
      curl_setopt_array($ch, $optArray);
      $result = curl_exec($ch);
      curl_close($ch);

      $TeknisiController = new TeknisiController();
      $checklist = $TeknisiController->checklist;
      if ($get_data->jenis_g == 'Grounding'){
        $message_chk = '';

        $params=[
          'chat_id' => '-1001152815513',
          // 'chat_id' => '-1001152815513' atau 519446576
          'parse_mode' => 'html',
          'text' => "\nChecklist Instalasi Grounding BA\n\n",
        ];
        $ch = curl_init();
        $optArray = array(
          CURLOPT_URL => $website.'/sendMessage',
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $params,
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);

        $chk = $req->checkbox_ground;

        if($chk){
          foreach($chk as $key_p => $dt){
            $public = public_path() . '/upload2/spbu/'.$id.'/chckbox';
            $file = $public . "/" . $dt . ".jpg";
            if (file_exists($file)) {
              $checked_ground[$key_p] = $dt;
            }
          }

          foreach($checked_ground as $checked_data_grounding){
            foreach ($checklist as $name => $rdata) {
              $message_chk = '';
              foreach ($rdata as $data_chk_bx) {
                if($checked_data_grounding == $data_chk_bx['nama']){
                  $message_chk .= "$name\n \n";
                  $var_array = $data_chk_bx['id'];
                  if (isset($req->$var_array)) {
                    $name = 'txt_' . $data_chk_bx['id'];
                    if (isset($req->$name)) {
                      $text = $req->$name;
                    } else {
                      $text = 'Tidak Ada Keterangan';
                    }
                    $chk = '✅';
                  } else {
                    $text = 'Tidak ada keterangan';
                    $chk = '❌';
                  }
                  $nama = $data_chk_bx['nama'];
                  $message_chk .= "$nama : $chk\n";
                  $message_chk .= "Keterangan : $text\n";
                  $message_chk .= '#TEKNISI'.preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm). " \n";

                  $name_id = $data_chk_bx['nama'];
                  $public = public_path() . '/upload2/spbu/'.$id.'/chckbox';
                  $file = $public . "/" . $name_id . ".jpg";
                  if (file_exists($file)) {
                    $file_text = "$public/$name_id-catatan.txt";
                    if (file_exists($file_text)) {
                      $note = File::get("$public/$name_id-catatan.txt");
                    } else {
                      $note = '';
                    }
                  }
                  $photo_caption = str_replace('_', ' ', $data_chk_bx['nama']);
                  if (file_exists($file)) {
                    $params_photo=[
                      'chat_id' => '-1001152815513',
                      // 'chat_id' => '-1001152815513' atau 519446576
                      'caption' => "Label Checklist $photo_caption \n $note \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
                      'photo' =>  new \CURLFile($file),
                    ];
                    $ch = curl_init();
                    $optArray_photo = array(
                      CURLOPT_URL => $website.'/sendPhoto',
                      CURLOPT_RETURNTRANSFER => 1,
                      CURLOPT_POST => 1,
                      CURLOPT_POSTFIELDS => $params_photo,
                      CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
                    );
                    curl_setopt_array($ch, $optArray_photo);
                    $result = curl_exec($ch);
                    curl_close($ch);
                  }
                }
              }
            }

            $params_detail_msg_chk=[
              'chat_id' => '-1001152815513',
              // 'chat_id' => '-1001152815513' atau 519446576
              'parse_mode' => 'html',
              'text' => "$message_chk",
            ];
            $ch = curl_init();
            $optArray_detail_msg_chk = array(
              CURLOPT_URL => $website.'/sendMessage',
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_POST => 1,
              CURLOPT_POSTFIELDS => $params_detail_msg_chk,
            );
            curl_setopt_array($ch, $optArray_detail_msg_chk);
            $result = curl_exec($ch);
            curl_close($ch);
          }
        }
      }

      $TeknisiController = new TeknisiController();

      $oddeven = array_chunk($TeknisiController->photodispatch, 7);
      foreach ($oddeven as $phototelegram_s) {
        for ($i = 0; $i < count($phototelegram_s); $i++) {
          $public = public_path() . '/upload2/spbu/' . $id;
          $file = $public . "/" . $phototelegram_s[$i] . ".jpg";
          if (file_exists($file)) {
            $file_text = "$public/$phototelegram_s[$i]-catatan.txt";
            if (file_exists($file_text)) {
              $note = File::get("$public/$phototelegram_s[$i]-catatan.txt");
            } else {
              $note = '';
            }
          }
          $photo_caption = str_replace('_', ' ', $phototelegram_s[$i]);
          if (file_exists($file)) {
            Telegram::sendPhoto([
              'chat_id' => '-1001152815513',
              // 'chat_id' => '-1001152815513' atau 519446576
              'caption' => "Label $photo_caption \n $note \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
              'photo' => $file,
            ]);
          }
        }
      }
    });
    return redirect('/home')->with('alerts', [['type' => 'success', 'text' => "Data Berhasil Diupdate!"]]);
  }

  public static function save_broken_part($req, $id = null){
    $matches = [];

      foreach (array_keys($req->all()) as $str){
        if (preg_match ('/^photo-(\w+)/i', $str, $m))
        {
          $matches[] = $m[1];
        }
      }

      if($matches)
      {
        foreach($matches as $val)
        {
          $messages[] = ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan foto '.$val.' batasnya adalah 3MB'];
          $rules['photo-'.$val] = 'image|mimes:jpeg,png,jpg,gif,svg|max:3000';
        }

        $validator = \Validator::make($req->all(), $rules);
        // dd($messages, $rules, $validator->fails());

        if ($validator->fails()) {
          return redirect()->back()
          ->withInput($req->all())
          ->withErrors($validator)
          ->with('alerts', $messages);
        }
      }

    DB::transaction(function () use ($id, $req) {
      $session = session('auth');
      $botToken="1837896175:AAHYWXdgthXhnqD8DfcivyG9RjtiEiYFJFQ";
      $website="https://api.telegram.org/bot".$botToken;

      $check_alamat = DB::table('spbu_alamat')
      ->where('id_spbu', 'like', '%' . $req->id_spbu . '%')
      ->where('alamat', 'like', '%' . $req->id_alamat . '%')
      ->where('kota', 'like', '%' . $req->sto . '%')
      ->first();

      $message = '';

      if (in_array($session->spbu_level, [1, 4])) {
        $teknisi_id = $req->teknisi;

        $check_user = DB::table('user as u')
        ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
        ->select('u.id_user', 'k.nama')
        ->where('id_user', $session->id_user)
        ->first();

        $nama_user = $check_user->nama ? "$check_user->id_user ($check_user->nama)" : $check_user->id_user;

        $message .= "Orderan dari NIK: $nama_user \n";
      } else {
        $teknisi_id = $session->id_user;
      }

      $pergantian_relok = null;

      if($req->hasfile('photo-perangkat_relokasi')){
        $pergantian_relok = $req->pergantian_relok;
      }

      if ($id) {
        DB::table('spbu_perangkat_rusak')->where('id', $id)->update([
          'id_spbu_add' => $check_alamat->id,
          'no_tiket' => $req->no_tiket,
          'lokasi' => $req->koor,
          'teknisi' => $teknisi_id,
          'change_part' => $req->change_part,
          'status_perangkat_relok' => $pergantian_relok,
          'note' => $req->note,
          'broken_part' => $req->perangkat_rusak,
          'jenis_perangkat' => $req->tipe_perangkat,
        ]);

        $get_data = DB::table('spbu_perangkat_rusak')->where('id', $id)->first();

        $tgl_open = $get_data->open;

      } else {
        $tgl_open = date('Y-m-d H:i:s');

        $id =DB::table('spbu_perangkat_rusak')->insertGetId([
          'id_spbu_add' => $check_alamat->id,
          'no_tiket' => $req->no_tiket,
          'lokasi' => $req->koor,
          'change_part' => $req->change_part,
          'status_perangkat_relok' => $pergantian_relok,
          'teknisi' => $teknisi_id,
          'note' => $req->note,
          'broken_part' => $req->perangkat_rusak,
          'jenis_perangkat' => $req->tipe_perangkat,
          'created_by' => $session->id_user,
        ]);
      }

      $that = new TeknisiModel();


      if($req->change_part == 'Dari Relokasi SPBU'){
        DB::table('spbu_perangkat_rusak')->where('id', $id)->update([
          'close' => date('Y-m-d H:i:s'),
          'update_by' => $session->id_user
        ]);
        $check_id_parent = DB::table('spbu_perangkat_rusak')->select('id_spbu_add')->where('parent', $id)->get();
        $get_alamat = DB::table('spbu_alamat')->where('id_spbu', $req->id_spbu_relok)->first();

        // dd($get_alamat, $req->id_spbu_relok, !in_array($get_alamat->id, array_column(json_decode(@$check_id_parent, true), 'id_spbu_add')));

        if(!in_array($get_alamat->id, array_column(json_decode(@$check_id_parent, true), 'id_spbu_add'))){
          $id_new =DB::table('spbu_perangkat_rusak')->insertGetId([
            'id_spbu_add' => $get_alamat->id,
            'no_tiket' => $req->no_tiket,
            'lokasi' => $req->koor,
            'jenis' => 1,
            'parent' => $id,
            'change_part' => 'NOK/Belum di ganti',
            'status_perangkat_relok' => $pergantian_relok,
            'teknisi' => $teknisi_id,
            'note' => $req->note,
            'broken_part' => $req->perangkat_rusak,
            'jenis_perangkat' => $req->tipe_perangkat,
            'created_by' => $session->id_user,
          ]);
        }
      }

      $that->handleFileUpload_brokenpart($req, $id);

      if($req->hasFile('file_ba')){
        $that->handleBAUpload($req, $id);
      }

      if($req->no_tiket){
        $message .= "Nomor Tiket: " . $req->no_tiket . " \n";
      }
      $message .= "<b>ID SPBU</b> : $req->id_spbu \n";
      $message .= "<b>Tanggal Open</b> : " . $tgl_open . "\n";

      $stts = 1;
      $detail_stts = 'Open';
      $file = public_path() . '/upload2/spbu/broken_part/' . $id . '/perangkat_baru.jpg';

      if (file_exists($file)) {
        $stts = 0;
        $detail_stts = 'Close';

        DB::table('spbu_perangkat_rusak')->where('id', $id)->update([
          'close' => date('Y-m-d H:i:s'),
          'update_by' => $session->id_user
        ]);

        $t1 = date_create($tgl_open);
        $t2 = date_create(date('Y-m-d H:i:s'));
        $diff = date_diff($t2, $t1);

        $message .= "<b>Tanggal Close</b> : " . date('Y-m-d H:i:s') . "\n";
        $message .= "<b>Durasi</b> : " . $diff->format("%d Hari %h Jam %i Menit") . "\n";
      }

      $check_teknisi = DB::table('user as u')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
      ->select('u.id_user', 'k.nama')
      ->where('id_user', $teknisi_id)
      ->first();

      $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

      $message .= "<b>Teknisi Yang Bertugas</b> : $teknisi_nm \n";

      $message .= "======================== \n";
      $message .= "<b>Kota</b> : $req->sto\n";
      $message .= "<b>Tiket</b> : $req->no_tiket\n";
      $message .= "<b>Jenis Gangguan</b> : Perangkat Rusak\n";
      $message .= "<b>Alat Rusak</b> : $req->perangkat_rusak\n";
      $message .= "<b>Catatan</b> : $req->note\n";
      $message .= "<b>Lokasi Perangkat rusak</b> : $req->koor\n";
      $message .= "<b>Status Pergantian Perangkat</b> : $req->change_part\n";

      $file_relok = public_path() . '/upload2/spbu/broken_part/' . $id . '/perangkat_relokasi.jpg';

      if (file_exists($file_relok)){
        DB::table('spbu_perangkat_rusak')->where('id', $id)->update([
          'status_ganti_relok' => $req->pergantian_relok
        ]);

        $message .= "<b>Status Pergantian Relokasi</b> : $req->koor\n";
      }

      $message .= "<b>Status</b> : $detail_stts\n";
      $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);

      $params=[
        'chat_id' => '-1001152815513',
          // 'chat_id' => '-1001152815513' atau 519446576
          'parse_mode' => 'html',
          'text'=> $message,
      ];
      $ch = curl_init();
      $optArray = array(
        CURLOPT_URL => $website.'/sendMessage',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $params,
      );
      curl_setopt_array($ch, $optArray);
      $result = curl_exec($ch);
      curl_close($ch);

      $path = public_path() . '/upload2/spbu/broken_part/' . $id . '/BA/';

      if (file_exists($path)) {
        $files = preg_grep('~^File BA.*$~', scandir($path));
        $files = array_values($files);
        $real_path = $path.''.$files[0];
        $params_file=[
          'chat_id' => '-1001152815513',
            // 'chat_id' => '-1001152815513' atau 519446576
            'caption' => "Dokumen Berita Acara \n",
            'document' =>  new \CURLFile($real_path),
        ];
        $ch = curl_init();
        $optArray_files = array(
          CURLOPT_URL => $website.'/sendDocument',
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $params_file,
          CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
        );
        curl_setopt_array($ch, $optArray_files);
        $result = curl_exec($ch);
        curl_close($ch);
      }

      $TeknisiController = new TeknisiController();

      $photo = $TeknisiController->part;

      foreach ($photo as $pht) {
        $public = public_path() . '/upload2/spbu/broken_part/' . $id;
        $file = $public . "/" . $pht . ".jpg";
        if (file_exists($file)) {
          if($req->hasfile('photo-perangkat_relokasi') && $pht == 'perangkat_relokasi'){
            $msg_photo = "Status: $pergantian_relok \n";
          } else {
            $msg_photo = null;
          }
          $file_text = "$public/$pht-catatan.txt";
          if (file_exists($file_text)) {
              $note = File::get("$public/$pht-catatan.txt");
          } else {
              $note = '';
          }
        }
        $photo_caption = str_replace('_', ' ', $pht);
        if (file_exists($file)) {
          $params_photo=[
            'chat_id' => '-1001152815513',
              // 'chat_id' => '-1001152815513' atau 519446576
              'caption' => "Label $photo_caption \n $msg_photo $note \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
              'photo' =>  new \CURLFile($file),
          ];
          $ch = curl_init();
          $optArray_photo = array(
            CURLOPT_URL => $website.'/sendPhoto',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params_photo,
            CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
          );
          curl_setopt_array($ch, $optArray_photo);
          $result = curl_exec($ch);
          curl_close($ch);
        }
      }
    });
  }

  private function handleFileUpload($req, $id)
  {
    $TeknisiController = new TeknisiController();
    foreach ($TeknisiController->photodispatch as $name) {
      $input = 'photo-' . $name;
      $kmntr = 'catatan_' . $name;
      $komentar = $req->$kmntr;
      $path = public_path() . '/upload2/spbu/' . $id;

      if (!file_exists($path)) {
        if (!mkdir($path, 0770, true)) {
          return 'gagal menyiapkan folder foto evidence';
        }
      }

      if ($komentar != '') {
        file_put_contents("$path/$name-catatan.txt", $komentar);
      }

      if ($req->hasFile($input)) {
        $file = $req->file($input);
        $ext = 'jpg';
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());

          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence ' . $name;
        }
      }
    }
  }

  private function handleBAUpload($req, $id)
  {
    $path = public_path() . '/upload2/spbu/broken_part/' . $id . '/BA/';

    if (!file_exists($path)) {
      if (!mkdir($path, 0770, true)) {
        return 'gagal menyiapkan folder BA';
      }
    }

    if ($req->hasFile('file_ba')) {
      $file = $req->file('file_ba');
      try {
        $filename = 'File BA.'.strtolower( $file->getClientOriginalExtension() );
        $file->move("$path", "$filename");
      } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan BA';
      }
    }
  }

  private function handleFileUpload_brokenpart($req, $id)
  {
    $TeknisiController = new TeknisiController();
    $part = $TeknisiController->part;
    foreach ($part as $name) {
      $input = 'photo-' . $name;
      $kmntr = 'catatan_' . $name;
      $komentar = $req->$kmntr;
      $path = public_path() . '/upload2/spbu/broken_part/' . $id;

      if (!file_exists($path)) {
        if (!mkdir($path, 0770, true)) {
          return 'gagal menyiapkan folder foto evidence';
        }

      }

      if ($komentar != '') {
        file_put_contents("$path/$name-catatan.txt", $komentar);
      }

      if ($name == 'berita_acara_relokasi'){
        file_put_contents("$path/$name-selectba.txt", $req->pergantian_ba);
      }

      if ($req->hasFile($input)) {
        $file = $req->file($input);
        $ext = 'jpg';
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());

          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence ' . $name. "\n $e";
        }
        $ada[] = $input;
      }
    }
    // dd($req->all(), $part, $ada);
  }

  private function handleFileUpload_chk($req, $id)
  {
    $TeknisiController = new TeknisiController();
    $checklist = $TeknisiController->checklist;
    foreach ($checklist as $rdata) {
      foreach ($rdata as $data) {
        $name = $data['nama'];
        $id_photo = $data['id'];
        $input = 'photo-' . $id_photo;
        $kmntr = 'catatan_chk_' . $id_photo;
        $komentar = $req->$kmntr;
        $path = public_path() . '/upload2/spbu/' . $id . '/chckbox';

        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)) {
            return 'gagal menyiapkan folder foto evidence';
          }
        }

        if ($komentar != '') {
          file_put_contents("$path/$name-catatan.txt", $komentar);
        }

        if ($req->hasFile($input)) {
          $file = $req->file($input);
          $ext = 'jpg';
          try {
            $moved = $file->move("$path", "$name.$ext");

            $img = new \Imagick($moved->getRealPath());

            $img->scaleImage(100, 150, true);
            $img->writeImage("$path/$name-th.$ext");
          } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence ' . $name;
          }
        }
      }
    }
  }

  public static function get_detail_partbroken($id){
    $AdminController = new AdminController();
    $tipe = $AdminController->foto_inventory;
    return $tipe[$id]['Tipe'];
  }

  public static function save_repair_item($req, $id = null){
    $matches = [];

      foreach (array_keys($req->all()) as $str){
        if (preg_match ('/^photo-(\w+)/i', $str, $m))
        {
          $matches[] = $m[1];
        }
      }

      if($matches)
      {
        foreach($matches as $val)
        {
          $messages[] = ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan foto '.$val.' batasnya adalah 3MB'];
          $rules['photo-'.$val] = 'image|mimes:jpeg,png,jpg,gif,svg|max:3000';
        }

        $validator = \Validator::make($req->all(), $rules);
        // dd($messages, $rules, $validator->fails());

        if ($validator->fails()) {
          return redirect()->back()
          ->withInput($req->all())
          ->withErrors($validator)
          ->with('alerts', $messages);
        }
      }

    DB::transaction(function () use ($id, $req) {
      $session = session('auth');
      $botToken="1837896175:AAHYWXdgthXhnqD8DfcivyG9RjtiEiYFJFQ";
      $website="https://api.telegram.org/bot".$botToken;

      $message = '';

      if (in_array($session->spbu_level, [1, 4])) {
        $teknisi_id = $req->teknisi;

        $check_user = DB::table('user as u')
        ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
        ->select('u.id_user', 'k.nama')
        ->where('id_user', $session->id_user)
        ->first();

        $nama_user = $check_user->nama ? "$check_user->id_user ($check_user->nama)" : $check_user->id_user;

        $message .= "Orderan dari NIK: $nama_user \n";
      } else {
        $teknisi_id = $session->id_user;
      }

      if ($id) {
        DB::table('spbu_repairing')->where('id', $id)->update([
          'perangkat_rusak' => $req->alat,
          'serial_number' => $req->sn,
          'witel' => $req->witel,
          'teknisi' => $teknisi_id,
          'status_repair' => $req->status_repair,
          'note_rusak' => $req->ket,
          'modified_by' => session('auth')->id_user,
        ]);

        $get_data = DB::table('spbu_repairing')->where('id', $id)->first();

        $tgl_open = $get_data->open;

      } else {
        $tgl_open = date('Y-m-d H:i:s');

        $id =DB::table('spbu_repairing')->insertGetId([
          'perangkat_rusak' => $req->alat,
          'serial_number' => $req->sn,
          'witel' => $req->witel,
          'teknisi' => $teknisi_id,
          'status_repair' => $req->status_repair,
          'note_rusak' => $req->ket,
          'created_by' => session('auth')->id_user,
        ]);
      }

      $that = new TeknisiModel();

      $that->handle_repairing_file($req, $id);

      if($req->no_tiket){
        $message .= "Nomor Tiket: " . $req->no_tiket . " \n";
      }
      $message .= "<b>Tanggal Open</b> : " . $tgl_open . "\n";

      $check_teknisi = DB::table('user as u')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
      ->select('u.id_user', 'k.nama')
      ->where('id_user', $teknisi_id)
      ->first();

      $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

      $message .= "<b>Teknisi Yang Bertugas</b> : $teknisi_nm \n";

      $message .= "======================== \n";
      $message .= "<b>Serial Numebr</b> : $req->sn\n";
      $message .= "<b>Jenis Gangguan</b> : Repairing\n";
      $message .= "<b>Alat Rusak</b> : $req->perangkat_rusak\n";
      $message .= "<b>Catatan</b> : $req->note_rusak\n";

      $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);

      $params=[
        'chat_id' => '-1001152815513',
          // 'chat_id' => '-1001152815513' atau 519446576
          'parse_mode' => 'html',
          'text'=> $message,
      ];
      $ch = curl_init();
      $optArray = array(
        CURLOPT_URL => $website.'/sendMessage',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $params,
      );
      curl_setopt_array($ch, $optArray);
      $result = curl_exec($ch);
      curl_close($ch);


      $AdminController = new AdminController();

      $photo = $AdminController->foto_repairing;

      foreach ($photo as $pht) {
        $public = public_path() . '/upload2/spbu/repairing/' . $id;
        $file = $public . "/" . $pht . ".jpg";
        if (file_exists($file)) {
          $file_text = "$public/$pht-catatan.txt";
          if (file_exists($file_text)) {
              $note = File::get("$public/$pht-catatan.txt");
          } else {
              $note = '';
          }
        }
        $photo_caption = str_replace('_', ' ', $pht);
        if (file_exists($file)) {
          $params_photo=[
            'chat_id' => '-1001152815513',
              // 'chat_id' => '-1001152815513' atau 519446576
              'caption' => "Label $photo_caption \n $note \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
              'photo' =>  new \CURLFile($file),
          ];
          $ch = curl_init();
          $optArray_photo = array(
            CURLOPT_URL => $website.'/sendPhoto',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params_photo,
            CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
          );
          curl_setopt_array($ch, $optArray_photo);
          $result = curl_exec($ch);
          curl_close($ch);
        }
      }
    });
  }

  private function handle_repairing_file($req, $id)
  {
    $AdminController = new AdminController();
    foreach ($AdminController->foto_repairing as $name) {
      $input = 'photo-' . $name;
      $kmntr = 'catatan_' . $name;
      $komentar = $req->$kmntr;
      $path = public_path() . '/upload2/spbu/repairing/' . $id;

      if (!file_exists($path)) {
        if (!mkdir($path, 0770, true)) {
          return 'gagal menyiapkan folder foto evidence';
        }
      }

      if ($komentar != '') {
        file_put_contents("$path/$name-catatan.txt", $komentar);
      }

      if ($req->hasFile($input)) {
        $file = $req->file($input);
        $ext = 'jpg';
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());

          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence ' . $name;
        }
      }
    }
  }

}

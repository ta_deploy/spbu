<?php

namespace App\DA;

use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\AdminController;
use Telegram;
use ZipArchive;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

date_default_timezone_set("Asia/Makassar");

class AdminModel
{
  public static function show_Single_id_data($id)
  {
    return DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->leftjoin('1_2_employee as k2', 'k2.nik', '=', 'sa.naker')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.id as id_adds', 'sd.teknisi as id_user', 'sa.naker as naker_id', 'k2.nama as naker_nama', 'k.nama')
      ->where('sd.id', $id)
      ->first();
  }

  public static function show_data($m1, $m2)
  {
    return DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->leftjoin('1_2_employee as k2', 'k2.nik', '=', 'sa.naker')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.id as id_adds', 'sd.teknisi as id_user', 'sa.naker as naker_id', 'k2.nama as naker_nama', 'k.nama')
      ->whereBetween(DB::RAW("DATE(sd.modifiet_at)"), [$m1, $m2])
      ->where('sd.active', 1)
      ->get();
  }

  public static function id_spbu_search($data, $id)
  {
    return DB::table('spbu_alamat')
      ->where('kota', 'like', '%' . $id . '%')
      ->where('id_spbu', 'like', '%' . $data . '%')
      ->get();
  }

  public static function show_list_alamat()
  {
    return DB::table('spbu_alamat')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'spbu_alamat.naker')
      ->select('k.nama', 'spbu_alamat.*')
      ->get();
  }

  public static function save_alamat($req, $id, $jenis)
  {
    if($jenis == 'update')
    {
      DB::table('spbu_alamat')->where('id', $id)->update([
        'type'    => $req->tipe,
        'TL'      => $req->teknisi,
        'id_spbu' => $req->id,
        'status'  => $req->status,
        'alamat'  => $req->alamat,
        'naker'   => $req->teknisi,
        'kota'    => $req->kota,
      ]);
    }
    else
    {
      DB::table('spbu_alamat')->insert([
        'type'    => $req->tipe,
        'TL'      => $req->teknisi,
        'id_spbu' => $req->id,
        'status'  => $req->status,
        'alamat'  => $req->alamat,
        'naker'   => $req->teknisi,
        'kota'    => $req->kota,
      ]);
    }
  }

  public static function show_alamat_detail($id, $jenis)
  {
    if ($jenis == 'data') {
      return DB::table('spbu_alamat')
        ->leftjoin('1_2_employee as k', 'k.nik', '=', 'spbu_alamat.naker')
        ->select('k.nama', 'spbu_alamat.*')
        ->where('id', $id)->first();
    } elseif ($jenis == 'status') {
      return DB::table('spbu_alamat')->select('status')->groupBy('status')->get();
    } elseif ($jenis == 'type') {
      return DB::table('spbu_alamat')->select('type')->groupBy('type')->get();
    } elseif ($jenis == 'kota') {
      return DB::table('spbu_alamat')->select('kota')->groupBy('kota')->get();
    } elseif ($jenis == 'naker') {
      return DB::table('spbu_alamat')->select('naker')->groupBy('naker')->get();
    }
  }

  // public static function alamat_search($data, $id, $sto)
  // {
  //   return DB::table('spbu_alamat')
  //     ->leftjoin('1_2_employee as k', 'k.nik', '=', 'spbu_alamat.naker')
  //     ->select('k.nama', 'spbu_alamat.*')
  //     ->where('id_spbu', 'like', '%' . $id . '%')
  //     ->where('kota', 'like', '%' . $sto . '%')
  //     ->where('alamat', 'like', '%' . $data . '%')
  //     ->get();
  // }

  public static function alamatstts_search($id)
  {
    return DB::table('spbu_alamat')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'spbu_alamat.naker')
      ->select('k.nama', 'spbu_alamat.*')
      ->where('id_spbu', $id)
      ->first();
  }

  public static function teknisi_search($data, $jenis = 'limit')
  {
    $sql =  DB::table('user as u')
    ->leftjoin('1_2_employee as k', 'u.id_user', '=', 'k.nik')
    ->select('u.id_user', 'k.nama')
    ->where(function($join) use($data){
      $join->where('id_user', $data)
      ->orWhere('k.nama', $data);
    });

    if($jenis == 'limit')
    {
      $sql = $sql->whereIn('spbu_level', [2, 4]);
    }

    return $sql->get();
  }

  public static function sto_search($data)
  {
    return DB::table('spbu_alamat')
      ->where('kota', 'like', '%' . $data . '%')
      ->groupBy('kota')
      ->get();
  }

  public static function save_dispatch($req, $id)
  {
    DB::transaction(function () use ($id, $req) {
      $session = session('auth');
      $check_alamat = DB::table('spbu_alamat')
        ->where('id_spbu', $req->id_spbu)
        ->first();
      $check = self::show_Single_id_data($id);

      $check_teknisi = DB::table('user as u')
        ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
        ->select('u.id_user', 'k.nama')
        ->where('id_user', $req->teknisi)
        ->first();

      $check_user = DB::table('user as u')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
      ->select('u.id_user', 'k.nama')
      ->where('id_user', $session->id_user)
      ->first();
      // dd($check_alamat);
      $no_tiket = 'INT-' . date('dH') . '' . $check_alamat->id . '-' . substr($req->id_spbu, 0, 2) . '-' . substr(strtotime("now"), 0, -4);

      $nama_user = $check_user->nama ? "$check_user->id_user ($check_user->nama)" : $check_user->id_user;
      $message = '';

      $message .= "Orderan dari NIK: $nama_user \n";
      $message .= "Nomor Tiket: " . $no_tiket . " \n";
      $message .= "<b>ID SPBU</b> : $req->id_spbu \n";
      $message .= "<b>Tanggal Open</b> : " . date('Y-m-d H:i:s') . "\n";

      $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

      $message .= "<b>Teknisi Yang Bertugas</b> : $teknisi_nm \n";

      if (empty($check)) {
        $id = DB::table('spbu_dispatch')->insertGetId([
          'id_spbu_add' => $check_alamat->id,
          'no_tiket' => $no_tiket,
          'jenis_g' => $req->jenis_g,
          'teknisi' => $req->teknisi,
          'ggn_open' => $req->ggn_open,
          'stts' => 1,
          'created_by' => $session->id_user,
          ]);
      } else {
        if($check_teknisi->id_user != $check->teknisi){
          $check_prev = DB::table('user as u')
          ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
          ->select('u.id_user', 'k.nama')
          ->where('id_user', $check->teknisi)
          ->first();
          $message .= "<b>Teknisi Sebelumnya</b> : $check_prev->nama (" . $check->teknisi . ")\n";
        }
        DB::table('spbu_dispatch')->where('id', $check->id)->update([
          'id_spbu_add' => $check_alamat->id,
          'no_tiket' => $no_tiket,
          'jenis_g' => $req->jenis_g,
          'teknisi' => $check_teknisi->id_user,
          'act' => $req->act,
          'ggn_open' => $req->ggn_open,
          'stts' => 1,
          'created_by' => $session->id_user,
          ]);
        $message .= "<b>Tanggal Edit</b> : " . $check->modifiet_at . "\n";
      }

      $message .= "======================== \n";
      $message .= "<b>Kota</b> : $req->sto\n";
      $message .= "<b>Jenis Gangguan</b> : $req->jenis_g\n";
      $message .= "<b>Catatan Gangguan</b> : $req->ggn_open\n";
      $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);

      Telegram::sendMessage([
        'chat_id' => '-1001152815513',
        // 'chat_id' => '-1001152815513' atau 519446576
        'parse_mode' => 'html',
        'text' => "$message",
      ]);
    });
  }

  public static function get_data_graph($req)
  {
    return DB::select("SELECT sa.kota as witel,
        SUM(CASE WHEN sd.close IS NOT NULL THEN 1 ELSE 0 END) as jml
        FROM spbu_dispatch sd LEFT JOIN spbu_alamat sa ON sd.id_spbu_add = sa.id WHERE sd.modifiet_at LIKE '$req->month1%' OR sd.modifiet_at LIKE '$req->month2%' AND active = 1 GROUP BY sa.kota HAVING jml != 0 ORDER BY sa.kota ASC");
  }

  public static function get_rajin_graph($req)
  {
    return DB::select("SELECT
        CONCAT(k.nama , ' (', sd.teknisi, ')' ) as teknisi,
        SUM(CASE WHEN sd.close IS NOT NULL THEN 1 ELSE 0 END) as jml
        FROM spbu_dispatch sd
        LEFT JOIN 1_2_employee k ON sd.teknisi = k.nik
        WHERE sd.modifiet_at LIKE '$req->month1%' OR sd.modifiet_at LIKE '$req->month2%' and active = 1 GROUP BY sd.teknisi HAVING jml != 0 ");
  }

  public static function table_dashboard($jenis_g, $status, $date1, $date2, $job)
  {
    $query ='';
    if($jenis_g != 'all'){
      $query .= "jenis_g IN ($jenis_g) AND ";
    }

    if($status != 'all'){
      $query .= " stts IN ($status) AND ";
    }

    if($job == 'me'){
      $auth = session('auth')->id_user;
      $query.= " teknisi = $auth AND ";
    }

    return DB::select("SELECT sd.teknisi, k.nama, sa.kota,
        (GROUP_CONCAT(sa.id_spbu SEPARATOR ', ')) AS spbu_id,
        (GROUP_CONCAT(sd.id SEPARATOR ', ')) AS id_dispt,
        (GROUP_CONCAT(sa.naker SEPARATOR ', ')) AS naker_responsible,
        (GROUP_CONCAT(sd.jenis_g SEPARATOR ', ')) AS jenis,
        (GROUP_CONCAT(IF(sd.close != '', DATEDIFF(sd.close, sd.open), DATEDIFF(NOW(), sd.open)) SEPARATOR ', ')) AS hari,
        (GROUP_CONCAT(TIMEDIFF(sd.close, sd.open) SEPARATOR ', ')) AS durasi,
        (GROUP_CONCAT(CASE
        WHEN sd.stts = 1 THEN 'belum'
        WHEN sd.stts = 0 THEN 'closed'
        ELSE 'pending'
        END SEPARATOR ', ')) AS stts
        FROM spbu_dispatch sd
        LEFT JOIN spbu_alamat sa ON sd.id_spbu_add = sa.id
        LEFT JOIN 1_2_employee k ON k.nik = sd.teknisi
        WHERE $query DATE(sd.modifiet_at) BETWEEN '$date1' AND '$date2' AND sd.active = 1 GROUP BY sd.teknisi
        ");
  }

  public static function dash_broken_part($lokasi, $date1, $date2, $change_part, $jenis)
  {
    $query ='';
    if($lokasi != 'all'){
      $query .= "lokasi IN ($lokasi) AND ";
    }

    if($change_part != 'all'){
      $query .= " change_part IN ($change_part) AND ";
    }

    if($jenis != 'all'){
      $query .= " jenis IN ($jenis) AND ";
    }

    $sql_strict = '';

    if(session('auth')->spbu_level == 2)
    {
      $sql_strict = ' AND spr.teknisi = '.session('auth')->id_user ;
    }
    else
    {
      $sql_strict = 'OR close IS NULL';
    }

    // dd(session('auth'), $sql_strict);

    return DB::select("SELECT spr.teknisi, k.nama, sa.kota,
        (GROUP_CONCAT(sa.id_spbu SEPARATOR ', ')) AS spbu_id,
        (GROUP_CONCAT(spr.id SEPARATOR ', ')) AS id_dispt,
        (GROUP_CONCAT(IF(spr.close != '', DATEDIFF(spr.close, spr.open), DATEDIFF(NOW(), spr.open)) SEPARATOR ', ')) AS hari,
        (GROUP_CONCAT(TIMEDIFF(spr.close, spr.open) SEPARATOR ', ')) AS durasi,
        (GROUP_CONCAT(CASE
        WHEN close IS NULL THEN 'belum'
        ELSE 'closed'
        END SEPARATOR ', ')) AS stts,
        (GROUP_CONCAT(CASE
        WHEN spr.jenis = 0 THEN 'Input Standar'
        ELSE 'Relokasi Dari SPBU'
        END SEPARATOR ', ')) AS jenis
        FROM spbu_perangkat_rusak spr
        LEFT JOIN spbu_alamat sa ON spr.id_spbu_add = sa.id
        LEFT JOIN 1_2_employee k ON k.nik = spr.teknisi
        WHERE spr.active = 1 AND ($query DATE(spr.modify_at) BETWEEN '$date1' AND '$date2' $sql_strict ) GROUP BY spr.teknisi
        ");
  }

  public static function table_act($jenis_g, $status, $date1, $date2)
  {
    $query ='';
    if($jenis_g != 'all'){
      $query .= "jenis_g IN ($jenis_g) AND ";
    }

    if($status != 'all'){
      $query .= " stts IN ($status) AND ";
    }

    $data = DB::select("SELECT sa.kota, c.id_user, d.nama,
    SUM(CASE WHEN jenis_g = 'Grounding' THEN 1 ELSE 0 END) As grounding,
    SUM(CASE WHEN jenis_g = 'Perangkat Rusak' THEN 1 ELSE 0 END) As perangkat_rusak,
    SUM(CASE WHEN jenis_g = 'Pergantian Perangkat' THEN 1 ELSE 0 END) As pergantian_perangkat,
    SUM(CASE WHEN jenis_g = 'Preventive Maintenance' THEN 1 ELSE 0 END) As preventive_maintenance,
    SUM(CASE WHEN jenis_g = 'Troubleshoot' THEN 1 ELSE 0 END) As troubleshoot
    FROM spbu_dispatch sd
    LEFT JOIN spbu_alamat sa ON sd.id_spbu_add = sa.id
    LEFT JOIN user c ON sd.teknisi = c.id_user
    LEFT JOIN 1_2_employee d ON d.nik = c.id_user
    WHERE $query DATE(sd.modifiet_at) BETWEEN '$date1' AND '$date2' AND sd.active = 1 GROUP BY c.id_user, sa.kota ORDER BY sa.kota ASC");

    $fn = [];

    foreach($data as $k => $v)
    {
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['kota']                   = $v->kota;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['id_user']                = $v->id_user;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['nama']                   = $v->nama;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['grounding']              = $v->grounding;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['perangkat_rusak']        = $v->perangkat_rusak;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['pergantian_perangkat']   = $v->pergantian_perangkat;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['preventive_maintenance'] = $v->preventive_maintenance;
      $fn[$v->nama . ' (' . $v->id_user . ')'][$v->kota]['troubleshoot']           = $v->troubleshoot;
    }

    return $fn;
  }

  public static function data_teknisi($id)
  {
    return DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->select('sd.*', 'k.nama', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.id as id_adds')
      ->where('teknisi', $id)
      ->where('stts', 1)->get();
  }

  public static function table_act_detail($req)
  {
    return DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->leftjoin('1_2_employee as k2', 'k2.nik', '=', 'sa.naker')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.id as id_adds', 'sd.teknisi as id_user', 'sa.naker as naker_id', 'k2.nama as naker_nama', 'k.nama')
      ->where('sa.kota', $req->kota)
      ->where('sd.act', $req->jenis)
      ->where(function ($join) use ($req) {
        $join->whereBetween(DB::RAW('DATE(sd.modifiet_at)'), ["$req->month1", "$req->month2"]);
      })
      ->get();
  }

  public static function spbu_search_by_naker($id)
  {
    return DB::table('spbu_alamat')->where('naker', $id)->get();
  }

  public static function get_list_absen()
  {
    $sql = DB::table('user as u')
    ->leftjoin('1_2_employee As k', 'u.id_user', '=', 'k.nik')
    ->select('u.id_user', 'k.nama')
    ->whereIn('spbu_level', [2, 4])
    ->where('k.ACTIVE', 1)
    ->get();

    $data_absen = DB::table('absen')->where([
      ['divisi', 'SPBU'],
      [DB::RAW("DATE_FORMAT(tglAbsen, '%Y-%m-%d')"), date('Y-m-d')]
    ])
    ->get();

    $data_absen = json_decode(json_encode($data_absen), TRUE);

    foreach($sql as $k => $v)
    {
      $data_renew[$k]['id_user']     = $v->id_user;
      $data_renew[$k]['nama']        = $v->nama;
      $data_renew[$k]['spbu_verify'] = null;

      if($data_absen)
      {
        $find_k = array_keys(array_column($data_absen, 'nik'), $v->id_user);

        if(count($find_k) )
        {
          $find_k = end($find_k);
          $data_renew[$k]['spbu_verify'] = $data_absen[$find_k]['approval'];
        }
      }
    }

    foreach($data_renew as $k => &$v)
    {
      if(is_null($v['spbu_verify']) )
      {
        unset($data_renew[$k]);
      }
    }

    return $data_renew;
  }

  public static function find_absen_u($id)
  {
    return DB::table('absen')->where([
      ['nik', $id],
      ['divisi', 'SPBU'],
      [DB::RAW("DATE_FORMAT(tglAbsen, '%Y-%m-%d')"), date('Y-m-d')]
    ])
    ->first();
  }

  public static function get_kota()
  {
    return DB::Table('spbu_kota')->get();
  }

  public static function verify_absen($id){
    DB::transaction(function () use ($id) {
      $data = self::find_absen_u($id);

      if($data)
      {
        DB::table('absen')->where([
          ['nik', $id],
          ['divisi', 'SPBU'],
          [DB::RAW("DATE_FORMAT(tglAbsen, '%Y-%m-%d')"), date('Y-m-d')]
        ])->update([
          'date_approval' => date('Y-m-d H:i:s'),
          'approval'      => 1,
          'approve_by'    => session('auth')->id_user,
          'status'        => 'HADIR',
        ]);

        DB::table('spbu_absen_log')->insert([
          'naker_absen' => $id,
          'detail' => 'OK',
          'status' => 1,
          'created_by' => session('auth')->id_user
        ]);
      }
    });

    $botToken="1893068284:AAHku9n4saLvLwnedoGQY_ATBkrjEBm45Fg";
    $website="https://api.telegram.org/bot".$botToken;

    $check_teknisi = DB::table('user as u')
    ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
    ->select('u.id_user', 'k.nama')
    ->where('id_user', $id)
    ->first();

    $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

    $message = "Teknisi $teknisi_nm sudah diverifikasi, selamat bekerja! \n";
    $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);

    $params=[
      'chat_id' => '-1001152815513',
      // 'chat_id' => '-1001152815513' atau 519446576
      'parse_mode' => 'html',
      'text' => "$message",
    ];
    $ch = curl_init();
    $optArray = array(
      CURLOPT_URL => $website.'/sendMessage',
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $params,
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
  }

  public static function reject_absen($id, $req){
    DB::transaction(function () use ($id, $req) {
      DB::table('absen')->where([
        ['nik', $id],
        ['divisi', 'SPBU'],
        [DB::RAW("DATE_FORMAT(tglAbsen, '%Y-%m-%d')"), date('Y-m-d')]
      ])->update([
        'approval' => 3,
      ]);

      DB::table('spbu_absen_log')->insert([
        'naker_absen' => $id,
        'detail' => $req->detail,
        'status' => 2,
        'created_by' => session('auth')->id_user
      ]);
    });

    $botToken="1893068284:AAHku9n4saLvLwnedoGQY_ATBkrjEBm45Fg";
    $website="https://api.telegram.org/bot".$botToken;

    $check_teknisi = DB::table('user as u')
    ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
    ->select('u.id_user', 'k.nama')
    ->where('id_user', $id)
    ->first();

    $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

    $message = "Verifikasi absen teknisi $teknisi_nm ditolak! Silahkan login halaman SPBU TOMMAN untuk melihat pesan! \n";
    $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);

    $params=[
      'chat_id' => '-1001152815513',
      // 'chat_id' => '-1001152815513' atau 519446576
      'parse_mode' => 'html',
      'text' => "$message",
    ];
    $ch = curl_init();
    $optArray = array(
      CURLOPT_URL => $website.'/sendMessage',
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $params,
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
  }

  public static function get_all_alamat(){
    return DB::table('spbu_alamat')
    ->leftjoin('1_2_employee', 'naker', '=', 'nik')
    ->select(DB::RAW("TRIM(CONCAT(COALESCE(nama, ''), ' (', naker, ')')) as nama_naker"), 'spbu_alamat.*')
    ->where('naker', '!=', 0)
    ->get();
  }

  private function HandleUploadinventory($req, $id)
  {
    $AdminController = new AdminController();
    foreach ($AdminController->foto_inventory as $key => $raw_data) {
      for ($x = 0; $x <= $raw_data['Evidence']; $x++){
        $input = 'photo-' . $key . '_'. $x;
        $name = $key . '_'. $x;
        $select_tipe = 'select_tipe_'. $key . '_'. $x;
        $select_stts = 'select_stts_'. $key . '_'. $x;
        $select_spbu = 'select_spbu_'. $key . '_'. $x;
        $catatan_inven = 'catatan_inven_' . $key . '_'. $x;
        $path = public_path() . '/upload/spbu/inventory/' . $id;
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)) {
            return 'gagal menyiapkan folder foto evidence';
          }
        }

        $collect_value = [$req->$select_tipe => $x.'-tipe.txt', $req->$select_stts => $x.'-status.txt', $req->$select_spbu => $x.'-spbu.txt', $req->$catatan_inven => $x.'-inven.txt'];

        foreach($collect_value as $isi_data => $cv){
          if ($isi_data != '') {
            file_put_contents($path.'/'.$key.'_'.$cv, $isi_data);
          }
        }

        if ($req->hasFile($input)) {
          $file = $req->file($input);
          $ext = 'jpg';
          try {
            $moved = $file->move("$path", "$name.$ext");

            $img = new \Imagick($moved->getRealPath());

            $img->scaleImage(100, 150, true);
            $img->writeImage("$path/$name-th.$ext");
          } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
            return 'gagal menyimpan foto evidence ' . $name;
          }
        }
      }
    }
  }

  public static function update_inventory($id, $req){
    DB::table('spbu_inventory_log')->insert([
      'id_spbu' => $id,
      'naker' => session('auth')->id_user
    ]);

    $that = new AdminModel();

    $matches = [];

    foreach (array_keys($req->all()) as $str){
      if (preg_match ('/^photo-(\w+)/i', $str, $m))
      {
        $matches[] = $m[1];
      }
    }

    if($matches)
    {
      foreach($matches as $val)
      {
        $messages[] = ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan foto '.$val.' batasnya adalah 5MB'];
        $rules['photo-'.$val] = 'image|mimes:jpeg,png,jpg,gif,svg|max:5000';
      }

      $validator = \Validator::make($req->all(), $rules);
      // dd($messages, $rules, $validator->fails());

      if ($validator->fails()) {
        return redirect()->back()
        ->withInput($req->all())
        ->withErrors($validator)
        ->with('alerts', $messages);
      }
    }

    $that->HandleUploadinventory($req, $id);
  }

  public static function log_inven($id){
    return DB::table('spbu_inventory_log')
    ->leftjoin('1_2_employee', 'naker', '=', 'nik')
    ->select(DB::RAW("TRIM(CONCAT(COALESCE(nama, ''), ' (', naker, ')')) as nama_naker"), 'spbu_inventory_log.*')
    ->where('spbu_inventory_log.id_spbu', $id)
    ->orderBy('spbu_inventory_log.created_at', 'DESC')
    ->get();
  }

  public static function data_download($jenis, $date1, $date2)
  {
    if($jenis == 'Pengerjaan'){
      $data = DB::TABLE('spbu_dispatch as sd')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->leftjoin('1_2_employee as k2', 'k2.nik', '=', 'sa.naker')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa.id as id_adds', 'sd.teknisi as id_user', 'sa.naker as naker_id', 'k2.nama as naker_nama', 'k.nama')
      ->whereBetween(DB::RAW('DATE(modifiet_at)'), [$date1, $date2])->get();
    }elseif($jenis == 'Perangkat Rusak'){
      $data = DB::TABLE('spbu_perangkat_rusak as sd')
      ->leftjoin('spbu_perangkat_rusak as sd_2', 'sd_2.parent', '=', 'sd.id')
      ->leftjoin('spbu_alamat as sa', 'sd.id_spbu_add', '=', 'sa.id')
      ->leftjoin('spbu_alamat as sa_2', 'sd_2.id_spbu_add', '=', 'sa_2.id')
      ->leftjoin('1_2_employee as k', 'k.nik', '=', 'sd.teknisi')
      ->leftjoin('1_2_employee as k2', 'k2.nik', '=', 'sa.naker')
      ->select('sd.*', 'sa.type', 'sa.id_spbu', 'sa.alamat', 'sa.kota', 'sa_2.id_spbu as alamat_relok', 'sa.id as id_adds', 'sd.teknisi as id_user', 'sa.naker as naker_id', 'k2.nama as naker_nama', 'k.nama')
      ->whereBetween(DB::RAW('DATE(sd.modify_at)'), [$date1, $date2])
      ->get();
    }
    return $data;
  }

  public static function list_teknisi()
  {
    return DB::table('user')
    ->leftjoin('1_2_employee as k', 'k.nik', '=', 'id_user')
    ->select('user.*', 'k.nama', DB::RAW("(CASE WHEN spbu_level = 1 THEN 'Admin' WHEN spbu_level IN (2, 4) THEN 'Teknisi' ELSE 'Monitor' END) As detail_level"))
    ->where('spbu_level', '!=', 0)->get();
  }

  public static function list_mitra()
  {
    return DB::table('mitra_amija')
			->select('mitra_amija as id', 'mitra_amija as text')
      ->where('witel', 'KALSEL')
			->get();
  }

  public static function cek_user($id, $jenis = 'new')
	{
    if($jenis == 'new'){
      return DB::table('user')->leftjoin('1_2_employee', '1_2_employee.nik', '=', 'user.id_user')
			->where('id_user', $id)->first();
    }else{
      $recheck = DB::table('user')->leftjoin('1_2_employee', '1_2_employee.nik', '=', 'user.id_user')
			->where('id_user', $id)->first();
      if($recheck->id_user == $id ){
        return null;
      }else{
        return $recheck;
      }
    }
	}

  // public static function save_user($req)
	// {
	// 	$cek_user = DB::table('user')->Where('id_user', $req->nik)
	// 		->first();

	// 	if ($cek_user)
	// 	{
	// 		return redirect('/')->with('alerts', [['type' => 'success', 'text' => "Data Sudah Ada!"]]);
	// 	}
	// 	else
	// 	{
	// 		$data['id_user'] = $req->nik;
	// 		$data['password'] = md5($req->pass);
	// 		$data['spbu_level'] = $req->job;
	// 		$data['level'] = 10;
	// 		$data['id_karyawan'] = $req->nik;
	// 		$data['witel'] = 1;
	// 		$data['psb_remember_token'] = md5($req->nik . microtime());
	// 		$data['updated_user'] = Session::get('auth')->id_user;
	// 		$data['datetime_updated'] = date('Y-m-d H: i: s');

	// 		DB::table('user')->insert($data);

	// 		DB::table('karyawan')->insert(['id_karyawan' => $req->nik, 'nama' => $req->nama, 'nama_instansi' => $req->mitra]);
	// 	}
	// }

	// public static function update_user($req, $id)
	// {
  //   // dd($req->all(), $id);
	// 	if (!empty($req->pass))
	// 	{
	// 		$data['password'] = md5($req->pass);
	// 	}

	// 	$data['spbu_level'] = $req->job;
	// 	$data['id_karyawan'] = $req->nik;
	// 	$data['psb_remember_token'] = md5($req->nik . microtime());
	// 	$data['datetime_updated'] = date('Y-m-d H: i: s');
	// 	$data['updated_user'] = Session::get('auth')->id_user;

	// 	DB::table('user')
	// 		->Where('id_user', $id)->update($data);

	// 	DB::table('karyawan')->Where('id_karyawan', $id)->update(['id_karyawan' => $req->nik, 'nama' => $req->nama, 'nama_instansi' => $req->mitra]);
	// }

  public static function delete_data($jenis, $id)
  {
    if($jenis == 'tiket')
    {
      $table = 'spbu_dispatch';
    }
    else
    {
      $table = 'spbu_perangkat_rusak';
    }

    DB::table($table)->where('id', $id)->update([
      'active' => 0
    ]);
  }

  public static function get_all_witel()
  {
    return DB::table('f_witel')->where('witel_nas', '!=', 'UNK WITEL')->get();
  }
}

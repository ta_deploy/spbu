<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\LoginController;
use Telegram;
use GuzzleHttp\Client;


date_default_timezone_set("Asia/Makassar");
class LoginModel
{
	public function login($user, $pass){
		return DB::select('SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.spbu_level, u.psb_remember_token, r.mainsector, u.maintenance_level
		FROM user u
    LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
    LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
		WHERE id_user = ? AND password = MD5(?)
		GROUP BY u.id_user', [$user, $pass]);
	}

	public function remembertoke($localUser, $token){
		return DB::table('user')
		->where('id_user', $localUser->id_user)
		->update([
			'psb_remember_token' => $token
		]);
	}

	public static function absen_teknsi($req){
    $session = session('auth');
    $botToken="1893068284:AAHku9n4saLvLwnedoGQY_ATBkrjEBm45Fg";
    $website="https://api.telegram.org/bot".$botToken;

		DB::transaction(function () {
      DB::table('absen')->insert([
        'nik'    => session('auth')->id_user,
        'divisi' => 'SPBU',
      ]);

      DB::table('spbu_absen_log')->insert([
        'naker_absen' => session('auth')->id_user,
        'status'      => 0,
        'created_by'  => session('auth')->id_user
      ]);
		});

    $check_teknisi = DB::table('user as u')
    ->leftjoin('1_2_employee as k', 'k.nik', '=', 'u.id_user')
    ->select('u.id_user', 'k.nama')
    ->where('id_user', $session->id_user)
    ->first();

    $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

		$message = "Teknisi $teknisi_nm absen! Segera TL melakukan Verifikasi \n";
    $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);
    $chat_id = '-1001152815513';
    // $chat_id = '519446576';

    $params=[
      'chat_id' => $chat_id,
      'parse_mode' => 'html',
      'text' => "$message",
    ];
    $ch = curl_init();
    $optArray = array(
      CURLOPT_URL => $website.'/sendMessage',
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $params,
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);

    $that = new LoginModel();

    $that->handleFileabsensi($req, $session->id_user);

    $LoginController = new LoginController();

    foreach ($LoginController->foto as $pht)
    {
      $public = public_path() . '/upload2/spbu/absensi/' . $session->id_user;
      $file = $public . "/" . $pht . ".jpg";
      $photo_caption = str_replace('_', ' ', $pht);
      // $params_photo=[
      //   'chat_id' => $chat_id,
      //   'caption' => "Label $photo_caption \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
      //   'photo' => new \CURLFile($file),
      // ];
      // $ch = curl_init();
      // $optArray_photo = array(
      //   CURLOPT_URL => $website.'/sendPhoto',
      //   CURLOPT_RETURNTRANSFER => 1,
      //   CURLOPT_POST => 1,
      //   CURLOPT_POSTFIELDS => $params_photo,
      //   CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
      // );
      // curl_setopt_array($ch, $optArray_photo);
      // $result = curl_exec($ch);
      // curl_close($ch);

      $chatId = $chat_id;
      $photoPath = $file;

      $client = new Client([
          'base_uri' => "https://api.telegram.org/bot{$botToken}/",
      ]);

      $response = $client->post('sendPhoto', [
        'multipart' => [
          [
            'name' => 'chat_id',
            'contents' => $chatId,
          ],
          [
            'name' => 'photo',
            'contents' => fopen($photoPath, 'r'),
            'filename' => $pht . ".jpg",
          ],
          [
            'name' => 'caption',
            'contents' => "Label $photo_caption \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
          ],
        ],
      ]);

      // Handle the response as per your requirement
    }
  }

	private function handleFileabsensi($req, $id)
  {
    $LoginController = new LoginController();
    foreach ($LoginController->foto as $name) {
      $input = 'photo-' . $name;
      $path = public_path() . '/upload2/spbu/absensi/' . $id;

      if ($req->hasFile($input)) {
        $file = $req->file($input);
        $ext = 'jpg';
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());

          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence ' . $name;
        }
      }
    }
  }

  public static function log_user($id){
    return DB::table('spbu_absen_log')->where('naker_absen', $id)->OrderBy('created_at', 'DESC')->first();
  }
}

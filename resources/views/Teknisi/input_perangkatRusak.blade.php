@extends('layout')
@section('title', !empty(@$data) ? 'Edit Perangkat Rusak' : 'Input Perangkat Rusak')
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style type="text/css">
    .select2-results__option, select{
        color: black;
    }

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }

    #map {
        position: absolute; top: 0; bottom: 0; width: 90vw; height: 100%;
    }

    .mapboxgl-canvas {
        left: 0;
    }

    .modal-xl{
        width: 100%;
        max-width:1300px;
    }

    .modal-body{
        height: 400px;
    }

    .foto {
        border: 3px solid white;
        resize: none;

        margin-bottom: 4px;
    }

    .coordinates {
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        position: absolute;
        bottom: 40px;
        left: 10px;
        padding: 5px 10px;
        margin: 0;
        font-size: 11px;
        line-height: 18px;
        border-radius: 3px;
        display: none;
    }

</style>
@endsection
@section('content')
<div class="modal fade" id="modal-large" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Find Me</h4>
            </div>
            <div class="modal-body">
                <div id="map"></div>
                <pre id="coordinates" class="coordinates"></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
            </div>
        </div>
    </div>
</div>
<div class="body">
    <div class="row">
        <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data">
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-check-square-o"></i>
                            Pengisian Data Tiket Perangkat Rusak
                            @if (!empty(@$data->parent))
                                <code>Tiket Berasal Dari Relokasi SPBU <b>{{ $data->relok_spbu }}</b></code>
                            @endif
                        </h4>
                    </header>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{-- pra kontrak --}}
                    <fieldset>
                        <legend class="section">Perangkat Rusak</legend>
                        @if(in_array(session('auth')->spbu_level, [1, 2, 4]))
                            <div class="form-group">
                                <label class="control-label col-md-5" for="teknisi">Teknisi</label>
                                <div class="col-md-7">
                                    @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                        <input type="text" disabled class="form-control input-transparent" value="{{ $data->teknisi }}">
                                    @else
                                        <select id="teknisi" name="teknisi" class="admin_select selectpicker"
                                        data-style="btn-default" data-width="auto"
                                        data-parsley-trigger="change"
                                        data-parsley-required-message="Pilih Teknisi yang Mengerjakan!">
                                        @if(@$data->teknisi)
                                            <option value="{{ $data->teknisi }}">{{ $data->teknisi }}</option>
                                        @endif
                                        </select>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_spbu">Id SPBU</label>
                            <div class="col-md-7">
                                @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                        <input type="text" disabled class="form-control input-transparent" value="{{ $data->id_spbu }}">
                                @else
                                    <select id="id_spbu" name="id_spbu" class="admin_select selectpicker"
                                    data-style="btn-default" data-width="auto"
                                    data-parsley-trigger="change"
                                    data-parsley-required-message="Pilih ID SPBU nya!">
                                    @if(@$data->id_spbu)
                                        <option value="{{ $data->id_spbu }}">{{ $data->id_spbu }}</option>
                                    @endif
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="sto">Sto</label>
                            <div class="col-md-7">
                                <input type="text" id="sto" name="sto" class="form-control admin input-transparent" readonly="readonly" style="cursor: not-allowed;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_alamat">Alamat</label>
                            <div class="col-md-7">
                                <input type="text" id="id_alamat" name="id_alamat" class="form-control admin input-transparent" readonly="readonly" style="cursor: not-allowed;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_status">Status</label>
                            <div class="col-md-7">
                                <input type="text" id="id_status" name="id_status" class="form-control admin input-transparent" readonly="readonly" style="cursor: not-allowed;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_status">Nomor Tiket</label>
                            <div class="col-md-7">
                                <input type="text" id="no_tiket" {{ !in_array(session('auth')->spbu_level, [1, 2, 4]) ? 'disabled' : '' }} name="no_tiket" class="form-control admin input-transparent" value="{{ @$data->no_tiket ?? '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="file_ba">File BA perangkat Rusak</label>
                            <div class="col-md-7">
                                @if (in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="file" id="file_ba" name="file_ba" class="form-control input-transparent">
                                    @php
                                    $p_raw1 = public_path() . '/upload/spbu/broken_part/' . $id . '/BA/';
                                    $p_raw2 = public_path() . '/upload2/spbu/broken_part/' . $id . '/BA/';

                                    if(file_exists($p_raw1) )
                                    {
                                        $path = $p_raw1;
                                    }
                                    else
                                    {
                                        $path = $p_raw2;
                                    }

                                    if (file_exists($path)) {
                                        $files = preg_grep('~^File BA.*$~', scandir($path));
                                        $files = array_values($files);
                                        $real_path = '/upload/spbu/broken_part/' . $id . '/BA/'.$files[0];
                                    }
                                    @endphp
                                @else
                                    @php
                                    $p_raw1 = public_path() . '/upload/spbu/broken_part/' . $id . '/BA/';
                                    $p_raw2 = public_path() . '/upload2/spbu/broken_part/' . $id . '/BA/';

                                    if(file_exists($p_raw1) )
                                    {
                                        $path = $p_raw1;
                                    }
                                    else
                                    {
                                        $path = $p_raw2;
                                    }

                                    if (file_exists($path)) {
                                        $files = preg_grep('~^File BA.*$~', scandir($path));
                                        $files = array_values($files);
                                        $real_path = '/upload/spbu/broken_part/' . $id . '/BA/'.$files[0];
                                    }
                                    @endphp
                                    @if (@!$real_path)
                                        File Belum Diupload!
                                    @endif
                                @endif
                                @if (@$real_path)
                                    File Bisa Diunduh <a href="/Spbu/download_BA/broken_part/{{ $id }}">Disini</a>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="perangkat_rusak">Perangkat Rusak</label>
                            <div class="col-md-7">
                                @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="text" disabled class="form-control input-transparent" value="{{ str_replace('_', ' ', $data->broken_part) }}">
                                @else
                                    <select id="perangkat_rusak" name="perangkat_rusak" data-style="btn-default" data-width="auto" data-parsley-trigger="change" data-parsley-required-message="Pilih Perangkat Rusak!" class="admin_select selectpicker">
                                        @foreach ($broken_part as $item)
                                            <option value="{{ $item }}">{{ str_replace('_', ' ', $item) }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group tipe_perangkat_dv">
                            <label class="control-label col-md-5" for="tipe_perangkat">Tipe Rusak</label>
                            <div class="col-md-7">
                                @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="text" disabled class="form-control input-transparent" value="{{ $data->jenis_perangkat }}">
                                @else
                                    <select id="tipe_perangkat" data-style="btn-default" data-width="auto" data-parsley-trigger="change" data-parsley-required-message="Pilih Tipe Rusak!" name="tipe_perangkat" class="admin_select selectpicker">
                                        @if(@$data->id_spbu)
                                        <option value="{{ $data->jenis_perangkat }}">{{ $data->jenis_perangkat }}</option>
                                    @endif
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="koor">Lokasi Perangkat rusak</label>
                            <div class="col-md-7">
                                @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="text" disabled class="form-control input-transparent" value="{{ $data->lokasi }}">
                                @else
                                   <select id="koor" name="koor" class="selectpicker input-transparent" data-style="btn-default" data-width="auto">
                                        <option value="Ditinggal di SPBU" {{ @$data->lokasi == 'Ditinggal di SPBU' ? 'selected' : '' }}>Ditinggal di SPBU</option>
                                        <option value="Diantar ke CCAN" {{ @$data->lokasi == 'Diantar ke CCAN' ? 'selected' : '' }}>Diantar ke CCAN</option>
                                        <option value="Team IM Balikpapan Reg 6" {{ @$data->lokasi == 'Team IM Balikpapan Reg 6' ? 'selected' : '' }}>Team IM Balikpapan Reg 6</option>
                                        <option value="Kantor TA Nagasari Office" {{ @$data->lokasi == 'Kantor TA Nagasari Office' ? 'selected' : '' }}>Kantor TA Nagasari Office</option>
                                        <option value="Teknisi Territory" {{ @$data->lokasi == 'Teknisi Territory' ? 'selected' : '' }}>Teknisi Territory</option>
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="change_part">Pergantian Perangkat</label>
                            <div class="col-md-7">
                                @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="text" disabled class="form-control input-transparent" value="{{ $data->change_part }}">
                                @else
                                    <select id="change_part" name="change_part" class="selectpicker input-transparent" data-style="btn-default" data-width="auto">
                                        <option value="NOK/Belum di ganti" {{ @$data->change_part == 'NOK/Belum di ganti' ? 'selected' : '' }}>NOK/Belum di ganti</option>
                                        <option value="Dari SDA/Logistik SPBU" {{ @$data->change_part == 'Dari SDA/Logistik SPBU' ? 'selected' : '' }}>Dari SDA/Logistik SPBU</option>
                                        <option value="Dari Relokasi SPBU" {{ @$data->change_part == 'Dari Relokasi SPBU' ? 'selected' : '' }}>Dari Relokasi SPBU</option>
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group id_spbu_relok_div">
                            <label class="control-label col-md-5" for="id_spbu_relok">Relokasi Dari SPBU</label>
                            <div class="col-md-7">
                                @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="text" disabled class="form-control input-transparent" value="{{ $data->relok_spbu }}">
                                @else
                                    <select id="id_spbu_relok" name="id_spbu_relok" class="admin_select selectpicker" data-style="btn-default" data-width="auto" data-parsley-trigger="change" data-parsley-required-message="Pilih Relokasi SPBU nya!">
                                        @if(@$data->id_spbu)
                                            <option value="{{ $data->relok_spbu }}">{{ $data->relok_spbu }}</option>
                                        @endif
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="note">Catatan</label>
                            <div class="col-md-7">
                                <textarea rows="4" {{ !in_array(session('auth')->spbu_level, [1, 2, 4]) ? 'disabled' : '' }} style="resize:none;" cols="50" id="note" name="note" class="form-control input-transparent"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Catatan Isi!"
                                >{{ !empty($data->note) ? $data->note : '' }}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    @if ( session('auth')->id_user == 18940469 || in_array(session('auth')->spbu_level, [1, 2, 4]))
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn_sbmt btn-danger">Validate &amp; Submit</button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    @endif
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-file-image-o"></i>
                            Upload Foto Berita Acara
                        </h4>
                    </header>
                    <fieldset>
                        <legend class="section">Foto Perangkat</legend>
                        <div class="row text-center input-photos" style="margin: 20px 0">
                            <?php
                                $number = 1;
                                clearstatcache();
                                if(@$data->parent){
                                    unset($part[1]);
                                }
                            ?>
                            @foreach($part as $input)
                            <?php
                                if(@$data->parent){
                                    $p_raw1 = "/upload/spbu/broken_part/$id/$input";
                                    $p_raw2 = "/upload2/spbu/broken_part/$id/$input";

                                    if(file_exists($p_raw1) )
                                    {
                                        $path_f = $p_raw1;
                                    }
                                    else
                                    {
                                        $path_f = $p_raw2;
                                    }


                                    if(in_array($input, ['berita_acara_perangkat_relokasi','berita_acara_pergantian_perangkat', 'perangkat_relokasi', 'berita_acara_pergantian_perangkat_relokasi']))
                                    {

                                        if(file_exists(public_path()."$path_f-th.jpg"))
                                        {
                                            $p_raw1 = "/upload/spbu/broken_part/$id/$input";
                                            $p_raw2 = "/upload2/spbu/broken_part/$id/$input";

                                            if(file_exists($p_raw1) )
                                            {
                                                $path_f = $p_raw1;
                                            }
                                            else
                                            {
                                                $path_f = $p_raw2;
                                            }
                                        }
                                        else
                                        {
                                            $p_raw1 = "/upload/spbu/broken_part/$data->parent/$input";
                                            $p_raw2 = "/upload2/spbu/broken_part/$data->parent/$input";

                                            if(file_exists($p_raw1) )
                                            {
                                                $path_f = $p_raw1;
                                            }
                                            else
                                            {
                                                $path_f = $p_raw2;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    $p_raw1 = "/upload/spbu/broken_part/$id/$input";
                                    $p_raw2 = "/upload2/spbu/broken_part/$id/$input";

                                    if(file_exists($p_raw1) )
                                    {
                                        $path_f = $p_raw1;
                                    }
                                    else
                                    {
                                        $path_f = $p_raw2;
                                    }
                                }
                                // dd($path_f);
                                $th   = "$path_f-th.jpg";
                                $img  = "$path_f.jpg";
                                $nt   = "$path_f-catatan.txt";
                                $ganti_ba = "$path_f-selectba.txt";
                                $flag = $foto_css = $note_tl = $img_cs = "";
                                $name = "flag_".$input;

                                if(in_array($input, ['perangkat_rusak', 'berita_acara_kerusakan'])){
                                    $foto_css .=' foto_standar ';
                                    $note_tl .=' cttn_standar ';
                                    $img_cs .=' img_standar ';
                                }

                                if(in_array($input, ['pergantian_perangkat', 'berita_acara_pergantian_perangkat', 'perangkat_rusak', 'perangkat_baru', 'berita_acara_kerusakan'])){
                                    $foto_css .=' foto_sda ';
                                    $note_tl .=' cttn_sda ';
                                    $img_cs .=' img_sda ';
                                }

                                if(in_array($input, ['perangkat_rusak', 'pergantian_perangkat_relokasi', 'berita_acara_perangkat_rusak', 'berita_acara_pergantian_perangkat', 'berita_acara_pergantian_perangkat_relokasi', 'perangkat_relokasi', 'berita_acara_perangkat_relokasi'])){
                                    $foto_css .=' foto_relok ';
                                    $note_tl .=' cttn_relok ';
                                    $img_cs .=' img_relok ';
                                }
                            ?>
                            @if (file_exists(public_path().$th))
                                <?php
                                $flag = 1;
                                $images = $img;
                                $src = $th;
                                ?>
                            @else
                                <?php
                                $images = "";
                                $src = "/img/placeholder.gif";
                                ?>
                            @endif
                            <div class="col-6 col-sm-5 {{ $input }} {{ $foto_css }}">
                                <a href="{{ $images }}">
                                    <img src="{{ $src }}" alt="{{ $input }}" id="img-{{ $input }}" class="photo_valid_dis {{ $img_cs }}" />
                                </a>
                                <br />
                                <input type="text" class="hidden" name="flag_{{ $input }}" value="" />
                                <input type="file" class="hidden select_photo_part " name="photo-{{ $input }}" accept="image/jpeg" />
                                @if (in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <button type="button" class="btn btn-sm btn-info">
                                        <i class="glyphicon glyphicon-camera"></i>
                                    </button>
                                @endif
                                <p style="margin-bottom: 3px;">{{ ucwords(str_replace('_',' ',$input)) }}</p>
                                @if (file_exists(public_path().$nt))
                                    <?php $note = File::get(public_path($nt)); ?>
                                @else
                                    <?php $note ='' ?>
                                @endif
                                @if (($input == 'perangkat_relokasi'))

                                    @if (!in_array(session('auth')->spbu_level, [1, 2, 4]))
                                    <input type="text" disabled class="form-control input-transparent" value="">
                                    @else
                                    <select id="pergantian_relok" name="pergantian_relok" class="admin_select selectpicker" data-style="btn-default" data-width="auto" style="margin-bottom: 3px; display: none;">
                                        <option value="Belum Diganti" {{ @$data->pergantian_relok == 'Belum Diganti' ? 'selected' : ''  }}>Belum Diganti</option>
                                        <option value="Sudah Diganti" {{ @$data->pergantian_relok == 'Sudah Diganti' ? 'selected' : ''  }}>Sudah Diganti</option>
                                        </select>
                                    @endif
                                @endif
                                <textarea class="form-control foto {{ $note_tl }} " id="catatan_{{$input}}" name="catatan_{{$input}}">{{$note}}</textarea>
                                {!! $errors->first("flag_".$input, '<span class="label label-danger">:message</span>') !!}
                            </div>
                            <?php
                            $number++;
                            ?>
                            @endforeach
                        </div>
                    </fieldset>
                </section>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js"></script>
<script type="text/javascript">
    function isObjectEmpty(obj) {
        for ( var name in obj ) {
            return false;
        }
        return true;
    }

    $(function(){

        $('.foto_standar, .foto_sda, .foto_relok').css({'display': 'none'});

        var raw_class_photo = ['.foto_standar', '.foto_sda', '.foto_relok'];

        var data = <?= json_encode($data) ?>;

        if(!isObjectEmpty(data)){
            console.log(data)
            if(data.change_part == 'Dari Relokasi SPBU'){
                $('.berita_acara_perangkat_relokasi, .berita_acara_pergantian_perangkat, .perangkat_relokasi, .berita_acara_pergantian_perangkat_relokasi').removeClass('foto_relok');

                $('.berita_acara_perangkat_relokasi, .berita_acara_pergantian_perangkat, .perangkat_relokasi, .berita_acara_pergantian_perangkat_relokasi').css({'display': 'block'});
            }

            if(data.status_ganti_relok != ''){
                $('#pergantian_relok').css({'display': 'inline'});
            }
        }

        if($('#change_part').val() == 'Dari Relokasi SPBU'){
            var value = '.foto_relok';
            class_photo = raw_class_photo.filter(function(item) {
                return item !== value
            });

            $(class_photo.join(',')).css({'display': 'none'});
            $(class_photo.join(',')).attr('src', '/img/placeholder.gif');
            $(class_photo.join(',')).val('');
            $('.foto_relok').css({'display': 'block'});

            $('.id_spbu_relok_div').css({'display': 'block'});
            // $('#koor').parsley()
            $('#id_spbu_relok').attr('data-parsley-validate', 'true');
            $('#id_spbu_relok').attr('required', true);
        } else{
            $('.id_spbu_relok_div').css({'display': 'none'});
            $('#id_spbu_relok').val('').change();
            //hapus ui parsley
            $('#id_spbu_relok').attr('data-parsley-validate', 'false');
            $('#id_spbu_relok').attr('required', false);
            if(document.getElementById('id_spbu_relok')){
                $('#id_spbu_relok').parsley().reset();
            }
        }

        if($('#change_part').val() == 'NOK/Belum di ganti'){
            var value = '.foto_standar';
            class_photo = raw_class_photo.filter(function(item) {
                return item !== value
            });

            $(class_photo.join(',')).css({'display': 'none'});
            $(class_photo.join(',')).attr('src', '/img/placeholder.gif');
            $(class_photo.join(',')).val('');
            $('.foto_standar').css({'display': 'block'});
        }

        if($('#change_part').val() == 'Dari SDA/Logistik SPBU'){
            var value = '.foto_sda';
            class_photo = raw_class_photo.filter(function(item) {
                return item !== value
            });
            $(class_photo.join(',')).css({'display': 'none'});
            $(class_photo.join(',')).attr('src', '/img/placeholder.gif');
            $(class_photo.join(',')).val('');
            $('.foto_sda').css({'display': 'block'});
        }

        $('#change_part').on('change', function(){
            if($('#change_part').val() == 'Dari Relokasi SPBU'){
                var value = '.foto_relok';
                class_photo = raw_class_photo.filter(function(item) {
                    return item !== value
                });

                $(class_photo.join(',')).css({'display': 'none'});
                $(class_photo.join(',')).attr('src', '/img/placeholder.gif');
                $(class_photo.join(',')).val('');
                console.log(class_photo.join(','))
                $('.foto_relok').css({'display': 'block'});

                $('.id_spbu_relok_div').css({'display': 'block'});
                // $('#koor').parsley()
                $('#id_spbu_relok').attr('data-parsley-validate', 'true');
                $('#id_spbu_relok').attr('required', true);
            } else{
                $('.id_spbu_relok_div').css({'display': 'none'});
                $('#id_spbu_relok').val('').change();
                //hapus ui parsley
                $('#id_spbu_relok').attr('data-parsley-validate', 'false');
                $('#id_spbu_relok').attr('required', false);
                if(document.getElementById('id_spbu_relok')){
                    $('#id_spbu_relok').parsley().reset();
                }
            }

            if($('#change_part').val() == 'NOK/Belum di ganti'){
                var value = '.foto_standar';
                class_photo = raw_class_photo.filter(function(item) {
                    return item !== value
                });

                $(class_photo.join(',')).css({'display': 'none'});
                $(class_photo.join(',')).attr('src', '/img/placeholder.gif');
                $(class_photo.join(',')).val('');
                console.log(class_photo.join(','))
                $('.foto_standar').css({'display': 'block'});
            }

            if($('#change_part').val() == 'Dari SDA/Logistik SPBU'){
                var value = '.foto_sda';
                class_photo = raw_class_photo.filter(function(item) {
                    return item !== value
                });
                $(class_photo.join(',')).css({'display': 'none'});
                $(class_photo.join(',')).attr('src', '/img/placeholder.gif');
                $(class_photo.join(',')).val('');
                console.log(class_photo.join(','))
                $('.foto_sda').css({'display': 'block'});
            }
        });

        $('#perangkat_rusak').select2({
            placeholder: "Pilih Perangkat Rusak",
            width: '100%'
        });

        $('#perangkat_rusak').val(null).change();
        $('.tipe_perangkat_dv').css({'display': 'none'});

        if(document.getElementById('perangkat_rusak')){
            $('#perangkat_rusak').on('select2:select', function(){
                var data_pr;
                $.ajax({
                    url: "/brokenpart/get_tipe",
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        data: $(this).val()
                    },
                    success: function(res) {
                        data_pr = res;
                        $('.tipe_perangkat_dv').css({'display': 'block'});
                        $('#tipe_perangkat').empty().change();
                        $('#tipe_perangkat').select2({
                            width: '100%',
                            placeholder: "Pilih Tipe Perangkat",
                            data: data_pr,
                        });
                    }
                });
            });
        }

        $('#teknisi').select2({
            width: '100%',
            placeholder: "Pilih Teknisi",
            minimumInputLength: 7,
            allowClear: true,
            ajax: {
                url: "/input/teknisi/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                language: {
                    noResults: function(){
                        return "User Tidak Terdaftar!";
                    }
                },
                cache: true,
                success: function(value) {

                }
            }
        });

        $('#id_spbu').select2({
            width: '100%',
            placeholder: "Pilih ID SPBU",
            minimumInputLength: 3,
            allowClear: true,
            ajax: {
            url: "/input/spbu_id/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true,
                success: function(value) {

                }
            }
        }).on("select2:open", function() {
            $(".select2-search__field").val('');
            $(".select2-search__field").attr('type', 'text');
            // $(".select2-search__field").attr('style', 'width: 10em;');
            $(".select2-search__field").inputmask({
                mask: '99-9{6,8}',
                placeholder: ''
            });
        }).on("select2:close", function() {
            $(".select2-search__field").inputmask('remove');
            $(".select2-search__field").val(null);
            $('#id_alamat').val(null);
            $('#id_status').val(null);
            $('#sto').val(null);
            $('#teknisi').empty().trigger('change');
        });

        $('#id_spbu_relok').select2({
            width: '100%',
            placeholder: "Pilih ID SPBU",
            minimumInputLength: 3,
            allowClear: true,
            ajax: {
                url: "/input/spbu_id/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true,
                success: function(value) {

                }
            }
        }).on("select2:open", function() {
            $(".select2-search__field").val('');
            $(".select2-search__field").attr('type', 'text');
            // $(".select2-search__field").attr('style', 'width: 10em;');
            $(".select2-search__field").inputmask({
                mask: '99-9{6,8}',
                placeholder: ''
            });
        })

        $('#id_spbu').on('select2:select', function (e) {
            $.ajax({
                type: 'GET',
                url: '/input/alamat_stts/search',
                dataType: 'json',
                data: {id_spbu: $('#id_spbu').val() },
                success: function(value) {
                    $('#id_alamat').val(value.alamat);
                    $('#id_status').val(value.status);
                    $('#sto').val(value.sto);
                    $('#teknisi').append($("<option selected='selected'></option>").val(value.naker).text(value.nama+' ('+value.naker+')')).trigger('change');
                }
            });
        });

        $('#berita_ac_sel').val(0).change();

        if(!isObjectEmpty(data)){
            if ($('#pergantian_ba').attr('data-val') == 'Dari BA Pergantian'){
                $(this).val('Dari BA Pergantian').trigger('change');
            }

            $('#perangkat_rusak').val(data.broken_part).change();

            if(document.getElementById('perangkat_rusak')){
                $.ajax({
                    url: "/brokenpart/get_tipe",
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        data: $('#perangkat_rusak').val()
                    },
                    success: function(res) {
                        data_pr = res;
                        $('.tipe_perangkat_dv').css({'display': 'block'});
                        $('#tipe_perangkat').empty().change();
                        $('#tipe_perangkat').select2({
                            width: '100%',
                            placeholder: "Pilih Tipe Perangkat",
                            data: data_pr,
                        });
                    }
                });
            }

            $('.tipe_perangkat_dv').css({'display': 'block'});

            $('#tipe_perangkat').val(data.jenis_perangkat).change();

            $.ajax({
                type: 'GET',
                url: '/input/alamat_stts/search',
                dataType: 'json',
                data: {id_spbu: data.id_spbu},
                success: function(value) {
                    $('#id_alamat').val(value.alamat);
                    $('#id_status').val(value.status);
                    $('#sto').val(value.sto);
                }
            });
        }

        $('input[type=file]').change(function() {
            inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                $(inputEl).parent().find('input[type=text]').val(1);
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);

                }
                reader.readAsDataURL(inputEl.files[0]);
            }
        });

        $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();

            console.log($(this).parent().find('img').attr('src'))
        });

        $('.select_photo_part').on('change', function(){
            if($(this).parent().find('input[type=file][name=photo-perangkat_relokasi]').length){
                console.log('tes')
                $('#pergantian_relok').css({'display': 'inline'});
            }
        });

        $("#validation-form").on('submit', function() {
            $(this).parsley().validate();

            if ($(this).parsley().isValid()) {
                $('.btn_sbmt').attr('disabled');
            }
        });
    });
</script>
@endsection
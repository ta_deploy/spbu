@extends('layout')
@section('title', (empty($data) ? 'Input Data SPBU' : 'Edit Data'))
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style type="text/css">
    .select2-results__option, select{
        color: black;
    }

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }

    #map {
        position: absolute; top: 0; bottom: 0; width: 90vw; height: 100%;
    }

    .mapboxgl-canvas {
        left: 0;
    }

    .modal-xl{
        width: 100%;
        max-width:1300px;
    }

    .modal-body{
        height: 400px;
    }

    .foto {
        border: 3px solid white;
        resize: none;

        margin-bottom: 4px;
    }

    .coordinates {
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        position: absolute;
        bottom: 40px;
        left: 10px;
        padding: 5px 10px;
        margin: 0;
        font-size: 11px;
        line-height: 18px;
        border-radius: 3px;
        display: none;
    }

</style>
@endsection
@section('content')
<div class="modal fade" id="modal-large" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Find Me</h4>
            </div>
            <div class="modal-body">
                <div id="map"></div>
                <pre id="coordinates" class="coordinates"></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
            </div>
        </div>
    </div>
</div>
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
    <div class="row">
        <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-check-square-o"></i>
                            Pengisian Data Tiket SPBU
                        </h4>
                    </header>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{-- pra kontrak --}}
                    <fieldset>
                        <legend class="section">Tiket SPBU</legend>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id_spbu">Id SPBU</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control input-transparent" style="cursor: not-allowed;" disabled value="{{ $data->id_spbu }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="sto">Sto</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control input-transparent" style="cursor: not-allowed;" disabled value="{{ $data->kota }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id_alamat">Alamat</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control input-transparent" style="cursor: not-allowed;" disabled value="{{ $data->alamat }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id_status">Status</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control input-transparent" style="cursor: not-allowed;" disabled value="{{ $data->status }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="jenis_g">Jenis Gangguan</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control input-transparent" style="cursor: not-allowed;" disabled value="{{ $data->jenis_g }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="act">Action</label>
                            <div class="col-md-10">
                                <select id="act" name="act" class="act selectpicker" data-style="btn-default" data-width="auto">
                                    <option value="1">Kunjungan</option>
                                    <option value="0">Vcall</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row knjng" style="display: block;">
                            <label class="control-label col-md-2" for="detail">Detail</label>
                            <div class="col-md-3" style="width: 20%">
                                <select class="detail detail_select" data-style="btn-default" data-width="auto">
                                    <option value="Perangkat Rusak">Perangkat Rusak</option>
                                    <option value="Pengecekan">Pengecekan</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-transparent sn" placeholder="Nomor SN">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-transparent no_tiket" placeholder="Nomor Tiket">
                            </div>
                        </div>
                        <div class="add_detail"></div>
                        <input type="hidden" name='collect_detail' id='collect_detail'>
                        <div class="form-group row knjng" style="display: block;">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="button" class="btn btn-info btn_add_detail" style="margin-top: 5px;">
                                    Tambah Tiket
                                </button>
                            </div>
                        </div>
                        <div class="form-group BA">
                            <label class="control-label col-md-2" for="berita_ac_sel">Berita Acara</label>
                            <div class="col-md-10">
                                <select id="berita_ac_sel" name="berita_ac_sel" class="berita_ac_sel selectpicker" data-style="btn-default" data-width="auto">
                                    <option value="1">OK</option>
                                    <option value="0">NOK</option>
                                </select>
                                <textarea rows="4" style="resize:none; display: block;" cols="50" id="note_rusak" name="note_rusak" class="form-control input-transparent"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Catatan Kerusakan Isi!">{{ !empty($data->note_rusak) ? $data->note_rusak : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="koor">Koordinat</label>
                            <div class="col-md-10">
                                <input type="text" id="koor" name="koor" class="form-control input-transparent"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Koordinat Isi!"
                                data-parsley-pattern="/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/"
                                data-parsley-pattern-message="Gunakan Format Standar. Contohnya -3.327974, 114.583997"
                                required="required" value="{{ !empty($data->koor) ? $data->koor : '' }}">
                                <button type="button" class="btn btn-primary koorkuh" data-toggle="modal" data-target="#modal-large" style="margin-top: 5px;">
                                    Tampilkan Peta
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="stts">Status</label>
                            <div class="col-md-10">
                                <select id="stts" name="stts" class="stts selectpicker" data-style="btn-default" data-width="auto">
                                    <option value="0">Closed</option>
                                    <option value="3">Pending</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="ggn_open">Keterangan Gangguan</label>
                            <div class="col-md-10">
                                <textarea rows="4" style="resize:none;" cols="50" id="ggn_open" name="ggn_open" class="admin form-control input-transparent"
                                    data-parsley-trigger="change"
                                    data-parsley-required-message="Catatan Isi!">{{ !empty($data->ggn_open) ? $data->ggn_open : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="note">Catatan</label>
                            <div class="col-md-7">
                                <textarea rows="4" style="resize:none;" cols="50" id="note" name="note" class="form-control input-transparent"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Catatan Isi!"
                                >{{ !empty($data->note) ? $data->note : '' }}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn_sbmt btn-danger">Validate &amp; Submit</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-file-image-o"></i>
                            Upload Foto Berita Acara
                        </h4>
                    </header>
                    <fieldset>
                        <legend class="section">Foto Pengerjaan</legend>
                        <div class="row text-center input-photos" style="margin: 20px 0">
                            <?php
                                $number = 1;
                                clearstatcache();
                            ?>
                            @foreach($photodispatch as $input)
                            @php
                            if(in_array($input, ['SN_Rusak', 'Pengganti', 'Cover SPBU', 'Berita Acara'], true)){
                                $cls = "rusakk";
                                $style = 'display: none;';
                            }else{
                                $cls = '';
                                $style = 'display: block;';
                            }
                            @endphp
                            <div class="col-6 col-sm-3 {{ $cls }}" style="{{ $style }}">
                                <?php
                                    $p_raw1 = "/upload/spbu/$id/$input";
                                    $p_raw2 = "/upload2/spbu/$id/$input";

                                    if(file_exists($p_raw1) )
                                    {
                                        $path = $p_raw1;
                                    }
                                    else
                                    {
                                        $path = $p_raw2;
                                    }

                                    $th   = "$path-th.jpg";
                                    $img  = "$path.jpg";
                                    $nt = "$path-catatan.txt";
                                    $flag = "";
                                    $name = "flag_".$input;
                                ?>
                                @if (file_exists(public_path().$th))
                                    <?php
                                    $flag = 1;
                                    $images = $img;
                                    $src = $th;
                                    ?>
                                @else
                                    <?php
                                    $images = "";
                                    $src = "/img/placeholder.gif";
                                    ?>
                                @endif
                                <a href="{{ $images }}">
                                    <img src="{{ $src }}" alt="{{ $input }}" id="img-{{ $input }}" class="photo_valid_dis" />
                                </a>
                                <br />
                                <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag or '' }}"/>
                                <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
                                <button type="button" class="btn btn-sm btn-info">
                                    <i class="glyphicon glyphicon-camera"></i>
                                </button>
                                <p style="margin-bottom: 3px;">{{ str_replace('_',' ',$input) }}</p>
                                @if (file_exists(public_path().$nt))
                                <?php $note = File::get(public_path($nt)); ?>
                                @else
                                <?php $note ='' ?>
                                @endif
                                <textarea class="form-control foto" id="catatan_{{$input}}" name="catatan_{{$input}}">{{$note}}</textarea>
                                {!! $errors->first("flag_".$input, '<span class="label label-danger">:message</span>') !!}
                            </div>
                            <?php
                            $number++;
                            ?>
                            @endforeach
                        </div>
                    </fieldset>
                </section>
            </div>
            @if(@$data->jenis_g == 'Grounding')
                <div class="col-md-12 grounding">
                    <header>
                        <h5>
                            Checklist <strong>Instalasi Grounding SPBU</strong>
                        </h5>
                    </header>
                    @foreach($chk as $dt)
                        @foreach($dt as $key => $d)
                            <div class="col-md-12">
                                <section class="widget">
                                    <fieldset>
                                        <legend class="section">
                                            {{ $key }}
                                        </legend>
                                        <div class="widget-controls">
                                            <a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-plus"></i></a>
                                            <a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-minus"></i></a>
                                        </div>
                                        <div class="row text-center input-photos" style="margin: 20px 0">
                                            <div class="body">
                                                @foreach($d as $new_d)
                                                <div class="col-6 col-md-3">
                                                    <div class="checkbox checkbox-info checkbox-circle"  style="margin-bottom: 20px;">
                                                        <?php
                                                            $p_raw1 = "/upload/spbu/$id/chckbox/".$new_d['nama'];
                                                            $p_raw2 = "/upload2/spbu/$id/chckbox/".$new_d['nama'];

                                                            if(file_exists($p_raw1) )
                                                            {
                                                                $path = $p_raw1;
                                                            }
                                                            else
                                                            {
                                                                $path = $p_raw2;
                                                            }

                                                            $th   = "$path-th.jpg";
                                                            $img  = "$path.jpg";
                                                            $nt = "$path-catatan.txt";
                                                            $flag = "";
                                                            $name = "flag_".$new_d['nama'];
                                                        ?>
                                                        @if (file_exists(public_path().$th))
                                                            <?php
                                                            $flag = 1;
                                                            $images = $img;
                                                            $src = $th;
                                                            $th = $th;
                                                            ?>
                                                        @else
                                                            <?php
                                                            $images = "";
                                                            $src = "/img/placeholder.gif";
                                                            $th = "";
                                                            ?>
                                                        @endif
                                                        <input id="{{ $new_d['id'] }}" name="checkbox_ground[]" type="checkbox" value="{{ $new_d['nama'] }}" class="chk-bx" data-src= "{{ $src }}">
                                                        <label for="{{ $new_d['id'] }}">
                                                            {{ $new_d['nama'] }}
                                                        </label>
                                                        <div class="colm-data input-photos chk_group_img" style="display: none;" data-src= "{{ $src }}">
                                                            <a href="{{ $images }}">
                                                                <img src="{{ $src }}" alt="{{ $new_d['id'] }}" id="img-{{ $new_d['id'] }}" class="photo_valid_dis" />
                                                            </a>
                                                            <br />
                                                            <input type="text" class="hidden" name="flag_{{ $new_d['id'] }}" value="{{ $flag or '' }}"/>
                                                            <input type="file" class="hidden" name="photo-{{ $new_d['id'] }}" accept="image/jpeg" />
                                                            <button type="button" class="btn btn-sm btn-info">
                                                                <i class="glyphicon glyphicon-camera"></i>
                                                            </button>
                                                            @if (file_exists(public_path().$nt))
                                                            <?php $note = File::get(public_path($nt)); ?>
                                                            @else
                                                            <?php $note ='' ?>
                                                            @endif
                                                            <textarea name="catatan_chk_{{ $new_d['id'] }}" class="form-control foto">{{ $note }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>
                                </section>
                            </div>
                       @endforeach
                    @endforeach
                </div>
            @endif
        </form>
    </div>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="/bower_component/widgster/widgster.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js"></script>
<script type="text/javascript">
    function isObjectEmpty(obj) {
        for ( var name in obj ) {
            return false;
        }
        return true;
    }


    $(function(){
        $('.widget').widgster();

        function initializeselectpicker() {
            var yourElement = $('.detail_select');

            if (yourElement.hasClass('selectpicker') )  {
                yourElement.selectpicker('destroy');
            }

            yourElement.selectpicker();
        }

        initializeselectpicker();

        $('.chk-bx').change(function(){
            if($(this).is(":checked")){
                if($(this).attr('data-src') == '/img/placeholder.gif'){
                    $(this).next().next().find('input[type=file]').attr('required', true);
                }
                $(this).next().next().css({'display': 'block'});
                $(this).next().next().find('textarea').attr('required', true);
            }
            else if($(this).is(":not(:checked)")){
                $(this).next().next().css({'display': 'none'});
                $(this).next().next().find('textarea, input[type=file]').attr('required', false);
            }
        });

        $('.chk_group_img').each( function(){
            if($(this).attr('data-src') != '/img/placeholder.gif'){
                $(this).parent().find('input[type=checkbox]').click();
                console.log($(this).parent(), $(this).parent().find('input[type=checkbox]'))
            }
        })

        $('#act').on('change', function(){
            $('#photo-Berita_Acara').removeAttr('required');

            if($(this).val() == 0){
                $('.BA, .knjng').css({
                    'display': 'none'
                });

                $('#koor').removeAttr('required');
                $('#koor').css({
                    'cursor' : 'not-allowed'
                });
                $('#koor').attr('readonly', 'readonly');
                //hapus ui parsley
                $('#koor').attr('data-parsley-validate', 'false');
                $('#koor').parsley().reset();
            }else{
                $('.BA, .knjng').css({
                    'display': 'block'
                });

                // $('#koor').parsley()
                $('#koor').attr('data-parsley-validate', 'true');

                $('#koor').attr('required', 'required');

                if($('#img-Berita_Acara').attr('src') == '/img/placeholder.gif'){
                    $('#photo-Berita_Acara').attr('required', true);
                }else{
                    $('#photo-Berita_Acara').removeAttr('required');
                }

                $('#koor').removeAttr('readonly');
                $('#koor').css({
                    'cursor' : ''
                });

            }
        });

        $('.btn_add_detail').on('click', function() {
            $('.add_detail').append(`<div class="form-group row knjng" style="display: block; margin-top: 5px;">
                <div class="col-md-offset-2 col-md-3" style="width: 20%">
                    <select name="detail" class="detail detail_select" data-style="btn-default" data-width="auto">
                        <option value="Perangkat Rusak">Perangkat Rusak</option>
                        <option value="Pengecekan">Pengecekan</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control input-transparent sn" placeholder="Nomor SN">
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control input-transparent no_tiket" placeholder="Nomor Tiket">
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-info del_detail">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>`);

            initializeselectpicker();
        });

        $(document).on('click', '.del_detail', function() {
            $(this).parent().parent().remove();
        });

        $('.koorkuh').click(function() {
            mapboxgl.accessToken = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
            var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [114.717711, -3.443288],
                zoom: 15
            });

            var marker = new mapboxgl.Marker({
                draggable: true
            }).setLngLat([114.717711, -3.443288])
            .addTo(map);

            var geolocate = new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            });

            function onDragEnd() {
                var lngLat = marker.getLngLat();
                coordinates.style.display = 'block';
                coordinates.innerHTML =
                'Longitude: ' + parseFloat(lngLat.lng).toFixed(8) + '<br />Latitude: ' + parseFloat(lngLat.lat).toFixed(8);
                $('#koor').val([marker._lngLat.lat, marker._lngLat.lng]);
            }

            map.addControl(geolocate);

            geolocate.on('geolocate', function(e) {
                var lon = e.coords.longitude;
                var lat = e.coords.latitude;
                marker._lngLat.lng = parseFloat(lon).toFixed(8);
                marker._lngLat.lat = parseFloat(lat).toFixed(8);
                onDragEnd()
            })

            marker.on('dragend', onDragEnd);

            map.on('load', function () {
                geolocate.trigger();
                map.resize();
            });

        });

        $('#berita_ac_sel').change( function(){
            if($(this).val() != 0){
                $('#note_rusak').css({
                    'display': 'block'
                });
                $('#note_rusak').removeAttr('data-parsley-ui-enabled');
                $('#note_rusak').attr('required', 'required');

                $('.rusakk').css({
                    'display': 'block'
                });
            }else{
                $('.rusakk').css({
                    'display': 'none'
                });
                $('#note_rusak').css({
                    'display': 'none'
                });
                $('#note_rusak').attr('data-parsley-ui-enabled', 'false');
                $('#note_rusak').removeAttr('required');
                $('#note_rusak').parsley().destroy();

            }
        });

        var data = {!! json_encode($data) !!};

        if(!isObjectEmpty(data)){
            $('.admin').css({
                'cursor' : 'not-allowed'
            });

            $('.admin').attr('readonly', 'readonly');


            if(data.act != null){
                $('#act').val(data.act).change();
            }

            if(data.berita_ac_sel != null){
                $('#berita_ac_sel').val(data.berita_ac_sel).change();
            }else{
                $('#berita_ac_sel').val(0).change();
            }

            var detail_tim = {!! json_encode($detail_tim) !!};
            console.log(detail_tim);

            $.each(detail_tim, function(k, v){
                var valuenya = $(`<div class="form-group row knjng" style="display: block; margin-top: 5px;">
                <div class="col-md-offset-2 col-md-3" style="width: 20%">
                    <select name="detail" class="detail detail_select" data-style="btn-default" data-width="auto">
                        <option value="Perangkat Rusak">Perangkat Rusak</option>
                        <option value="Pengecekan">Pengecekan</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control input-transparent sn" placeholder="Nomor SN" value='${v.sn}'>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control input-transparent no_tiket" placeholder="Nomor Tiket" value='${v.no_tiket}'>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-info del_detail"><i class="fa fa-minus"></i></button>
                </div>
            </div>`);

            valuenya.find(`.detail_select option[value='${v.jenis_detail}']`).prop('selected', true);

            $('.add_detail').append(valuenya);
            })

            initializeselectpicker();
        }else{
            $('.chk-bx').next().next().find('img').attr('src', '/img/placeholder.gif');
            $('.chk-bx').next().next().find('textarea').val('');
        }

        $('input[type=file]').change(function() {
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                $(inputEl).parent().find('input[type=text]').val(1);
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);

                }
                reader.readAsDataURL(inputEl.files[0]);
            }
        });

        $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();
        });

        $('.btn_sbmt').on('click', function(e) {
            var data = [];

            $.each($('.detail_select'), function(k, v){
                var val_detail = $(this).find("select :selected").val();

                if(typeof val_detail != 'undefined' && val_detail.length > 0){
                    data.push({
                        'detail': $(this).find("select :selected").val(),
                        'sn': $(this).parent().parent().find('.sn').val(),
                        'no_tiket': $(this).parent().parent().find('.no_tiket').val()
                    });
                }
            });

            $('#collect_detail').val(JSON.stringify(data) );
        });

        $("#validation-form").on('submit', function(e) {
            $(this).parsley().validate();

            if ($(this).parsley().isValid()) {
                $('.btn_sbmt').attr('disabled');
            }
        });
    });
</script>
@endsection
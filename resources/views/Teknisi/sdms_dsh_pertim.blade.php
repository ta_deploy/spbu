@extends('layout')
@section('title', 'List Pekerjaan Off')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha512-3kAToXGLroNvC/4WUnxTIPnfsiaMlCn0blp0pl6bmR9X6ibIiBZAi9wXmvpmg1cTyd2CMrxnMxqj7D12Gn5rlw==" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
    .form-group.footable-filtering-search .input-group-btn {
        display: none;
    }
    .select2-results__option, select{
        color: black;
    }
</style>
@section('content')
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <form id="formlistG" name="formlistG" method="get">
                    <div class="input-group date">
                        <select id="selectih" name="selectih" class="selectpicker" data-style="btn-default" data-width="auto">
                            <option value="edc">EDC</option>
                            <option value="pn">Pump/Nozzle</option>
                            <option value="atg">ATG</option>
                            <option value="sla">SLA</option>
                            <option value="penalty">Penalty</option>
                        </select>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i> Cari</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    @if(!empty($final_data['data']))
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <header>
                    <h4>
                        List <span class="fw-semi-bold">{{$final_data['nama']}}</span> Dengan Total <b>{{ count($final_data['data']) }}</b>
                    </h4>
                </header>
                <div class="body">
                    <table class="table dbs table-striped table-bordered" data-sorting="true" data-filtering="true">
                        <thead>
                            <tr>
                                <th>No</th>
                                @foreach($final_data['head'] as $d)
                                <th data-breakpoints="xs sm">{{ $d }}</th>
                                @endforeach
                                <th data-breakpoints="xs sm">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($final_data['data'] as $raw_d)
                            <tr>
                                <td>{{ ++$no }}</td>
                                @foreach($raw_d as $d)
                                <td>{{ $d }}</td>
                                @endforeach
                                <td><a type="button" onclick="send_data(this)" data-site="{{ $raw_d['Site'] }}" class="btn submit_data btn-primary btn-sm"><i class="material-icons">filter_list</i>Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
    @endif
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha512-aVkYzM2YOmzQjeGEWEU35q7PkozW0vYwEXYi0Ko06oVC4NdNzALflDEyqMB5/wB4wH50DmizI1nLDxBE6swF3g==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
    function send_data(ths) {
        let data = $(ths).data('site');
        window.location.href = '/sdms/input/'+ data;
    }
    $(function(){
        $('#selectih').select2({
            width: '100%'
        });

        $('.dbs').footable({
            'paging': {
                'enabled': true,
                'size': 10
            }
        });
    })
</script>
@endsection
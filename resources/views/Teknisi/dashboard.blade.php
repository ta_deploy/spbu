@extends('layout')
@section('title', 'Dashboard')
@section('style')
<link rel="stylesheet" href="/bower_component/jPages-master/css/animate.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="/bower_component/footable/css/footable.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style type="text/css">
	.holder {
		position: relative;
		left: 50%;
		display: inline-block;
	}

	.holder a, .holder span {
		color: black;

		font-size: 13px;
		padding: 8px 16px;
		text-decoration: none;
		transition: background-color .3s;
	}

	.holder a.jp-current {
		background-color: #3c4452;
		color: white;
		border-radius: 5px;
	}

	.holder a {
		background-color: #303641;
		color: white;
		border-radius: 5px;
	}

	.holder a:hover {
		background-color: #303641;
		border-radius: 5px;
	}

	.holder a:first-child {
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
	}

	.holder a:last-child {
		border-top-right-radius: 5px;
	}

	.daterangepicker  {
		background-color: #414e60;
	}

	.daterangepicker .month, .daterange {
		color: black;
	}

	.daterange {
		width: 100%;
	}

	.table-condensed{
		background-color: #5f666f;
	}

</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						Table
						<span class="fw-semi-bold">
							<select id="jenis" style="border-radius: 12px; background-color: #474a4f;" class="selectpicker" data-style="btn-default" data-width="auto">
								<option value="1" >Open</option>
								<option value="3" >Pending</option>
								<option value="0" >Close</option>
							</select>
						</span>
					</h4>
				</header>
				<div class="body">
					<table class="table dbs table-striped" data-expand-all="true" style="color: white;">
						<thead>
							<tr>
								<th>ID SPBU</th>
								<th data-breakpoints="xs sm">Witel</th>
								<th data-breakpoints="xs sm">Jenis Gangguan</th>
								<th data-breakpoints="xs sm">Open</th>
								<th class="final" data-breakpoints="xs sm">Status</th>
								<th data-breakpoints="xs sm">Action</th>
							</tr>
						</thead>
						<tbody id="data_table">
						</tbody>
					</table>
					<div class="holder"></div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/footable/js/footable.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(function(){
		$('#jenis').val(1).change();
		function get_data(data){
			var tableBody = $(".dbs #data_table");
			tableBody.html('');
			$.ajax({
				type: 'GET',
				url: '/teknisi/table',
				dataType: 'json',
				data: {
					type: data
				},
				success: function(data) {
					var markup;
					$.each(data, function(key, val) {
						markup += "<tr>";
						markup += "<td>"+ val.id_spbu +"</td>";
						markup += "<td>"+ val.kota +"</td>";
						markup += "<td>"+ val.jenis_g +"</td>";
						markup += "<td>"+ val.open +"</td>";
						markup += "<td>"+ (val.close == null ? 'Belum Close' : val.close) +"</td>";
						markup += '<td><a type="button" href="/Spbu/update_wo/'+ val.id +'" class="btn btn-primary btn-sm pra_kontrak"><i class="material-icons">filter_list</i>Edit</a></td>'
						markup += "</tr>";
					});
					tableBody.html(markup);
					$('.dbs').footable();
				}
			})
		}

		get_data($("#jenis option:selected").val());

		$('#jenis').change(function() {
			get_data($(this).val());

			if($(this).val() == 3){
				$('.final').html('Pending')
			}else{
				$('.final').html('Close')
			}
		});
	});
</script>
@endsection

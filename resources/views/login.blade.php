<!DOCTYPE html>
<html>
<head>
    <title>Login</title>

    <link href="/css/application.css" rel="stylesheet">
    <link rel="shortcut icon" href="/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script>
        /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
        chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
        https://code.google.com/p/chromium/issues/detail?id=332189
        */
    </script>
</head>
<body>
    <div class="single-widget-container">
        <section class="widget login-widget">
            <header class="text-align-center">
                <h4>SPBU TA</h4>
            </header>
            <div class="body">
                <form class="no-margin" method="POST">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="email" >Username</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input id="username" name="username" type="text" class="form-control input-lg input-transparent"
                                placeholder="Nama Anda">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" >Password</label>

                            <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                                <input id="password" name="password" type="password" class="form-control input-lg input-transparent"
                                placeholder="Kata Sandi">
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-block btn-lg btn-danger">
                            <span class="small-circle"><i class="fa fa-caret-right"></i></span>
                            <small>Sign In</small>
                        </button>
                        <a class="forgot"></a>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <!-- common libraries. required for every page-->
    <script src="/bower_component/jquery/dist/jquery.min.js"></script>
    <script src="/bower_component/jquery-pjax/jquery.pjax.js"></script>
    <script src="/bower_component/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
    <script src="/bower_component/widgster/widgster.js"></script>
    <script src="/bower_component/underscore/underscore.js"></script>

    <!-- common application js -->
    <script src="/js/app.js"></script>
    <script src="/js/settings.js"></script>
</body>
</html>
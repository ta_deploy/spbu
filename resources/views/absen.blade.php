<!-- light-blue - v4.0.1 - 2018-10-08 -->

<!DOCTYPE html>
<html>
<head>
    <title>Absensi</title>

    <link href="/css/application.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script>
        /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
           chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
                      https://code.google.com/p/chromium/issues/detail?id=332189
        */
    </script>
    <style type="text/css">
      .input-photos img {
          width: 100px;
          height: 150px;
          margin-bottom: 5px;
      }

      .foto {
          border: 3px solid white;
          resize: none;
  
          margin-bottom: 4px;
      }
  
  </style>
</head>
<body>
  <div class="error-page container">
  <main id="content" class="error-container" role="main">
    <div class="row">
      <div class="col-lg-4 col-sm-6 col-xs-10 col-lg-offset-4 col-sm-offset-3 col-xs-offset-1">
        <div class="error-container">
          <h1 class="error-code">STOP</h1>
          <p class="error-info">
            <code>{{ $session->nama }} Belum Absen!</code>
          </p>
          <p class="error-help mb">
            @if (empty($session->spbu_absen))
            @if(@$get_log->status == 2)
              Alasan Ditolak: <b style="font-size: 20px;">{{ $get_log->detail }}</b>
            @else
              Tidak bisa bekerja jika tida absen!
              @endif
            @else
             Menunggu Konfirmasi TL
            @endif
            <br/>
            <a type="button" href="/logout" class="btn btn-transparent">
              Logout <i class="fa fa-circle-thin text-danger ml-xs"></i>
            </a>
          </p>
          <div class="form-group">
            <section class="widget">
              <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
                {{ csrf_field() }}
                <div class="widget-body">
                  <div class="row">
                    <div class="col-md-12">
                      <fieldset>
                        @if (empty($session->spbu_absen) || date('Y-m-d', strtotime($session->spbu_absen)) != date('Y-m-d'))
                          @foreach($foto as $new_d)
                          <div class="row text-center input-photos" style="margin: 20px 0">
                              <img src="/img/placeholder.gif" alt="{{ $new_d }}" id="img-{{ $new_d }}" class="photo_valid_dis" />
                            <br />
                            <input type="text" class="hidden" name="flag_{{ $new_d }}" value="{{ $flag or '' }}"/>
                            <input type="file" class="hidden" name="photo-{{ $new_d }}" accept="image/jpeg" {{ $new_d == 'Foto_Kehadiran' ? "capture=camera" : '' }} required/>
                            <button type="button" class="btn btn-sm btn-info">
                              <i class="glyphicon glyphicon-camera"></i>
                            </button>
                            <p style="margin-bottom: 3px;">{{ str_replace('_',' ',$new_d) }}</p>
                          </div>
                          @endforeach
                          <button type="submit" class="btn btn-transparent">
                            Submit Absen <i class="fa fa-circle-thin text-warning ml-xs"></i>
                          </button>
                        @else
                          Sudah Berhasil Absen! Silahkan Hubungi TL untuk segera Verifikasi!
                        @endif
                      </fieldset>
                    </div>
                  </div>
                </div>
              </form>
            </section>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
<!-- common libraries. required for every page-->
<script src="/bower_component/jquery/dist/jquery.min.js"></script>
<script src="/bower_component/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="/bower_component/widgster/widgster.js"></script>
<script src="/bower_component/underscore/underscore.js"></script>

<!-- common application js -->
<script src="/js/app.js"></script>
<script src="/js/settings.js"></script>
<script>
  $(function () {
    $('input[type=file]').change(function() {
      var inputEl = this;
      if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
              $(inputEl).parent().find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(inputEl.files[0]);
      }
    });

    $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
    });
  });
</script>
</body>
</html>
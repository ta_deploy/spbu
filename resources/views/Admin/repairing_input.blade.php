@extends('layout')
@section('title', (empty($data) ? 'Input Data Repairing' : 'Edit Data'))
@section('style')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style type="text/css">


    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }

    .select2-results__option, select{
        color: black;
    }

    #map {
        position: absolute; top: 0; bottom: 0; width: 90vw; height: 100%;
    }

    .mapboxgl-canvas {
        left: 0;
    }

    .modal-xl{
        width: 100%;
        max-width:1300px;
    }

    .modal-body{
        height: 400px;
    }

    .foto {
        border: 3px solid white;
        resize: none;

        margin-bottom: 4px;
    }

    .coordinates {
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        position: absolute;
        bottom: 40px;
        left: 10px;
        padding: 5px 10px;
        margin: 0;
        font-size: 11px;
        line-height: 18px;
        border-radius: 3px;
        display: none;
    }

</style>
@endsection
@section('content')
<div class="modal fade" id="modal-large" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Find Me</h4>
            </div>
            <div class="modal-body">
                <div id="map"></div>
                <pre id="coordinates" class="coordinates"></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
            </div>
        </div>
    </div>
</div>
<div class="body">
    <div class="row">
        <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-check-square-o"></i>
                            Pengisian Data Tiket SPBU
                        </h4>
                    </header>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{-- pra kontrak --}}
                    <fieldset>
                        <legend class="section">Tiket SPBU</legend>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="alat">Alat</label>
                            <div class="col-md-7">
                                <select id="alat" name="alat" class="admin_select selectpicker" data-style="btn-default" data-width="auto">
                                    <option value="FCC">FCC</option>
                                    <option value="FDM-081(Dispenser Gilbarco)">FDM-081(Dispenser Gilbarco)</option>
                                    <option value="EDC Zte Z90">EDC Zte Z90</option>
                                    <option value="EDC Zte A920">EDC Zte A920</option>
                                    <option value="ATG Probe Windbell 2,9M">ATG Probe Windbell 2,9M</option>
                                    <option value="ATG Probe Windbell 2,4M">ATG Probe Windbell 2,4M</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="witel">Witel</label>
                            <div class="col-md-7">
                                <select id="witel" name="witel" class="admin_select selectpicker" data-style="btn-default" data-width="auto">
                                    @foreach ($witel as $v)
                                        <option value="{{ $v->witel_tomman }}">{{ $v->witel_tomman }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="sn">Serial Number</label>
                            <div class="col-md-7">
                                <input type="text" id="sn" name="sn" class="form-control admin input-transparent">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="teknisi">Teknisi</label>
                            <div class="col-md-7">
                                <select id="teknisi" name="teknisi" class="admin_select selectpicker"
                                data-style="btn-default" data-width="auto"
                                data-parsley-trigger="change"
                                required
                                data-parsley-required-message="Pilih Teknisi yang Mengerjakan!"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="status_repair">Status Repairing</label>
                            <div class="col-md-7">
                                <select id="status_repair" name="status_repair" class="admin_select selectpicker" data-style="btn-default" data-width="auto">
                                    <option value="OK">OK</option>
                                    <option value="UNREPAIR">UNREPAIR</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="ket">Keterangan</label>
                            <div class="col-md-7">
                                <textarea rows="4" style="resize:none;" cols="50" id="ket" name="ket" class="admin form-control input-transparent"
                                    data-parsley-trigger="change"
                                    data-parsley-required-message="Catatan Isi!"
                                    required
                                    >{{ $data->ggn_open ?? '' }}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn_sbmt btn-danger">Validate &amp; Submit</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <section class="widget">
                      <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="widget-body">
                          <div class="row">
                            <fieldset>
                                @foreach($foto as $new_d)
                                  <div class="col-md-3 text-center input-photos" style="margin: 20px 0">
                                      <img src="/img/placeholder.gif" alt="{{ $new_d }}" id="img-{{ $new_d }}" class="photo_valid_dis" />
                                    <br />
                                    <input type="text" class="hidden" name="flag_{{ $new_d }}" value="{{ $flag or '' }}"/>
                                    <input type="file" class="hidden" name="photo-{{ $new_d }}" accept="image/jpeg" {{ $new_d == 'Foto_Kehadiran' ? "capture=camera" : '' }} required/>
                                    <button type="button" class="btn btn-sm btn-info">
                                      <i class="glyphicon glyphicon-camera"></i>
                                    </button>
                                    <p style="margin-bottom: 3px;">{{ str_replace('_',' ',$new_d) }}</p>
                                  </div>
                                  @endforeach
                              </fieldset>
                          </div>
                        </div>
                      </form>
                    </section>
                  </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript">
    function isObjectEmpty(obj) {
        for ( var name in obj ) {
            return false;
        }
        return true;
    }

    $(function(){
        $('#alat').select2({
            width: '100%'
        });

        $('#witel').select2({
            width: '100%'
        });

        var data = {!! json_encode($data) !!};

        $('#teknisi').select2({
            width: '100%',
            placeholder: "Pilih Teknisi",
            minimumInputLength: 7,
            allowClear: true,
            ajax: {
                url: "/input/teknisi_repair/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                language: {
                    noResults: function(){
                        return "User Tidak Terdaftar!";
                    }
                },
                cache: true,
                success: function(value) {

                }
            }
        });
        if(!isObjectEmpty(data)){
            $('#alat').val(data.jenis_g).change();

            // $.ajax({
            //     type: 'GET',
            //     url: '/input/alamat_stts/search',
            //     dataType: 'json',
            //     data: {id_spbu: data.id_spbu},
            //     success: function(value) {
            //         $('#id_alamat').val(value.alamat);
            //         $('#id_status').val(value.status);
            //         $('#sto').val(value.sto);
            //     }
            // });
        }

        $("#validation-form").on('submit', function() {
            $(this).parsley().validate();
            if ($(this).parsley().isValid()) {
                $('.btn_sbmt').attr('disabled');
            }
        });
    });
</script>
@endsection
@extends('layout')
@section('title', 'Dashboard')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.daterangepicker  {
		background-color: #414e60;
	}

	.select2-results__option, select{
        color: black;
    }

	.select2-selection__choice, select{
        color: black;
  }

	.daterangepicker .month, .daterange {
		color: black;
	}

	.daterangepicker .calendar-table {
		background-color: #4a4d50;
	}

	.daterange {
		width: 100%;
	}

	.modal-xl{
		width: 100%;
		max-width:1300px;
	}

</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
	<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content" style="background-color: #6e767e;">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle" style="color: white;">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body body_id_sec">
				</div>
				<div class="modal-footer">
					<div class="form-group row">
						<div class="col-md-4 btn_edit">
							<a type="button" href="#" class="btn btn-block btn_edit btn-primary brtn_edit">Edit Data</a>
						</div>
						<div class="col-md-4 btn_up">
							<a type="button" href="#" class="btn btn-block btn_edit btn-info brtn_up">Update Pekerjaan</a>
						</div>
						<div class="col-md-4 btn_delete">
							<a type="button" href="#" class="btn btn-block btn_edit btn-warning brtn_del">Hapus Pekerjaan</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-large" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Tabel Data</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-bordered modalkuh" id="gdrive" style="width: 100%;">
						<thead>
							<tr class="headerr">

							</tr>
						</thead>
						<tbody id="data_table">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						Filter <span class="fw-semi-bold">Pengerjaan</span>
					</h4>
				</header>
				<div class="body">
					<form method="GET">
						<div class="form-group row">
							<div class="col-sm-12">
								<div class="input-group date">
									<input type="text" name="daterange" class="form-control daterange" readonly value=" {{ @$prev_data['daterange'] }}">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-4">
								<select id="jenis_g" name="jenis_g[]" class="dashboard_menu selectpicker" data-style="btn-default" data-width="auto" multiple>
									<option value="Troubleshoot" {{ @in_array('Troubleshoot', $prev_data['jenis_g'])? 'selected' : '' }} {{ empty($prev_data['jenis_g']) ? 'selected' : '' }}>TROUBLESHOOT</option>
									<option value="Grounding" {{ @in_array('Grounding', $prev_data['jenis_g'])? 'selected' : '' }} {{ empty($prev_data['jenis_g']) ? 'selected' : '' }}>GROUNDING</option>
									<option value="Pergantian Perangkat" {{ @in_array('Pergantian Perangkat', $prev_data['jenis_g'])? 'selected' : '' }} {{ empty($prev_data['jenis_g']) ? 'selected' : '' }}>Pergantian Perangkat</option>
									<option value="Perangkat Rusak" {{ @in_array('Perangkat Rusak', $prev_data['jenis_g'])? 'selected' : '' }} {{ empty($prev_data['jenis_g']) ? 'selected' : '' }}>Perangkat Rusak</option>
									<option value="Preventive Maintenance" {{ @in_array('Preventive Maintenance', $prev_data['jenis_g'])? 'selected' : '' }} {{ empty($prev_data['jenis_g']) ? 'selected' : '' }}>Preventive Maintenance</option>
								</select>
							</div>
							<div class="col-sm-4">
								<select id="status" name="status[]" class="dashboard_menu selectpicker" data-style="btn-default" data-width="auto" multiple>
									<option value="1" {{ @in_array('1', $prev_data['status'])? 'selected' : '' }} {{ empty($prev_data['status']) ? 'selected' : '' }}>Open</option>
									<option value="0" {{ @in_array('0', $prev_data['status'])? 'selected' : '' }} {{ empty($prev_data['status']) ? 'selected' : '' }}>Close</option>
									<option value="3" {{ @in_array('3', $prev_data['status'])? 'selected' : '' }} {{ empty($prev_data['status']) ? 'selected' : '' }}>Pending</option>
								</select>
							</div>
							<div class="col-sm-4">
								<select id="job" name="job" class="dashboard_menu selectpicker" data-style="btn-default" data-width="auto">
									<option value="me" {{ @$prev_data['job'] == 'me' ? 'selected' : '' }} {{ empty($prev_data['job']) ? 'selected' : '' }}>Pekerjaan Saya</option>
									<option value="all" {{ @$prev_data['job'] == 'all' ? 'selected' : '' }} {{ empty($prev_data['job']) ? 'selected' : '' }}>Semua Pekerjaan</option>
								</select>
							</div>
						</div>
						<button type="submit" class="btn btn-primary mb-2">Cari Data</button>
					</form>
				</div>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						Dashboard <span class="fw-semi-bold">Pengerjaan</span>
					</h4>
				</header>
				<p class="no-margin">
					<span class="badge badge-success">Sudah Selesai </span>
					<span class="badge badge-primary">Kurang Dari 10 Hari</span>
					<span class="badge badge-info">Kurang Dari 20 Hari</span>
					<span class="badge badge-warning">Kurang Dari 30 Hari</span>
					<span class="badge badge-danger">Lebih Dari 30 Hari</span>
					<span class="badge badge-default">Pending</span>
				</p>
				<div class="body">
					<table class="table dbs table-bordered">
						<thead>
							<tr>
								<th class="hidden-xs">#</th>
								<th>Datel</th>
								<th>Sektor</th>
								<th>Nama Teknisi</th>
							</tr>
						</thead>
						<tbody>
							@php
								$nomor = 0;
							@endphp
							@foreach ($table_kerja as $no => $raw_tk)
							@php
								$rp = true;
							@endphp
								@foreach ($raw_tk as $tk)
								<tr>
									@if ($rp == true)
									<td rowspan="{{ count($raw_tk) }}">{{ ++$nomor }}</td>
									<td rowspan="{{ count($raw_tk) }}">{{ $no }}</td>
									@php
										$rp = false;
									@endphp
								@endif
									<td><b>{{ $tk->nama }}</b> ({{ $tk->teknisi }})</td>
									<td>
										<p>
											@foreach ($tk->data as $detail_data )
											@php
												if($detail_data->status == 'closed'){
													$label = 'label-success';
												}else if($detail_data->status == 'belum'){
													if($detail_data->hari < 10){
														$label = 'label-primary';
													}else if($detail_data->hari > 10 && $detail_data->hari < 20 ){
														$label = 'label-info';
													}else if($detail_data->hari > 20 && $detail_data->hari < 30 ){
														$label = 'label-warning';
													}else if($detail_data->hari >= 30 ){
														$label = 'label-danger';
													}
												}else if($detail_data->status == 'pending' ){
													$label = 'label-default';
												}
											@endphp
											<span data-id='{{ $detail_data->id_dis }}' class='label_sel_opt label {{ $label }}' style='margin-left: 0.3EM; margin-bottom: 0.3EM; cursor: pointer;' data-toggle='modal' data-target='#exampleModalLong'><b style="color: black;">{{ $detail_data->jenis }}: </b>{{ $detail_data->id_spbu }}</span>
											@endforeach
										</p>
									</td>
								</tr>
								@endforeach
							@endforeach
						</tbody>
					</table>
				</div>
			</section>
		</div>
		<div class="col-md-6">
			<section class="widget">
				<header>
					<h4>
						Dashboard <span class="fw-semi-bold">Action</span>
					</h4>
				</header>
				<div class="body">
					<table class="table dbs">
						<thead>
							<tr>
								<th class="hidden-xs" rowspan="2">#</th>
								<th rowspan="2">Teknisi</th>
								<th colspan="5" style="text-align: center;">Jenis Gangguan</th>
							</tr>
							<tr>
								<th class="align-middle" style="text-align: center;">STO</th>
								<th class="align-middle" style="text-align: center;">Grounding</th>
								<th class="align-middle" style="text-align: center;">Perangkat Rusak</th>
								<th class="align-middle" style="text-align: center;">Pergantian Perangkat</th>
								<th class="align-middle" style="text-align: center;">Preventive Maintenance</th>
								<th class="align-middle" style="text-align: center;">TroubleShoot</th>
							</tr>
						</thead>
						<tbody id="durasi_table">
							@php
								$no = $grounding = $perangkat_rusak = $pergantian_perangkat = $preventive_maintenance = $troubleshoot = 0;
							@endphp
							@foreach ($table_act as $k => $v )
								@php
									$first = true;
									$no_row = count($v);
								@endphp

								@foreach ($v as $k2 => $v2)
									<tr>
										@if ($first == true)
											<td rowspan="{{ $no_row }}" style="vertical-align: middle;">{{ ++$no }}</td>
											<td rowspan="{{ $no_row }}" style="vertical-align: middle;">{{ $k }}</td>
											@php
												$first = false;
												$grounding += $v2['grounding'];
												$perangkat_rusak += $v2['perangkat_rusak'];
												$pergantian_perangkat += $v2['pergantian_perangkat'];
												$preventive_maintenance += $v2['preventive_maintenance'];
												$troubleshoot += $v2['troubleshoot'];
											@endphp
										@endif
										<td style='text-align: center; color: white;'>{{ $k2 }}</td>
										<td style='text-align: center; color: white;'>{{ $v2['grounding'] ?: '-' }}</td>
										<td style='text-align: center; color: white;'>{{ $v2['perangkat_rusak'] ?: '-' }}</td>
										<td style='text-align: center; color: white;'>{{ $v2['pergantian_perangkat'] ?: '-' }}</td>
										<td style='text-align: center; color: white;'>{{ $v2['preventive_maintenance'] ?: '-' }}</td>
										<td style='text-align: center; color: white;'>{{ $v2['troubleshoot'] ?: '-' }}</td>
									</tr>
								@endforeach
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td style='text-align: center;' colspan="3">Total</td>
								<td style='text-align: center; color: white;'>{{ $grounding ?: $grounding }}</td>
								<td style='text-align: center; color: white;'>{{ $perangkat_rusak ?: $perangkat_rusak }}</td>
								<td style='text-align: center; color: white;'>{{ $pergantian_perangkat ?: $pergantian_perangkat }}</td>
								<td style='text-align: center; color: white;'>{{ $preventive_maintenance ?: $preventive_maintenance }}</td>
								<td style='text-align: center; color: white;'>{{ $troubleshoot ?: $troubleshoot }}</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_component/moment/moment.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script type="text/javascript">
	$(function(){

		$('#jenis_g').select2({
			width: '100%',
			placeholder: 'Masukkan Jenis WO nya!'
		});

		$('#status').select2({
			width: '100%',
			placeholder: 'Masukkan statusnya!'
		});

		$('#job').select2({
			width: '100%',
		});

		$('.label_sel_opt').each(function() {
			$(this).on("click", function(){
				var data_id = $(this).data('id');
				$.ajax({
					url:"/get/table/get_tekn_det",
					type:"GET",
					data: {
						id : data_id
					},
					dataType: 'html',
					success: (function(data){
						$(".body_id_sec").html(data);
					}),
				})
			});
		});

		$('a[href="/spbu/ket"]').on('click', function(e){
			var raw_data = $('.daterange').val().split('-'),
			b1 = raw_data[0].split('/').join('-'),
			b2 = raw_data[1].split('/').join('-');
			e.preventDefault();
			$.ajax({
				url: "/get/table/detail_act",
				type: 'GET',
				data: {
					kota : $(this).parent().prev().text(),
					month1	 : b1,
					month2   : b2,
					jenis    : 1
				},
				success: function(data){
					console.log(data)
					// $('.body_id_sec').html(data)
				}
			})
		});

		$('a[href="/spbu/vc"]').on('click', function(e){
			var raw_data = $('.daterange').val().split('-'),
			b1 = raw_data[0].split('/').join('-'),
			b2 = raw_data[1].split('/').join('-');
			e.preventDefault();
			$.ajax({
				url: "/get/table/detail_act",
				type: 'GET',
				data: {
					kota : $(this).parent().prevAll(':eq(1)').text(),
					month1	 : b1,
					month2   : b2,
					jenis    : 0
				},
				success: function(data){
					console.log(data)
						// $('.body_id_sec').html(data)
				}
			})
		});

		$('input[name="daterange"]').daterangepicker({
			opens: 'left',
			"locale": {
				"format": "YYYY/MM/DD",
				"separator": " - ",
			}
		});

		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="daterange"]').click();
		});
	});
</script>
@endsection

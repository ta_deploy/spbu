@extends('layout')
@section('title', 'List Detail  Chart')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha512-3kAToXGLroNvC/4WUnxTIPnfsiaMlCn0blp0pl6bmR9X6ibIiBZAi9wXmvpmg1cTyd2CMrxnMxqj7D12Gn5rlw==" crossorigin="anonymous" />
<style type="text/css">
    .form-group.footable-filtering-search .input-group-btn {
        display: none;
    }
    th{
        color: black;
    }
    .kotak_ajaib{
        height: 100%;
        overflow-y: scroll;
    }
</style>
@section('content')
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <form id="formlistG" name="formlistG" method="get">
                    <div class="input-group date">
                        <select id="selectih" name="selectih selectpicker" data-style="btn-default" data-width="auto">
                            <option value="edc">EDC</option>
                            <option value="pn">Pump/Nozzle</option>
                            <option value="atg">ATG</option>
                            <option value="sla">SLA</option>
                            <option value="penalty">Penalty</option>
                        </select>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i> Cari</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    @if(!empty($aver))
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <header>
                    <h4>
                        Pivot <span class="fw-semi-bold">{{ $aver['nama'] }}</span>
                    </h4>
                </header>
                <div class="body">
                    <table class="table table-bordered table-stripped dbs">
                        <head>
                            <tr>
                                <th>Naker</th>
                                <th>ID SPBU</th>
                                <th>Performa</th>
                                <th>Average</th>
                            </tr>
                        </head>
                        <tbody>
                            @foreach($aver['data'] as $key => $value)
                            @php $first = true @endphp
                            @php $first2 = true @endphp
                            @php
                            $avg = 0;
                            @endphp
                            @foreach($value['detail_data'] as $key2 => $rvalue)
                            <tr>
                                <?php
                                $avg += $rvalue['Average'];
                                ?>
                                @if($first == true)
                                <td rowspan="{{ count($value['detail_data']) }}" class="align-middle">{{ $key }}</td>
                                @php $first = false @endphp
                                @endif
                                <td>{{ $rvalue['SPBU'] }}</td>
                                <td>{{ $rvalue['Average'] }}</td>
                                @if($first2 == true)
                                <td rowspan="{{ count($value['detail_data']) }}" class="align-middle">{{ round($value['avg'] / $value['jml'], 2) }}</td>
                                @php $first2 = false @endphp
                                @endif
                            </tr>
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
    @else
    Tidak ada Data
    @endif
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha512-aVkYzM2YOmzQjeGEWEU35q7PkozW0vYwEXYi0Ko06oVC4NdNzALflDEyqMB5/wB4wH50DmizI1nLDxBE6swF3g==" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function(){
        $('#selectih').select2({
            width: '100%'
        });

        $('.dbs').footable({
            'paging': {
                'enabled': true,
                'size': 10
            }
        });
    })
</script>
@endsection
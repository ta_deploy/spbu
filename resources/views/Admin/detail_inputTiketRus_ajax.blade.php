<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style type="text/css">
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}

	#map {
		position: absolute; top: 0; bottom: 0; width: 99%; height: 100%;
	}

	.mapboxgl-canvas {
		left: 0;
	}

	.foto_catata {
		border: 3px solid white;
		resize: none;
		margin-bottom: 4px;
	}

</style>
<div class="body">
	<div class="row">
		<div class="col-md-5">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-check-square-o"></i>
						Data Tiket SPBU
					</h4>
					@if (!empty($data->parent))
						<code>Tiket Berasal Dari Relokasi SPBU <b>{{ $data->relok_spbu }}</b></code>
					@endif
				</header>
				{{-- pra kontrak --}}
				<form class="form-horizontal">
					<fieldset>
						<legend class="section">Tiket SPBU</legend>
						<div class="form-group">
							<label class="control-label col-md-4" for="sto">Sto:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->kota }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_spbu">Id SPBU:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->id_spbu }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_spbu">Naker Sektor:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->naker_nama }} ({{ $data->naker_id }})</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="no_tiket">Nomor Tiket:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->no_tiket ?? 'Tidak Ada Nomor Tiket' }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="file_ba">File BA perangkat Rusak</label>
							<div class="col-md-8">
								@php
								$path = public_path() . '/upload/spbu/broken_part/' . $id . '/BA/';
								$path2 = public_path() . '/upload2/spbu/broken_part/' . $id . '/BA/';

								if (file_exists($path)) {
										$files = preg_grep('~^File BA.*$~', scandir($path));
										$files = array_values($files);
										$real_path = '/upload/spbu/broken_part/' . $id . '/BA/'.$files[0];
								}else{
									$files = preg_grep('~^File BA.*$~', scandir($path2));
									$files = array_values($files);
									$real_path = '/upload2/spbu/broken_part/' . $id . '/BA/'.$files[0];
								}
								@endphp
								@if (@!$real_path)
										<p class="form-control input-transparent"><b>File Belum Diupload!</b></p>
								@endif
								@if (@$real_path)
										<p class="form-control input-transparent"><b>File Bisa Diunduh <a href="/Spbu/download_BA/broken_part/{{ $id }}">Disini</a></b></p>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_alamat">Alamat:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->alamat }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_status">Status:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->type }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Perangkat Rusak:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->broken_part }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Jenis Perangkat:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->jenis_perangkat }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Teknisi Pengerjaan:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->nama }}  {{ $data->teknisi }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Status Ganti Relokasi:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->status_ganti_relok ?: 'Tidak Ada' }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Pergantian Relokasi:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->status_perangkat_relok ?: 'Tidak Ada' }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="act">lokasi:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->lokasi }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="detail">Pergantian Perangkat:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->change_part }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="note">Catatan:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->note ?: 'kosong' }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="stts">Status:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><span class="label label-{{ ($data->close != NULL ? 'success' : 'warning' ) }}">{{ ($data->close != NULL ? 'Close' : 'Open' ) }}</span></p>
							</div>
						</div>
					</fieldset>
				</form>
			</section>
		</div>
		<div class="col-md-7">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-file-image-o"></i>
						Upload Foto Berita Acara
					</h4>
				</header>
				<fieldset>
					<legend class="section">Foto Perangkat Rusak</legend>
					<div class="row text-center input-photos" style="margin: 20px 0">
						<?php
						$number = 1;
						clearstatcache();
						?>
						@foreach($part as $input)
						<?php
						if(@$data->parent){
							if(in_array($input, ['berita_acara_perangkat_relokasi','berita_acara_pergantian_perangkat', 'perangkat_relokasi', 'berita_acara_pergantian_perangkat_relokasi']))
							{
								$path_raw = "/upload/spbu/broken_part/$data->parent/$input";

								if(!file_exists(public_path()."$path-th.jpg") )
								{
									$path = "/upload/spbu/broken_part/$data->parent/$input";
								}
								else
								{
									$path = "/upload2/spbu/broken_part/$data->parent/$input";
								}
							}
						}
						else
						{
							$path_raw = "/upload/spbu/broken_part/$id/$input";

							if(!file_exists(public_path()."$path-th.jpg") )
							{
								$path = "/upload/spbu/broken_part/$id/$input";
							}
							else
							{
								$path = "/upload2/spbu/broken_part/$id/$input";
							}
						}
						$th   = "$path-th.jpg";
						$img  = "$path.jpg";
						$nt = "$path-catatan.txt";
						$flag = "";
						$name = "flag_".$input;

						?>
						@if (file_exists(public_path().$th))
						<?php
						$flag = 1;
						$images = $img;
						$src = $th;
						$th = $th;
						?>
						<div class="col-6 col-sm-3">
							<a href="{{ $images }}">
								<img src="{{ $src }}" alt="{{ $input }}" id="img-{{ $input }}" class="photo_valid_dis" />
							</a>
							<br />
							<p style="margin-bottom: 3px;">{{ str_replace('_',' ',$input) }}</p>
							@if (file_exists(public_path().$nt))
							<?php $note = File::get(public_path($nt)); ?>
							@else
							<?php $note ='' ?>
							@endif
							<textarea class="form-control foto_catata" id="catatan_{{$input}}" name="catatan_{{$input}}">{{$note}}</textarea>
							{!! $errors->first("flag_".$input, '<span class="label label-danger">:message</span>') !!}
						</div>
						<?php
						$number++;
						?>
						@endif
						@endforeach
					</div>
				</fieldset>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
	var data = <?= json_encode($data) ?>,
	spbu_level = <?= json_encode(session('auth')->spbu_level) ?>,
	teknisi = <?= json_encode(session('auth')->id_user) ?>;
	$( document ).ajaxStop(function() {
		var date = new Date();
		data.close = data.close != null ? data.close : date.getFullYear() + '-' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + ' ' + ((date.getHours() > 9) ? date.getHours() : ('0' + date.getHours())) + ':' + ((date.getMinutes() > 9) ? date.getMinutes() : ('0' + date.getMinutes()));
		var date_future = new Date(data.close),
		date_now = new Date(data.open),
		seconds = Math.floor((date_future - (date_now))/1000),
        minutes = Math.floor(seconds/60),
        hours = Math.floor(minutes/60),
        days = Math.floor(hours/24),
        hours = hours-(days*24),
        minutes = minutes-(days*24*60)-(hours*60),
        seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);
        var result='';
        if(days != 0){
        	result += days + ' Hari ';
        }
        if(hours != 0){
        	result += hours + ' Jam ';
        }
        if(minutes != 0){
        	result += minutes + ' Menit ';
        }
        $(".modal-title").html(result);
        $(".modal-title").html("<p style='font-size: 18px;'><b>Durasi Pengerjaan selama <u>" + result + "</u> Dimulai dari <u>"+ data.open.substr(0, 16) + "</u> sampai <u>" + data.close.substr(0, 16) + "</u></b></p>");
				if(data.stts != 0){
					$('.btn_delete').css({'display': 'block'});
        	$(".brtn_del").attr('href', '/Spbu/delete_wo/tiket_rusak/' + data.id);
				}else{
					$('.btn_delete').css({'display': 'none'});
        	$(".brtn_del").attr('href', '');
				}
				if(data.id_user == teknisi || [1, 4].includes(spbu_level) || teknisi == 18940469){
					console.log(data.stts, data.id_user, teknisi, spbu_level)
					$('.btn_up').css({'display': 'block'})
        	$(".brtn_up").attr('href', '/Spbu/update_perangkatRusk/' + data.id);
				}else{
					$('.btn_up').css({'display': 'none'})
				}
    });

	$(function(){
			$('.foto_catata').css({
				'cursor':'not-allowed',
				'color' : 'black'
			})

			$('.foto_catata').attr('readonly', 'readonly')
		});
	</script>

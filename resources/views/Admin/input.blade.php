@extends('layout')
@section('title', (empty($data) ? 'Input Data SPBU' : 'Edit Data'))
@section('style')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style type="text/css">


    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }

    .select2-results__option, select{
        color: black;
    }

    #map {
        position: absolute; top: 0; bottom: 0; width: 90vw; height: 100%;
    }

    .mapboxgl-canvas {
        left: 0;
    }

    .modal-xl{
        width: 100%;
        max-width:1300px;
    }

    .modal-body{
        height: 400px;
    }

    .foto {
        border: 3px solid white;
        resize: none;

        margin-bottom: 4px;
    }

    .coordinates {
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        position: absolute;
        bottom: 40px;
        left: 10px;
        padding: 5px 10px;
        margin: 0;
        font-size: 11px;
        line-height: 18px;
        border-radius: 3px;
        display: none;
    }

</style>
@endsection
@section('content')
<div class="modal fade" id="modal-large" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Find Me</h4>
            </div>
            <div class="modal-body">
                <div id="map"></div>
                <pre id="coordinates" class="coordinates"></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
            </div>
        </div>
    </div>
</div>
<div class="body">
    <div class="row">
        <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-check-square-o"></i>
                            Pengisian Data Tiket SPBU
                        </h4>
                    </header>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{-- pra kontrak --}}
                    <fieldset>
                        <legend class="section">Tiket SPBU</legend>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_spbu">Id SPBU</label>
                            <div class="col-md-7">
                                <select id="id_spbu" name="id_spbu" class="admin_select selectpicker"
                                data-style="btn-default" data-width="auto"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Pilih ID SPBU nya!" required></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="sto">Sto</label>
                            <div class="col-md-7">
                                <input type="text" id="sto" name="sto" class="form-control admin input-transparent" readonly="readonly" style="cursor: not-allowed;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_alamat">Alamat</label>
                            <div class="col-md-7">
                                <input type="text" id="id_alamat" name="id_alamat" class="form-control admin input-transparent" readonly="readonly" style="cursor: not-allowed;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="id_status">Status</label>
                            <div class="col-md-7">
                                <input type="text" id="id_status" name="id_status" class="form-control admin input-transparent" readonly="readonly" style="cursor: not-allowed;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="jenis_g">Jenis Gangguan</label>
                            <div class="col-md-7">
                                <select id="jenis_g" name="jenis_g" class="admin_select selectpicker" data-style="btn-default" data-width="auto">
                                    <option value="Troubleshoot">TROUBLESHOOT</option>
                                    <option value="Grounding">GROUNDING</option>
                                    <option value="Perangkat Rusak">Perangkat Rusak</option>
                                    <option value="Pergantian Perangkat">Pergantian Perangkat</option>
                                    <option value="Preventive Maintenance">Preventive Maintenance</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="alert alert-danger">
                            <ul>
                                <li class="errornya"></li>
                            </ul>
                        </div> --}}
                        <div class="form-group">
                            <label class="control-label col-md-5" for="teknisi">Teknisi</label>
                            <div class="col-md-7">
                                <select id="teknisi" name="teknisi" class="admin_select selectpicker"
                                data-style="btn-default" data-width="auto"
                                data-parsley-trigger="change"
                                required
                                data-parsley-required-message="Pilih Teknisi yang Mengerjakan!"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5" for="stts">Keterangan Gangguan</label>
                            <div class="col-md-7">
                                <textarea rows="4" style="resize:none;" cols="50" id="ggn_open" name="ggn_open" class="admin form-control input-transparent"
                                    data-parsley-trigger="change"
                                    data-parsley-required-message="Catatan Isi!"
                                    required
                                    >{{ $data->ggn_open ?? '' }}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn_sbmt btn-danger">Validate &amp; Submit</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript">
    function isObjectEmpty(obj) {
        for ( var name in obj ) {
            return false;
        }
        return true;
    }

    $(function(){
        $('#jenis_g').select2({
            width: '100%'
        });

        var data = <?= json_encode($data) ?>;
        $('#teknisi').select2({
            width: '100%',
            placeholder: "Pilih Teknisi",
            minimumInputLength: 7,
            allowClear: true,
            ajax: {
                url: "/input/teknisi/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                language: {
                    noResults: function(){
                        return "User Tidak Terdaftar!";
                    }
                },
                cache: true,
                success: function(value) {

                }
            }
        });

        $('#id_spbu').select2({
            width: '100%',
            placeholder: "Pilih ID SPBU",
            minimumInputLength: 3,
            allowClear: true,
            ajax: {
                url: "/input/spbu_id/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true,
                success: function(value) {

                }
            }
        }).on("select2:open", function() {
            $(".select2-search__field").val('');
            $(".select2-search__field").attr('type', 'text');
            // $(".select2-search__field").attr('style', 'width: 10em;');
            $(".select2-search__field").inputmask({
                mask: '99-9{6,8}',
                placeholder: ''
            });
        }).on("select2:close", function() {
            $(".select2-search__field").inputmask('remove');
            $(".select2-search__field").val(null);
            $('#id_alamat').val(null);
            $('#id_status').val(null);
            $('#sto').val(null);
            $('#teknisi').empty().trigger('change');
        });

        $('#id_spbu').on('select2:select', function (e) {
            $.ajax({
                type: 'GET',
                url: '/input/alamat_stts/search',
                dataType: 'json',
                data: {id_spbu: $('#id_spbu').val() },
                success: function(value) {
                    $('#id_alamat').val(value.alamat);
                    $('#id_status').val(value.status);
                    $('#sto').val(value.sto);
                    $('#teknisi').append($("<option selected='selected'></option>").val(value.naker).text(value.nama+' ('+value.naker+')')).trigger('change');
                }
            });
        });

        if(!isObjectEmpty(data)){
            console.log(data)
            $('#teknisi').append($("<option selected='selected'></option>").val(data.id_user).text(data.nama+' ('+data.id_user+')')).trigger('change');

            $('#id_spbu').append($("<option selected='selected'></option>").val(data.id_spbu).text(data.id_spbu)).trigger('change');

            $('#jenis_g').val(data.jenis_g).change();

            $.ajax({
                type: 'GET',
                url: '/input/alamat_stts/search',
                dataType: 'json',
                data: {id_spbu: data.id_spbu},
                success: function(value) {
                    $('#id_alamat').val(value.alamat);
                    $('#id_status').val(value.status);
                    $('#sto').val(value.sto);
                }
            });
        }

        $("#validation-form").on('submit', function() {
            $(this).parsley().validate();
            if ($(this).parsley().isValid()) {
                $('.btn_sbmt').attr('disabled');
            }
        });
    });
</script>
@endsection
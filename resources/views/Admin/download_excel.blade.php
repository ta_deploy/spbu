@extends('layout')
@section('title', (empty($data) ? 'Input Data SPBU' : 'Edit Data'))
@section('style')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style type="text/css">
  .daterangepicker  {
		background-color: #414e60;
	}

	.select2-results__option, select{
        color: black;
    }

	.select2-selection__choice, select{
        color: black;
  }

	.daterangepicker .month, .daterange {
		color: black;
	}

	.daterangepicker .calendar-table {
		background-color: #4a4d50;
	}

	.daterange {
		width: 100%;
	}
</style>
@endsection
@section('content')
<div class="body">
  <div class="row">
    <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-12">
        <section class="widget">
          <header>
            <h4>
              <i class="fa fa-check-square-o"></i>
              Form Download
            </h4>
          </header>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          {{-- pra kontrak --}}
          <fieldset>
            <legend class="section">Download Excel</legend>
            <div class="form-group row">
							<div class="col-sm-12">
								<div class="input-group date">
									<input type="text" name="daterange" class="form-control daterange" readonly value=" {{ @$prev_data['daterange'] }}">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
									</span>
								</div>
							</div>
						</div>
            <div class="form-group">
              <label class="control-label col-md-3" for="jenis">Download</label>
              <div class="col-md-9">
                  <select id="jenis" name="jenis" class="admin_select selectpicker" data-style="btn-default" data-width="auto">
                    <option value="Pengerjaan"> Pengerjaan</option>
                    <option value="Perangkat Rusak"> Perangkat Rusak</option>
                  </select>
              </div>
            </div>
          </fieldset>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn_sbmt btn-danger">Validate &amp; Submit</button>
                  <button type="button" class="btn btn-default">Cancel</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script type="text/javascript">
    function isObjectEmpty(obj) {
        for ( var name in obj ) {
            return false;
        }
        return true;
    }

    $(function(){

      $('#jenis').select2({
        'width': '100%'
      });

      $('input[name="daterange"]').daterangepicker({
        opens: 'left',
        "locale": {
          "format": "YYYY/MM/DD",
          "separator": " - ",
        }
      });

      $('.kalender').click(function(e){
        e.preventDefault();
        $('input[name="daterange"]').click();
      });
    });
</script>
@endsection
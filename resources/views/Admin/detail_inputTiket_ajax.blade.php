<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<style type="text/css">
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}

	#map {
		position: absolute; top: 0; bottom: 0; width: 99%; height: 100%;
	}

	.mapboxgl-canvas {
		left: 0;
	}

	.foto_catata {
		border: 3px solid white;
		resize: none;
		margin-bottom: 4px;
	}

</style>
<div class="body">
	<div class="row">
		<div class="col-md-5">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-check-square-o"></i>
						Data Tiket SPBU
					</h4>
				</header>
				{{-- pra kontrak --}}
				<form class="form-horizontal">
					<fieldset>
						<legend class="section">Tiket SPBU</legend>
						<div class="form-group">
							<label class="control-label col-md-4" for="sto">Sto:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->kota }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_spbu">Id SPBU:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->id_spbu }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_spbu">Naker Sektor:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->naker_nama }} ({{ $data->naker_id }})</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="no_tiket">Nomor Tiket:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->no_tiket }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_alamat">Alamat:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->alamat }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="id_status">Status:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->type }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Gangguan:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->jenis_g }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="jenis_g">Teknisi Pengerjaan:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->nama }}  {{ $data->teknisi }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="act">Action:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->act == 1 ? 'Vcall' : "Kunjungan" }}</b></p>
							</div>
						</div>
						@foreach($detail_tim as $k => $v)
							<div class="form-group row knjng" style="display: block; margin-top: 5px;">
								<div class="col-md-offset-2 col-md-3" style="width: 20%">
									<p type="text" class="form-control input-transparent">{{ $v->jenis_detail }}'></p>
								</div>
								<div class="col-md-3">
									<p type="text" class="form-control input-transparent">{{ $v->sn }}'></p>
								</div>
								<div class="col-md-4">
									<p type="text" class="form-control input-transparent">{{ $v->no_tiket }}'></p>
								</div>
							</div>
						@endforeach
						<div class="form-group">
							<label class="control-label col-md-4" for="berita_ac_sel">Berita Acara:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->berita_ac_sel == 0 ? 'NOK' : "OK" }}</b></p>
								@if($data->berita_ac_sel == 1)
								<p class="form-control input-transparent"><b>{{ $data->note_rusak }}</b></p>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="koor">Koordinat</label>
							@if(!empty($data->koor))
							<div class="col-md-8" style="height: 250px; right:12px;">
								<div id="map"></div>
							</div>
							@else
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>Belum Ada Koordinat!</b></p>
							</div>
							@endif
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="note">Catatan:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><b>{{ $data->note ?: 'kosong' }}</b></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4" for="stts">Status:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent"><span class="label label-{{ ($data->stts == 0 ? 'important' :( $data->stts == 1 ? 'success' : 'warning'))}}">{{ ($data->stts == 0 ? 'Closed' :( $data->stts == 1 ? 'Open' : 'Pending'))}}</span></p>
							</div>
						</div>
					</fieldset>
				</form>
			</section>
		</div>
		<div class="col-md-7">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-file-image-o"></i>
						Upload Foto Berita Acara
					</h4>
				</header>
				<fieldset>
					<legend class="section">Foto Pengerjaan</legend>
					<div class="row text-center input-photos" style="margin: 20px 0">
						<?php
						$number = 1;
						clearstatcache();
						?>
						@foreach($photodispatch as $input)
						@php
						if(in_array($input, ['SN_Rusak', 'Pengganti', 'Cover SPBU', 'Berita Acara'], true)){
							$cls = "rusakk";
							$style = 'display: none;';
						}else{
							$cls = '';
							$style = 'display: block;';
						}
						@endphp
						<?php
						$path = "/upload/spbu/{$id}/$input";
						$path2 = "/upload2/spbu/{$id}/$input";
						$name = "flag_".$input;
						$flag = "";


						if(file_exists(public_path()."$path-th.jpg") )
						{
							$th   = "$path-th.jpg";
							$img  = "$path.jpg";
							$nt = "$path-catatan.txt";
						}
						else
						{
							$th   = "$path2-th.jpg";
							$img  = "$path2.jpg";
							$nt = "$path2-catatan.txt";
						}
						?>
						@if (file_exists(public_path().$th))
						<?php
						$flag = 1;
						$images = $img;
						$src = $th;
						$th = $th;
						?>
						<div class="col-6 col-sm-3 {{ $cls }}" style="{{ $style }}">
							<a href="{{ $images }}">
								<img src="{{ $src }}" alt="{{ $input }}" id="img-{{ $input }}" class="photo_valid_dis" />
							</a>
							<br />
							<p style="margin-bottom: 3px;">{{ str_replace('_',' ',$input) }}</p>
							@if (file_exists(public_path().$nt))
							<?php $note = File::get(public_path($nt)); ?>
							@else
							<?php $note ='' ?>
							@endif
							<textarea class="form-control foto_catata" id="catatan_{{$input}}" name="catatan_{{$input}}">{{$note}}</textarea>
							{!! $errors->first("flag_".$input, '<span class="label label-danger">:message</span>') !!}
						</div>
						<?php
						$number++;
						?>
						@endif
						@endforeach
					</div>
				</fieldset>
			</section>
		</div>
		@if($data->jenis_g == 'Grounding')
			<div class="col-md-12">
				<section class="widget">
					<header>
						<h4>
							<i class="fa fa-file-image-o"></i>
							Upload Foto Instalasi Grounding SPBU
						</h4>
					</header>
					<fieldset>
						<legend class="section">Foto Instalasi Grounding SPBU</legend>
						<div class="row text-center input-photos" style="margin: 20px 0">
							@foreach($chk as $dt)
								@foreach($dt as $key => $d)
									<div class="col-md-12">
										<section class="widget">
											<fieldset>
												<legend class="section">
														{{ $key }}
												</legend>
												<div class="row text-center input-photos" style="margin: 20px 0">
													<div class="body">
														@foreach($d as $new_d)
														<?php
																	$path = "/upload/spbu/$id/chckbox/".$new_d['nama'];
																	$path2 = "/upload2/spbu/$id/chckbox/".$new_d['nama'];
																	$flag = "";
																	$name = "flag_".$new_d['nama'];


																	if(file_exists(public_path()."$path-th.jpg") )
																	{
																		$th   = "$path-th.jpg";
																		$img  = "$path.jpg";
																		$nt = "$path-catatan.txt";
																	}
																	else
																	{
																		$th   = "$path2-th.jpg";
																		$img  = "$path2.jpg";
																		$nt = "$path2-catatan.txt";
																	}
																	?>
															@if (file_exists(public_path().$th))
															<?php
																	$flag = 1;
																	$images = $img;
																	$src = $th;
																	$th = $th;
																	?>
															<div class="col-6 col-md-3">
																<div class="colm-data input-photos chk_group_img" data-src= "{{ $src }}">
																	<a href="{{ $images }}">
																			<img src="{{ $src }}" alt="{{ $new_d['id'] }}" id="img-{{ $new_d['id'] }}" class="photo_valid_dis" />
																	</a>
																	<br />
																	@if (file_exists(public_path().$nt))
																	<?php $note = File::get(public_path($nt)); ?>
																	@else
																	<?php $note ='' ?>
																	@endif
																	<textarea name="catatan_chk_{{ $new_d['id'] }}" class="form-control foto foto_catata">{{ $note }}</textarea>
																</div>
															</div>
															@endif
														@endforeach
													</div>
												</div>
											</fieldset>
										</section>
									</div>
								@endforeach
							@endforeach
						</div>
					</fieldset>
				</section>
			</div>
		@endif
	</div>
</div>
<script type="text/javascript">
	var data = <?= json_encode($data) ?>,
	spbu_level = <?= json_encode(session('auth')->spbu_level) ?>,
	teknisi = <?= json_encode(session('auth')->id_user) ?>;
	$( document ).ajaxStop(function() {
		var date = new Date();
		data.close = data.close != null ? data.close : date.getFullYear() + '-' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + ' ' + ((date.getHours() > 9) ? date.getHours() : ('0' + date.getHours())) + ':' + ((date.getMinutes() > 9) ? date.getMinutes() : ('0' + date.getMinutes()));
		var date_future = new Date(data.close),
		date_now = new Date(data.open),
		seconds = Math.floor((date_future - (date_now))/1000),
        minutes = Math.floor(seconds/60),
        hours = Math.floor(minutes/60),
        days = Math.floor(hours/24),
        hours = hours-(days*24),
        minutes = minutes-(days*24*60)-(hours*60),
        seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);
        var result='';
        if(days != 0){
        	result += days + ' Hari ';
        }
        if(hours != 0){
        	result += hours + ' Jam ';
        }
        if(minutes != 0){
        	result += minutes + ' Menit ';
        }
        $(".modal-title").html(result);
        $(".modal-title").html("<p style='font-size: 18px;'><b>Durasi Pengerjaan selama <u>" + result + "</u> Dimulai dari <u>"+ data.open.substr(0, 16) + "</u> sampai <u>" + data.close.substr(0, 16) + "</u></b></p>");
				if(data.stts != 0){
					$('.btn_delete').css({'display': 'block'});
        	$(".brtn_del").attr('href', '/Spbu/delete_wo/tiket/' + data.id);
				}else{
					$('.btn_delete').css({'display': 'none'});
        	$(".brtn_del").attr('href', '');
				}
				if(data.id_user == teknisi || [1, 4].includes(spbu_level) || teknisi == 18940469){
					console.log(data.stts, data.id_user, teknisi, spbu_level)
					$('.btn_edit, .btn_up').css({'display': 'block'})
        	$(".brtn_edit").attr('href', '/Spbu/' + data.id);
        	$(".brtn_up").attr('href', '/Spbu/update_wo/' + data.id);
				}else{
					$('.btn_edit, .btn_up').css({'display': 'none'})
        	$(".brtn_edit, .brtn_up").attr('href', '');
				}
    });

	$(function(){
		if(data.koor != null){
			var split = data.koor.split(',');
			var put_Cen = JSON.parse("["+split[1]+","+split[0]+"]");
			$.when($.getScript('https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js')
			).done(function( x ) {
				mapboxgl.accessToken = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
				var map = new mapboxgl.Map({
					container: 'map',
					style: 'mapbox://styles/mapbox/streets-v11',
					center: put_Cen,
					zoom: 20
				});

				var popup = new mapboxgl.Popup({ closeOnClick: false })
				.setLngLat(put_Cen)
				.setHTML('<b style="color: black;">'+data.koor+'</b>')
				.addTo(map);

				var marker = new mapboxgl.Marker()
				.setLngLat(put_Cen)
				.addTo(map);

				map.on('load', function () {
					map.resize();
				});

			}).fail(function(jqxhr, textStatus, errorThrown) {
				/*console.log(textStatus, errorThrown)*/
			});
		}

			$('.foto_catata').css({
				'cursor':'not-allowed',
				'color' : 'black'
			})

			$('.foto_catata').attr('readonly', 'readonly')

			$('#berita_ac_sel').change( function(){
				if($(this).val() == 0){
					$('#note_rusak').css({
						'display': 'block'
					});
					$('.rusakk').css({
						'display': 'none'
					});
				}else{
					$('#note_rusak').css({
						'display': 'none'
					});
					$('.rusakk').css({
						'display': 'block'
					});
				}
			});
		});
	</script>

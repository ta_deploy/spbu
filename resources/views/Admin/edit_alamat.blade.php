@extends('layout')
@section('title', (empty($data) ? 'Input Alamat SPBU' : 'Edit Alamat'))
@section('style')
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
    .select2-results__option, select{
        color: black;
    }
</style>
@endsection
@section('content')
<div class="body">
    <div class="row">
        <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate>
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-check-square-o"></i>
                            Edit Alamat
                        </h4>
                    </header>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{-- pra kontrak --}}
                    <fieldset>
                        <legend class="section">Alamat</legend>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="id_spbu">Id SPBU</label>
                            <div class="col-md-8">
                                <input id="id" name="id" class="form-control input-transparent"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Isi ID SPBU!"
                                value="{{ !empty($data->id_spbu) ? $data->id_spbu : '' }}"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="teknisi">TL Bertanggung Jawab</label>
                            <div class="col-md-8">
                                <select id="teknisi" name="teknisi" class="admin_select selectpicker"
                                data-style="btn-default" data-width="auto"
                                data-parsley-trigger="change"
                                data-parsley-minSelect="2"
                                data-parsley-required-message="Pilih Teknisi Teritori!"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="sto">Kota</label>
                            <div class="col-md-8">
                                <select id="kota" name="kota" class="admin_select selectpicker" data-style="btn-default" data-width="auto" data-parsley-trigger="change" data-parsley-required-message="Pilih Kota nya!" required="required">
                                    @foreach($kota as $k)
                                        <option value="{{ $k->kota }}">{{ $k->kota }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="id_status">Status</label>
                            <div class="col-md-8">
                                <select id="status" name="status" class="admin_select selectpicker" data-style="btn-default" data-width="auto" data-parsley-trigger="change"data-parsley-required-message="Pilih Status nya!" required="required">
                                    @foreach($status as $k)
                                        <option value="{{ $k->status }}">{{ $k->status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="alamat">Alamat</label>
                            <div class="col-md-8">
                                <input id="alamat" name="alamat" class="form-control input-transparent"
                                data-parsley-trigger="change"
                                data-parsley-required-message="Pilih Alamat!"
                                value="{{ !empty($data->alamat) ? $data->alamat : '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="stts">Tipe</label>
                            <div class="col-md-8">
                                <select id="tipe" name="tipe" class="admin_select selectpicker" data-style="btn-default" data-width="auto" data-parsley-trigger="change" data-parsley-required-message="Pilih Tipe nya!" required="required">
                                    @foreach($type as $k)
                                        <option value="{{ $k->type }}">{{ $k->type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn_sbmt btn-danger">Validate &amp; Submit</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.0/mapbox-gl.js"></script>
<script type="text/javascript">
    function isObjectEmpty(obj) {
        for ( var name in obj ) {
            return false;
        }
        return true;
    }

    $(function(){
        var data = {!! json_encode($data) !!};
        $('#teknisi').select2({
            width: '100%',
            placeholder: "Pilih Teknisi",
            minimumInputLength: 7,
            allowClear: true,
            ajax: {
                url: "/input/teknisi/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true,
                success: function(value) {

                }
            }
        });

        $('#kota, #status, #tipe').select2({
            width: '100%'
        });

        if(!isObjectEmpty(data)){
            $('#kota').val(data.kota).trigger('change');
            $('#status').val(data.status).trigger('change');
            $('#tipe').val(data.type).trigger('change');

            var teknisi = $("<option selected='selected'></option>").val(data.naker).text(data.nama+' ('+data.naker+')');
            $('#teknisi').append(teknisi).trigger('change');
        }

        $("#validation-form").on('submit', function() {
            $(this).parsley().validate();
            if ($(this).parsley().isValid()) {
                $('.btn_sbmt').attr('disabled');
            }
        });
    });
</script>
@endsection
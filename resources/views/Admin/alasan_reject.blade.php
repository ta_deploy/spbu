@extends('layout')
@section('title', 'Reject Absensi')
@section('style')
<style type="text/css">
  .input-photos img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
  }

  .foto {
      border: 3px solid white;
      resize: none;

      margin-bottom: 4px;
  }
</style>
@endsection
@section('content')
<div class="body">
  <div class="row">
    <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" autocomplete="off">
        <div class="col-md-12">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-file-image-o"></i>
                        Foto Absensi
                    </h4>
                </header>
                <fieldset>
                    <div class="row text-center input-photos" style="margin: 20px 0">
                        <?php
                            $number = 1;
                            clearstatcache();
                        ?>
                        @foreach($foto as $input)
                        <div class="col-6 col-sm-4">
                            <?php
                                $path = "/upload/spbu/absensi/$id/$input";
                                $path2 = "/upload2/spbu/absensi/$id/$input";

                                if(file_exists(public_path()."$path-th.jpg") )
                                {
                                    $th  = "$path-th.jpg";
                                    $img = "$path.jpg";
                                }
                                else
                                {
                                    $th  = "$path2-th.jpg";
                                    $img = "$path2.jpg";
                                }
                            ?>
                            @if (file_exists(public_path().$th))
                                <?php
                                $images = $img;
                                $src = $th;
                                ?>
                            @else
                                <?php
                                $images = "";
                                $src = "/img/placeholder.gif";
                                ?>
                            @endif
                            <a href="{{ $images }}">
                                <img src="{{ $src }}" alt="{{ $input }}" id="img-{{ $input }}" class="photo_valid_dis" />
                            </a>
                            <br />
                            <p style="margin-bottom: 3px;">{{ str_replace('_',' ',$input) }}</p>
                        </div>
                        <?php
                        $number++;
                        ?>
                        @endforeach
                        <form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate >
                          {{ csrf_field() }}
                          <textarea class="form-control" name="detail" ></textarea>
                          <button type="submit" class="btn btn-danger">Submit Reject</button>
                        </form>
                    </div>
                </fieldset>
            </section>
        </div>
    </form>
  </div>
</div>
@endsection
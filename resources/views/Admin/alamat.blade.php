@extends('layout')
@section('title', 'List Alamat')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha512-3kAToXGLroNvC/4WUnxTIPnfsiaMlCn0blp0pl6bmR9X6ibIiBZAi9wXmvpmg1cTyd2CMrxnMxqj7D12Gn5rlw==" crossorigin="anonymous" />
<style type="text/css">
    .form-group.footable-filtering-search .input-group-btn {
        display: none;
    }
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						List <span class="fw-semi-bold">Alamat</span>
					</h4>
				</header>
				<div class="body">
					<a type="button" href="/input/alamat" class="btn btn_edit btn-primary">Input Alamat</a>
					<table class="table dbs table-striped" data-sorting="true" data-filtering="true">
						<thead>
							<tr>
								<th class="hidden-xs">#</th>
								<th>ID SPBU</th>
								<th>Naker</th>
								<th>Alamat</th>
								<th>Kota</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php $no = 0; @endphp
							@foreach($data as $d)
							<tr>
								<td>{{ ++$no }}</td>
								<td>{{ $d->id_spbu }}</td>
								<td>{{ $d->nama }} ({{ $d->naker }})</td>
								<td>{{ $d->alamat }}</td>
								<td>{{ $d->kota }}</td>
								<td>{{ $d->status }}</td>
								<td><a type="button" href="/edit/alamat/{{ $d->id }}" class="btn btn_edit btn-info">Edit Alamat</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div class="holder"></div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha512-aVkYzM2YOmzQjeGEWEU35q7PkozW0vYwEXYi0Ko06oVC4NdNzALflDEyqMB5/wB4wH50DmizI1nLDxBE6swF3g==" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(function(){
		$('.table').footable({
            'paging': {
                'enabled': true,
                'size': 10
            }
        });
	})
</script>
@endsection
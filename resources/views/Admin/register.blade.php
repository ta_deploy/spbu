@extends('layout')
@section("title", empty($data) ? "Tambah User" : "Edit User")
@section('style')
<link href="/bower_components/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
<style type="text/css">
	.select2-results__option, select{
        color: black;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="user_form">
		<div class="panel panel-info">
			<div class="panel-heading">{{ empty($edit) ? 'Tambah User' : 'Edit User' }}</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Nama</label>
						<input type="text" class="form-control" id="nama" name="nama" required placeholder="Masukkan Nama Karyawan" value="{{ old('nama', $data->nama ?? '') }}">
				</div>
				<div class="form-group">
					<label>Password</label>
						<input type="password" class="form-control" id="pass" name="pass" placeholder="Masukkan Password" value="">
						<input type="checkbox" class="form-checkbox" id="chk_bx"><label for="chk_bx">&nbsp; Lihat Password</label>
					@if (empty($edit))
						<span class="label label-rounde label-info" style="color: black; font-size:13px;">*Jika kosong, password tidak akan berubah!</span>
					@endif
				</div>
					<div class="form-group">
							<label>Nama Instansi</label>
							<select id="mitra" name="mitra" class="selectpicker" data-style="btn-default" data-width="auto">
							</select>
					</div>
				<div class="form-group">
					<label>Pekerjaan</label>
					<select id="job" name="job" class="selectpicker" data-style="btn-default" data-width="auto">
						<option value="2">Teknisi</option>
						<option value="3">Monitor</option>
						<option value="1">Admin</option>
					</select>
				</div>
				<div class="form-group">
						<button type="submit" class="btn btn-block btn-info bt_sv" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan Perubahan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script src="/bower_components/bootstrap-switch/bootstrap-switch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
<script>
	$(function(){
		function isEmpty(obj) {
			for(var prop in obj) {
				if(obj.hasOwnProperty(prop))
					return false;
			}
			return true;
		}

		var load = <?= json_encode($data) ?>;
		if(isEmpty(load) == true){
			var jenis ='new';
		}else{
			var jenis = 'exists';

			$('#job').val(load.spbu_level).change();

			$('#mitra').val(load.nama_instansi).change();
		}

		$("#nik").on("keypress keyup",function (e){
				if ($(this).val().length >= 6) {
					$.ajax({
						url: '/get/cek_user',
						type: 'GET',
						data: {
							data: $(this).val(),
							jenis: jenis
						},
					}).done(function(data){
						console.log($.isEmptyObject(data))
						if(!$.isEmptyObject(data)){
							$('.notif').css({
								'display': 'block'
							});
							$('.bt_sv').prop('disabled', true);
						} else {
							$('.notif').css({
								'display': 'none'
							});
							$('.bt_sv').removeAttr('disabled');
						}
					})
				} else {
					$('.bt_sv').prop('disabled', true);
				}
			});

		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('#pass').attr('type','text');
			}else{
				$('#pass').attr('type','password');
			}
		});

		$('#job').select2({ width: '100%'});

		$('#mitra').select2({
				width: '100%',
				data: <?= json_encode($mitra) ?>
		})

    });
</script>
@endsection
@extends('layout')
@section('title', 'List Inventory')
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
	<div class="row">
		@forelse ($data_inven as $key => $data)
			<div class="col-md-12">
				<section class="widget">
					<header>
						<h4>
							Kota <span class="fw-semi-bold">{{ $key }}</span>
						</h4>
						<div class="widget-controls">
							<a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-plus"></i></a>
							<a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-minus"></i></a>
						</div>
					</header>
					<div class="body">
						<table class="table dbs table-striped">
							<thead>
								<tr>
									<th>Teknisi</th>
									<th>ID Spbu</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($data as $nik => $child_dt )
									<tr>
										<td>{{ $nik }}</td>
										<td>
											@foreach ($child_dt as $value)
												@php
													$no = $total = 0;
													foreach($foto as $key => $raw_data)
													{
														for ($x = 0; $x <= $raw_data['Evidence']; $x++)
														{
															$p_raw1 = "/upload/spbu/inventory/$value->id_spbu/".$key."_".$x;
															$p_raw2 = "/upload2/spbu/inventory/$value->id_spbu/".$key."_".$x;

															if(file_exists(public_path()."$p_raw1-th.jpg") )
															{
																$path = $p_raw1;
															}
															else
															{
																$path = $p_raw2;
															}

															$th   = "$path-th.jpg";

															if (file_exists(public_path().$th))
															{
																 ++$no;
															}
															++$total;
														}
													}
												@endphp
												<a href="{{ in_array(session('auth')->spbu_level, [1, 2, 4]) ? '/update_inventory/'.$value->id_spbu : '/view_inventory'.$value->id_spbu }}"><span class=' label label-info' style='margin-left: 0.3EM; cursor: pointer;'>{{ $value->id_spbu }} <span class='label label-primary' style="color: black">{{ $no }} / {{ $total }}</span></span></a>
											@endforeach
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</section>
			</div>
		@empty
			<p>Nik tidak memiliki ID SPBU</p>
		@endforelse
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_component/widgster/widgster.js"></script>
<script>
	$(function(){
		$('.widget').widgster();
	});
</script>
@endsection
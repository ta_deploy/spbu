@extends('layout')
@section('title', "Inventory $id" )
@section('style')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
    .input-photos img {
    width: 100px;
    height: 150px;
    margin-bottom: 5px;
    }
		.select2-results__option, select{
        color: black;
    }
    .foto {
    border: 3px solid white;
    resize: none;

    margin-bottom: 4px;
    }
</style>
@endsection
@section('content')
<div class="body">
	<div class="row">
		<form id="validation-form" class="form-horizontal form-label-left" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
			{{ csrf_field() }}
			<div class="col-md-12">
				<header>
						<h5>
						Data <strong>Inventory</strong>
						</h5>
				</header>
				@foreach($foto as $key => $raw_data)
					<div class="col-md-6">
						<section class="widget">
						<header>
							<h4>
							<i class="fa fa-file-image-o"></i>
							Upload Foto {{ str_replace("_"," ", $key) }}
							</h4>
						</header>
						<fieldset>
							<div class="widget-controls">
									<a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-plus"></i></a>
									<a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-minus"></i></a>
							</div>
							<div class="row text-center input-photos" style="margin: 20px 0">
								@for ($x = 0; $x <= $raw_data['Evidence']; $x++)
									<?php
										$p_raw1 = "/upload/spbu/inventory/$id/".$key."_".$x;
										$p_raw2 = "/upload2/spbu/inventory/$id/".$key."_".$x;

										if(file_exists($p_raw1) )
										{
											$path = $p_raw1;
										}
										else
										{
											$path = $p_raw2;
										}

										$th   = "$path-th.jpg";
										$img  = "$path.jpg";
										$inven = "$path-inven.txt";
										$tipe = "$path-tipe.txt";
										$status = "$path-status.txt";
										$spbu = "$path-spbu.txt";
										$flag = "";
										$name = "flag_".$key;
									?>
									@if (file_exists(public_path().$th))
										<?php
											$flag = 1;
											$images = $img;
											$src = $th;
										?>
										<div class="body">
											<div class="col-6 col-md-12">
												<div class="" data-src= "{{ $src }}">
													<a href="{{ $images }}">
														<img src="{{ $src }}" alt="{{ $key }}_{{ $x }}" id="img-{{ $key }}_{{ $x }}" class="photo_valid_dis" />
													</a>
												<br />
												<input type="text" class="hidden" name="flag_{{ $key }}_{{ $x }}" value="{{ $flag or '' }}"/>
												<input type="file" class="hidden" name="photo-{{ $key }}_{{ $x }}" accept="image/jpeg" />
												@if (in_array(session('auth')->spbu_level, [1, 2, 4]))
													<button type="button" class="btn btn-sm btn-info">
														<i class="glyphicon glyphicon-camera"></i>
													</button>
												@endif
												<div style="margin-top:6px; margin-bottom: 3px;">
													@if (file_exists(public_path().$tipe))
														<?php
															$pilih_tipe = File::get(public_path($tipe));
														?>
													@else
														<?php $pilih_tipe ='' ?>
													@endif
															<input type="text" disabled class="form-control input-transparent" value="{{ $pilih_tipe }}">
												</div>
												<div style="margin-top:3px; margin-bottom: 3px;">
													@if (file_exists(public_path().$status))
														<?php
															$pilih_stts = File::get(public_path($status));
														?>
													@else
														<?php $pilih_stts = '' ?>
													@endif
														<input type="text" disabled class="form-control input-transparent" value="{{ $pilih_stts }}">
												</div>
													<div style="margin-top:3px; margin-bottom: 6px; display: none;" id="div_spbu_{{ $key }}_{{ $x }}">
														@if (file_exists(public_path().$spbu))
															<?php
																$pilih_spbu = File::get(public_path($spbu));
															?>
														@else
															<?php $pilih_spbu = null ?>
														@endif
														<input type="text" disabled class="form-control input-transparent" value="{{ $pilih_spbu }}">
													</div>
												@if (file_exists(public_path().$inven))
													<?php
														$note = File::get(public_path($inven));
													?>
												@else
													<?php $note ='' ?>
												@endif
													<textarea name="catatan_inven_{{ $key }}_{{ $x }}" {{ !in_array(session('auth')->spbu_level, [1, 2, 4]) ? 'disabled' : '' }} class="form-control foto">{{ $note }}</textarea>
												</div>
											</div>
										</div>
									@endif
								@endfor
							</div>
						</fieldset>
						</section>
					</div>
				@endforeach
			</div>
			<div class="col-md-12">
				<section class="widget">
				<header>
					<h5>
						Log <strong>Inventory {{ $id }}</strong>
					</h5>
				</header>
				<fieldset>
						<div class="body">
							<table class="table">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Naker</th>
										<th scope="col">Tanggal Update</th>
									</tr>
								</thead>
								<tbody>
									@forelse  ($log as $key => $l )
										<tr>
											<th scope="row">{{ ++$key }}</th>
											<td>{{ $l->nama_naker }}</td>
											<td>{{ $l->created_at }}</td>
										</tr>
									@empty
										<tr>
											<td colspan="3" style="text-align: center;">Tidak ada data!</td>
										</tr>
									@endforelse
								</tbody>
							</table>
						</div>
					</fieldset>
				</section>
		</div>
		</form>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<script src="/bower_component/widgster/widgster.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript">
	$(function(){

	$('.widget').widgster();

	var foto = <?= json_encode($foto) ?>;

	$.each(foto, function( index, value ) {
		for (var i = 0; i <= value.Evidence; i++) {
			$('#select_tipe_'+index+'_'+i).select2({
				placeholder: 'Pilih Tipe Perangkat',
				width: '100%'
			});

			$('#select_stts_'+index+'_'+i).select2({
				placeholder: 'Pilih Status Perangkat',
				width: '100%'
			});

			$('#select_spbu_'+index+'_'+i).select2({
				width: '100%',
				placeholder: "Pilih ID SPBU",
				minimumInputLength: 3,
				allowClear: true,
				ajax: {
				url: "/input/spbu_id/search",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
				}
				}
			}).on("select2:open", function() {
				$(".select2-search__field").val('');
				$(".select2-search__field").attr('type', 'text');
				// $(".select2-search__field").attr('style', 'width: 10em;');
				$(".select2-search__field").inputmask({
					mask: '99-9{6,8}',
					placeholder: ''
				});
			}).on("select2:close", function() {
				$(".select2-search__field").inputmask('remove');
				$(".select2-search__field").val(null);
			});

			$('#select_stts_'+index+'_'+i).on('select2:select', function(){
				if($(this).val() == 'Relokasi'){
					$(this).parent().next().css({'display': 'block'});
				}else{
					$(this).parent().next().css({'display': 'none'});
					$(this).parent().next().find('select').val('').change()
				}
			});
		}
	});

	$('input[type=file]').change(function() {
		var inputEl = this;
		if (inputEl.files && inputEl.files[0]) {
			$(inputEl).parent().find('input[type=text]').val(1);
			var reader = new FileReader();
			reader.onload = function(e) {
				$(inputEl).parent().find('img').attr('src', e.target.result);
			}
			reader.readAsDataURL(inputEl.files[0]);
		}
	});

	$('.input-photos').on('click', 'button', function() {
		$(this).parent().find('input[type=file]').click();
	});

	$("#validation-form").on('submit', function() {
		$(this).parsley().validate();

		if ($(this).parsley().isValid()) {
			$('.btn_sbmt').attr('disabled');
		}
	});
});
</script>
@endsection
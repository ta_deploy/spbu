@extends('layout')
@section('title', 'List '.$data->nama)
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha512-3kAToXGLroNvC/4WUnxTIPnfsiaMlCn0blp0pl6bmR9X6ibIiBZAi9wXmvpmg1cTyd2CMrxnMxqj7D12Gn5rlw==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pivottable/2.23.0/pivot.min.css" integrity="sha256-nn3M6N8S2BjPirPhvCF61ZCcgcppdLtnaNOLhiwro7E=" crossorigin="anonymous" />
<style type="text/css">
    .form-group.footable-filtering-search .input-group-btn {
        display: none;
    }
</style>
@section('content')
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <header>
                    <h4>
                        List <span class="fw-semi-bold">{{$data->nama}}</span> Dengan Total <b>{{ $data->jumlah }}</b>
                    </h4>
                </header>
                <div class="body">
                    <table class="table dbs table-striped table-bordered" data-sorting="true" data-filtering="true">
                        <thead>
                            <tr>
                                <th>No</th>
                                @foreach($data->head as $d)
                                <th>{{ $d }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($data->data as $raw_d)
                            <tr>
                                <td>{{ ++$no }}</td>
                                @foreach($raw_d as $d)
                                <td>{{ $d }}</td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha512-aVkYzM2YOmzQjeGEWEU35q7PkozW0vYwEXYi0Ko06oVC4NdNzALflDEyqMB5/wB4wH50DmizI1nLDxBE6swF3g==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pivottable/2.23.0/pivot.min.js" integrity="sha256-/btBGbvOvx2h/NcXVS+JPFhnoUGbZXDX0O2v6AaABLU=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function(){
        $('.dbs').footable({
            'paging': {
                'enabled': true,
                'size': 10
            }
        });
    })
</script>
@endsection
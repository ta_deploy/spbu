@extends('layout')
@section('title', 'Matrix')
@section('style')
<link rel="stylesheet" href="/bower_component/jPages-master/css/animate.css">
<link rel="stylesheet" href="/bower_component/jPages-master/css/jPages.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style type="text/css">
	.holder {
		position: relative;
		left: 50%;
		display: inline-block;
	}

	.holder a, .holder span {
		color: black;
		
		font-size: 13px;
		padding: 8px 16px;
		text-decoration: none;
		transition: background-color .3s;
	}

	.holder a.jp-current {
		background-color: #3c4452;
		color: white;
		border-radius: 5px;
	}

	.holder a {
		background-color: #303641;
		color: white;
		border-radius: 5px;
	}

	.holder a:hover {
		background-color: #303641;
		border-radius: 5px;
	}

	.holder a:first-child {
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
	}

	.holder a:last-child {
		border-top-right-radius: 5px;
	}

	.daterangepicker  {
		background-color: #414e60;
	}

	.daterangepicker .month, .daterange {
		color: black;
	}

	.daterange {
		width: 100%;
	}

	.table-condensed{
		background-color: #5f666f;
	}

</style>
@endsection
@section('content')
<div class="body">
	<div class="row">
		<div class="col-md-12">
			<div class="input-group date">
				<input type="text" name="daterange" class="form-control daterange" readonly value="{{ date("Y/m/d", strtotime("first day of this month")) }} - {{ date("Y/m/d", strtotime("last day of this month")) }}">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
				</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<fieldset>
				<legend class="section">Grafik Witel</legend>
				<p><a type="button" class="btn btn-info btn-sm download_witel"><i class="material-icons">
				cloud_download</i>Unduh Excel</a></p>
				<canvas id="bar_chart"></canvas>
			</fieldset>
		</div>
		<div class="col-md-6">
			<fieldset>
				<legend class="section">Grafik Teknisi</legend>
				<canvas id="pie_chart"></canvas>
			</fieldset>
		</div>	
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_component/jPages-master/js/jPages.js"></script>
<script src="/bower_component/moment/moment.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(function(){
		var value = $('input[name="daterange"]').val(),
		date    = value.split('-'),
		slice1  = date[0].split("/"),
		month1_ = slice1[0]+'-'+slice1[1]+'-'+slice1[2],
		month1_ = month1_.trim(),
		slice2 = date[1].split("/"),
		month2_ = slice2[0]+'-'+slice2[1]+'-'+slice2[2]+' 23:59:59',
		month2_ = month2_.trim();

		var barGraph;
		function bar_chart(b1, b2){
			var ctx =$('#bar_chart');
			$.ajax({
				url:"/grab/kenaikan/graph",
				type:"GET",
				data: {
					month1 : b1,
					month2 : b2
				},
				datatype: 'json',
				success: function(data) {
					var dynamicColors = function() {
						var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ", 0.5)";
					};
					var label = [],
					count = [],
					color_my = [];
					$.each( data, function( key, val ) {
						label.push(val.witel);
						count.push(val.jml);
						color_my.push(dynamicColors());
					});
					var chartdata = {
						labels: label,
						datasets : [
						{
							label: 'Data Per Witel Bulan',
							fill: false,
							lineTension: 0.1,
							data: count,
							backgroundColor: color_my,
							borderColor: color_my ,
							borderWidth: 1
						}
						]
					};
					barGraph = Chart.Bar(ctx, {
						data: chartdata,
						options: {
							legend: {
								display: false
							},
							tooltips: {
								mode: 'index',
								intersect: false,
								callbacks: {
									label: function(tooltipItem, data) {
										var label = data.labels[tooltipItem.index];
										var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										return label+': '+val;
									}


								}
							},
							scales: {
								yAxes: [{
									ticks: {
										fontColor: "black",
										beginAtZero: true
									}
								}],
								xAxes: [{
									ticks: {
										fontColor: "black",
										beginAtZero: true
									}
								}]
							}
						}
					});
					var canvas = document.getElementById('bar_chart');
					canvas.onclick = function(evt) {
						var activePoint = barGraph.getElementAtEvent(evt)[0];
						var data = activePoint._chart.config.data;
						var datasetIndex = activePoint._datasetIndex;
						var label = data.labels[activePoint._index];
						var value = data.datasets[datasetIndex].data[activePoint._index];
					};

					$('.download_witel').on('click', function(e){
						document.location.assign('/excel/download/'+b1+'/'+b2)
						
					});
				},
				error: function(data) {
					/*console.log(data);*/
				}
			});
		}
		bar_chart(month1_, month2_);

		var pieGraph;
		function line_chart(b1, b2){
			var ctx =$('#pie_chart');
			$.ajax({
				url:"/grab/kerajinan/graph",
				type:"GET",
				data: {
					month1 : b1,
					month2 : b2
				},
				datatype: 'json',
				success: function(data) {
					var dynamicColors = function() {
						var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ", 0.5)";
					};
					var label = [],
					count = [],
					color_my = [];
					$.each( data, function( key, val ) {
						label.push(val.teknisi);
						count.push(val.jml);
						color_my.push(dynamicColors());
					});
					var chartdata = {
						labels: label,
						datasets : [
						{
							label: 'Data Per Witel Bulan',
							data: count,
							backgroundColor: color_my,
							borderColor: color_my ,
							borderWidth: 1
						}
						]
					};
					pieGraph = new Chart(ctx, {
						type: 'pie',
						data: chartdata,
						options: {
							legend: {
								position: 'right',
								labels: {
									fontColor: "black",
								}
							},
							tooltips: {
								mode: 'index',
							},
							responsive: true
						}
					});
				},
				error: function(data) {
					/*console.log(data);*/
				}
			});
		}

		line_chart(month1_, month2_);

		$('input[name="daterange"]').daterangepicker({
			opens: 'left',
			"locale": {
				"format": "YYYY/MM/DD",
				"separator": " - ",
			}
		}, function(start, end){
			var month1 = start.format('YYYY-MM-DD'),
			month2 = end.format('YYYY-MM-DD')+" 23:59:59";
			console.log(month1)
			console.log(month2)
			$.ajax({
				url:"/grab/kenaikan/graph",
				type:"GET",
				data: {
					month1 : month1,
					month2 : month2
				},
				dataType:"json",
				success: function(data) {
					console.log(data)
					var dynamicColors = function() {
						var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ", 0.5)";
					}
					var color_my = [];
					console.log(barGraph.data)
					barGraph.data.labels = [];
					barGraph.data.datasets[0].data = [];
					for(var i in data){
						color_my.push(dynamicColors());
						barGraph.data.datasets[0].data[i] = data[i].jml;
						barGraph.data.labels[i]=data[i].witel;
						barGraph.data.datasets[0].backgroundColor=color_my;
						barGraph.data.datasets[0].borderColor=color_my;
					}
					barGraph.update();
				}
			});

			$.ajax({
				url:"/grab/kerajinan/graph",
				type:"GET",
				data: {
					month1 : month1,
					month2 : month2
				},
				dataType:"json",
				success: function(data) {
					console.log(data)
					var dynamicColors = function() {
						var r = Math.floor(Math.random() * 255);
						var g = Math.floor(Math.random() * 255);
						var b = Math.floor(Math.random() * 255);
						return "rgb(" + r + "," + g + "," + b + ", 0.5)";
					}
					var color_my = [];
					pieGraph.data.labels = [];
					pieGraph.data.datasets[0].data = [];
					for(var i in data){
						color_my.push(dynamicColors());
						pieGraph.data.datasets[0].data[i] = data[i].jml;
						pieGraph.data.labels[i]=data[i].teknisi;
						pieGraph.data.datasets[0].backgroundColor=color_my;
						pieGraph.data.datasets[0].borderColor=color_my;
					}
					pieGraph.update();
				}
			});
		});

		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="daterange"]').click();
		});
	});
</script>
@endsection

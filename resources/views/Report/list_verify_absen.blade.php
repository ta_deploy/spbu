@extends('layout')
@section('title', 'List Naker')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha512-3kAToXGLroNvC/4WUnxTIPnfsiaMlCn0blp0pl6bmR9X6ibIiBZAi9wXmvpmg1cTyd2CMrxnMxqj7D12Gn5rlw==" crossorigin="anonymous" />
<style type="text/css">
    .form-group.footable-filtering-search .input-group-btn {
        display: none;
    }
</style>
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						List <span class="fw-semi-bold">Naker Absen</span>
					</h4>
				</header>
				<div class="body">
					<table class="table dbs table-striped table-bordered" data-sorting="true" data-filtering="true">
						<thead>
							<tr>
								<th class="hidden-xs">#</th>
								<th>Naker</th>
								<th>NIK</th>
								<th data-sorted="true" data-direction="DESC">Tanggal Terakhir Absen</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php $no = 0; @endphp
							@foreach($data as $d)
							<tr>
								<td>{{ ++$no }}</td>
								<td>{{ $d['id_user'] }}</td>
								<td>{{ $d['nama'] }}</td>
								<td>{{ $d['spbu_verify'] }}</td>
								<td>
									@if($d['spbu_verify'] == 0)
										<a type="button" href="/verify/absen/{{ $d['id_user'] }}" class="btn btn_edit btn-primary">Verify</a>
										<a type="button" href="/why_reject/{{ $d['id_user'] }}" class="btn btn_edit btn-warning">Reject</a>
									@elseif($d['spbu_verify'] == 1)
										<span class="label label-success">Sudah Absen</span>
									@else
									<span class="label label-warning">Belum Absen!</span>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div class="holder"></div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha512-aVkYzM2YOmzQjeGEWEU35q7PkozW0vYwEXYi0Ko06oVC4NdNzALflDEyqMB5/wB4wH50DmizI1nLDxBE6swF3g==" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(function(){
		$('.table').footable({
            'paging': {
                'enabled': true,
                'size': 10
            },
        });
	})
</script>
@endsection
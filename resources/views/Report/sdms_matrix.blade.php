@extends('layout')
@section('title', 'Matrix SDMS OFF')
@section('style')
<link rel="stylesheet" href="/bower_component/jPages-master/css/animate.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.bootstrap.min.css" integrity="sha512-3kAToXGLroNvC/4WUnxTIPnfsiaMlCn0blp0pl6bmR9X6ibIiBZAi9wXmvpmg1cTyd2CMrxnMxqj7D12Gn5rlw==" crossorigin="anonymous" />
<link href="/bower_component/toast-master/css/jquery.toast.css" rel="stylesheet">
<style type="text/css">
    .jq-icon-info {
        border-radius: 25px;
    }
</style>
@section('content')
<div class="modal fade" id="modal_data">
    <div class="modal-dialog" style="width: auto; margin: 20px;">
        <div class="modal-content" style="background-color: #6e767e;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle" style="color: white;">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body_id_sec">
                <table class="table_sdms table table-striped table-bordered"></table>
            </div>
        </div>
    </div>
</div>
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend class="section">Grafik SDMS OFF</legend>
                <canvas id="sdms_chart"></canvas>
            </fieldset>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.6/footable.min.js" integrity="sha512-aVkYzM2YOmzQjeGEWEU35q7PkozW0vYwEXYi0Ko06oVC4NdNzALflDEyqMB5/wB4wH50DmizI1nLDxBE6swF3g==" crossorigin="anonymous"></script>
<script src="/bower_component/toast-master/js/jquery.toast.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function(){
        var ctx =$('#sdms_chart'),
        data = <?= json_encode($chart) ?>;
        console.log(data)
        var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ", 0.5)";
        };
        var label = [],
        count = [],
        id = [],
        color_my = [];
        $.each( data, function( key, val ) {
            label.push(val.nama);
            id.push(val.id);
            count.push(val.jumlah);
            color_my.push(dynamicColors());
        });
        var chartdata = {
            labels: label,
            datasets : [
            {
                label: 'Data Off',
                fill: false,
                lineTension: 0.1,
                data: count,
                backgroundColor: color_my,
                borderColor: color_my ,
                borderWidth: 1,
                id: id
            }
            ]
        };
        barGraph = Chart.Bar(ctx, {
            data: chartdata,
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var label = data.labels[tooltipItem.index];
                            var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            return label+': '+val;
                        }


                    }
                },
                hover: {
                    onHover: function(e) {
                        var point = this.getElementAtEvent(e);
                        if (point.length) e.target.style.cursor = 'pointer';
                        else e.target.style.cursor = 'default';
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "black",
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: "black",
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        var canvas = document.getElementById('sdms_chart');
        canvas.onclick = function(evt) {
            var activePoint = barGraph.getElementAtEvent(evt)[0];
            var data = activePoint._chart.config.data;
            var datasetIndex = activePoint._datasetIndex;
            var label = data.labels[activePoint._index];
            var value = data.datasets[datasetIndex].data[activePoint._index];
            var id = data.datasets[datasetIndex].id[activePoint._index];
            window.open('/sdms_matrix/data/'+id, '_blank');
        };
    })
</script>
@endsection
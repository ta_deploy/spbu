@extends('layout')
@section('title', 'Dashboard Perangkat Rusak!')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.daterangepicker  {
		background-color: #414e60;
	}

	.select2-results__option, select{
        color: black;
    }

	.select2-selection__choice, select{
        color: black;
  }

	.daterangepicker .month, .daterange {
		color: black;
	}

	.daterangepicker .calendar-table {
		background-color: #4a4d50;
	}

	.daterange {
		width: 100%;
	}

	.modal-xl{
		width: 100%;
		max-width:1300px;
	}

</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="body">
	<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content" style="background-color: #6e767e;">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle" style="color: white;">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body body_id_sec">
				</div>
				<div class="modal-footer">
					<div class="form-group row">
						<div class="col-md-6 btn_up">
							<a type="button" href="#" class="btn btn-block btn_edit btn-info brtn_up">Update Pekerjaan</a>
						</div>
						<div class="col-md-6 btn_delete">
							<a type="button" href="#" class="btn btn-block btn_edit btn-warning brtn_del">Hapus Pekerjaan</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-large" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Tabel Data</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-bordered modalkuh" id="gdrive" style="width: 100%;">
						<thead>
							<tr class="headerr">

							</tr>
						</thead>
						<tbody id="data_table">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						Filter <span class="fw-semi-bold">Pengerjaan</span>
					</h4>
				</header>
				<div class="body">
					<form method="GET">
						<div class="form-group row">
							<div class="col-sm-12">
								<div class="input-group date">
									<input type="text" name="daterange" class="form-control daterange" readonly value=" {{ @$prev_data['daterange'] }}">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-4">
								<select id="lokasi" name="lokasi[]" class="dashboard_menu selectpicker" data-style="btn-default" data-width="auto" multiple>
									<option value="Ditinggal di SPBU" {{ @in_array('Ditinggal di SPBU', $prev_data['lokasi'])? 'selected' : '' }} {{ empty($prev_data['lokasi']) ? 'selected' : '' }}>Ditinggal di SPBU</option>
									<option value="Diantar ke CCAN" {{ @in_array('Diantar ke CCAN', $prev_data['lokasi'])  ? 'selected' : '' }} {{ empty($prev_data['lokasi']) ? 'selected' : '' }}>Diantar ke CCAN</option>
									<option value="Team IM Balikpapan Reg 6" {{ @in_array('Team IM Balikpapan Reg 6', $prev_data['lokasi'])? 'selected' : '' }} {{ empty($prev_data['lokasi']) ? 'selected' : '' }}>Team IM Balikpapan Reg 6</option>
									<option value="Teknisi Territory" {{ @in_array('Teknisi Territory', $prev_data['lokasi'])? 'selected' : '' }} {{ empty($prev_data['lokasi']) ? 'selected' : '' }}>Teknisi Territory</option>
								</select>
							</div>
							<div class="col-sm-4">
								<select id="change_part" name="change_part[]" class="dashboard_menu selectpicker" data-style="btn-default" data-width="auto" multiple>
									<option value="NOK/Belum di ganti" {{ @in_array('NOK/Belum di ganti', $prev_data['change_part'])? 'selected' : '' }} {{ empty($prev_data['change_part']) ? 'selected' : '' }}>NOK/Belum di ganti</option>
									<option value="Dari SDA/Logistik SPBU" {{ @in_array('Dari SDA/Logistik SPBU', $prev_data['change_part'])? 'selected' : '' }} {{ empty($prev_data['change_part']) ? 'selected' : '' }}>Dari SDA/Logistik SPBU</option>
									<option value="Dari Relokasi SPBU" {{ @in_array('Dari Relokasi SPBU', $prev_data['change_part'])? 'selected' : '' }} {{ empty($prev_data['change_part']) ? 'selected' : '' }}>Dari Relokasi SPBU</option>
								</select>
							</div>
							<div class="col-sm-4">
								<select id="jenis" name="jenis[]" class="dashboard_menu selectpicker" data-style="btn-default" data-width="auto" multiple>
									<option value="0" {{ @in_array('0', $prev_data['jenis'])? 'selected' : '' }} {{ empty($prev_data['jenis']) ? 'selected' : '' }}>Input Standar</option>
									<option value="1" {{ @in_array('1', $prev_data['jenis'])? 'selected' : '' }} {{ empty($prev_data['jenis']) ? 'selected' : '' }}>Input Dari SDA/Logistik SPBU</option>
								</select>
							</div>
						</div>
						<button type="submit" class="btn btn-primary mb-2">Cari Data</button>
					</form>
				</div>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						Dashboard <span class="fw-semi-bold">Perangkat Rusak</span>
					</h4>
				</header>
				<p class="no-margin">
					<span class="badge badge-success">Sudah Selesai </span>
					<span class="badge badge-primary">Kurang Dari 10 Hari</span>
					<span class="badge badge-info">Kurang Dari 20 Hari</span>
					<span class="badge badge-warning">Kurang Dari 30 Hari</span>
					<span class="badge badge-danger">Lebih Dari 30 Hari</span>
				</p>
				<div class="body">
					<table class="table dbs table-bordered">
						<thead>
							<tr>
								<th class="hidden-xs">#</th>
								<th>Datel</th>
								<th>Sektor</th>
								<th>Nama Teknisi</th>
							</tr>
						</thead>
						<tbody>
							@php
								$nomor = 0;
							@endphp
							@foreach ($table_kerja as $no => $raw_tk)
							@php
								$rp = true;
							@endphp
								@foreach ($raw_tk as $tk)
								<tr>
									@if ($rp == true)
									<td rowspan="{{ count($raw_tk) }}">{{ ++$nomor }}</td>
									<td rowspan="{{ count($raw_tk) }}">{{ $no }}</td>
									@php
										$rp = false;
									@endphp
								@endif
									<td><b>{{ $tk->nama }}</b> ({{ $tk->teknisi }})</td>
									<td>
										<p>
											@foreach ($tk->data as $detail_data )
											@php
												if($detail_data->status == 'closed'){
													$label = 'label-success';
												}else if($detail_data->status == 'belum'){
													if($detail_data->hari < 10){
														$label = 'label-primary';
													}else if($detail_data->hari > 10 && $detail_data->hari < 20 ){
														$label = 'label-info';
													}else if($detail_data->hari > 20 && $detail_data->hari < 30 ){
														$label = 'label-warning';
													}else if($detail_data->hari >= 30 ){
														$label = 'label-danger';
													}
												}
											@endphp
											<a>
											<span data-id='{{ $detail_data->id_dis }}' class='label_sel_opt label {{ @$label }}' style='cursor: pointer; margin-left: 0.3EM; margin-bottom: 0.3EM;' data-toggle='modal' data-target='#exampleModalLong'><b style="color: #000000">{{ $detail_data->jenis }}: </b>{{ $detail_data->id_spbu }} </span>
											</a>
											@endforeach
										</p>
									</td>
								</tr>
								@endforeach
							@endforeach
						</tbody>
					</table>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_component/moment/moment.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script type="text/javascript">
	$(function(){
		$('#lokasi').select2({
			width: '100%',
			placeholder: 'Pilih Lokasi!'
		});

		$('.label_sel_opt').each(function() {
			$(this).on("click", function(){
				var data_id = $(this).data('id');
				$.ajax({
					url:"/get/table/get_table_broken",
					type:"GET",
					data: {
						id : data_id
					},
					dataType: 'html',
					success: (function(data){
						$(".body_id_sec").html(data);
					}),
				})
			});
		});

		$('#change_part').select2({
			width: '100%',
			placeholder: 'Pilih Pergantian Perangkat!'
		});

		$('#jenis').select2({
			width: '100%',
			placeholder: 'Pilih Jenis Order!'
		});

		$('[data-toggle="popover"]').popover();

		$('#status').select2({
			width: '100%',
			placeholder: 'Masukkan statusnya!'
		});

		$('input[name="daterange"]').daterangepicker({
			opens: 'left',
			"locale": {
				"format": "YYYY/MM/DD",
				"separator": " - ",
			}
		});

		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="daterange"]').click();
		});
	});
</script>
@endsection

@extends('layout')
@section('title', 'SDMS Performa Witel')
@section('style')
<style type="text/css">
    .th-green{
        background-color: #52c41a;
    }
</style>
@endsection
@section('content')
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <form id="formlistG" name="formlistG" method="get">
                    <div class="input-group date">
                        <input type="text" name="datepicker" class="form-control datepicker">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i> Cari</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <div class="body">
                    <table class="table dbs table-bordered">
                        <thead>
                            <tr>
                                @foreach ($table->head as $t)
                                <th>{{ $t }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody id="data_table">
                            @foreach ($table->data as $d)
                            <tr class="{{ $d[6]  }}">
                                @for ($i=0; $i <= 5; $i++)
                                <td>{{ $d[$i] }}</td>
                                @endfor
                            </tr>
                            @endforeach
                        </tbody>
                    </table>    
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_component/moment/moment.js"></script>
<script src="/bower_component/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
@endsection

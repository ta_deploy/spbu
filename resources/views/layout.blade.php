<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    <link href="/css/application.css" rel="stylesheet">
    <link rel="shortcut icon" href="/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <link href="/css/line_css/linea.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield('headerS')
    @yield('style')
    <style type="text/css">
        .material-icons{
            position: relative;
            top: 2.4px;
            font-size: 17px;
            margin-right: 6px;
        }

        .material-icons.menu_ta{
            color: #9da6af;
        }

        @media (max-width: 767px) {
            .title{
                display: none;
            }
        }

    </style>
</head>
<body>
    <div class="logo">
        <h4>
            <a href="/home">
                <div style="display: inline-block;width: 100%;text-align: center;">
                    <img src="{{ asset('/img/favicon.png/')}}" alt="home" class="dark-logo" style="position:relative;left:-10px;width: 50px; height: 50px; border-radius: 50%;" /><strong class="title">SPBU TOMMAN</strong>
                </div>
            </a>
        </h4>
    </div>
    <nav id="sidebar" class="sidebar nav-collapse collapse">
        <ul id="side-nav" class="side-nav">
            <li class="panel ">
                <a class="accordion-toggle collapsed" data-toggle="collapse"
                data-parent="#side-nav" href="#dashboard-collapse"><i class="material-icons menu_ta">storage</i> <span class="name">Dashboard</span></a>
                <ul id="dashboard-collapse" class="panel-collapse collapse ">
                    @if(in_array(session('auth')->spbu_level, [1, 4]))
                    <li class=""><a href="/home">Pengerjaan</a></li>
                    @endif
                    @if(session('auth')->spbu_level == 2)
                        <li class=""><a href="/home">Pekerjaan</a></li>
                    @endif
                    <li class=""><a href="/dashboard/broken_part">Perangkat Rusak</a></li>
                    <li class=""><a href="/list/Inventory">Inventory</a></li>
                </ul>
            </li>
            @if(in_array(session('auth')->spbu_level, [1, 2, 4]) || session('auth')->id_user == 18940469)
            <li class="panel">
                <a class="accordion-toggle collapsed" data-toggle="collapse"
                data-parent="#side-nav" href="#tiket-collapse"><i class="material-icons menu_ta">insert_chart_outlined</i><span class="name">Input</span></a>
                <ul id="tiket-collapse" class="panel-collapse collapse ">
                    <li class=""><a href="/Spbu/perangkat_rusak/input">Perangkat Rusak</a></li>
                    <li class=""><a href="/Spbu/Input">SPBU</a></li>
                    <li class=""><a href="/repairing/Input">Reparasi Alat</a></li>
                </ul>
            </li>
            @endif
            <li class="panel">
                <a class="accordion-toggle collapsed" data-toggle="collapse"
                data-parent="#side-nav" href="#report-collapse"><i class="material-icons menu_ta">insert_chart_outlined</i><span class="name">Report</span></a>
                <ul id="report-collapse" class="panel-collapse collapse ">
                    @if(in_array(session('auth')->spbu_level, [1, 4]) || session('auth')->id_user == 18940469)
                    <li class=""><a href="/matrix">Matrix</a></li>
                    @endif
                    <li class=""><a href="/list/download">Download</a></li>
                </ul>
            </li>
            @if(in_array(session('auth')->spbu_level, [1, 4]) || session('auth')->id_user == 18940469)
            <li class="panel">
                <a class="accordion-toggle collapsed" data-toggle="collapse"
                data-parent="#side-nav" href="#list-collapse"><i class="material-icons menu_ta">amp_stories</i><span class="name">List</span></a>
                <ul id="list-collapse" class="panel-collapse collapse ">
                    <li class=""><a href="/list/alamat">Address</a></li>
                    <li class=""><a href="/list/absen">Absen</a></li>
                    <li class=""><a href="/list/teknisi">Teknisi</a></li>
                </ul>
            </li>
            <li class="panel">
                <a class="accordion-toggle collapsed" data-toggle="collapse"
                data-parent="#side-nav" href="#sdms-collapse"><i class="fa fa-tree"></i> <span class="name">Sdms</span></a>
                <ul id="sdms-collapse" class="panel-collapse collapse ">
                    <li class=""><a href="/sdms/dsh">Sdms OFF</a></li>
                    <li class=""><a href="/sdms/table">Performance Witel</a></li>
                    <li class=""><a href="/sdms/table/detail">Detail Performance Witel</a></li>
                </ul>
            </li>
            @endif
            @if(in_array(session('auth')->spbu_level, [2, 4]))
            <li class="">
                <a href="/sdms/teknisi/detail"><i class="material-icons menu_ta">developer_board</i><span class="name">Cek Pekerjaan SDMS OFF</span></a>
            </li>
            @endif
            <li class="panel">
                <a href="https://perwira.tomman.app/input_potensi_b/{{ session('auth')->id_karyawan }}"><i class="fa fa-life-buoy"></i>Lapor Potensi Bahaya</a>
            </li>
            <li class="visible-xs">
                <a href="/logout"><i class="fa fa-sign-out"></i> <span class="name">Sign Out</span></a>
            </li>
        </ul>
    </nav>
    <div class="wrap">
        <header class="page-header">
            <div class="navbar">
                <ul class="nav navbar-nav navbar-right pull-right">
                    <li class="hidden-xs dropdown">

                        <a href="#" title="Account" id="account"
                           class="dropdown-toggle"
                           data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i>
                        </a>
                        <ul id="account-menu" class="dropdown-menu account" role="menu">
                            <li role="presentation" class="account-picture">
                                <img src="/img/15.png" alt="">
                                {{ session('auth')->id_user }}
                            </li>
                            <li role="presentation">
                                <a href="#" class="link">
                                    <i class="fa fa-user"></i>
                                    {{ session('auth')->nama }}
                                </a>
                            </li>
                            {{--<li role="presentation">
                                <a href="component_calendar.html" class="link">
                                    <i class="fa fa-calendar"></i>
                                    Calendar
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#" class="link">
                                    <i class="fa fa-inbox"></i>
                                    Inbox
                                </a>
                            </li> --}}
                        </ul>
                    </li>
                    <li class="visible-xs">
                        <a href="#"
                        class="btn-navbar"
                        data-toggle="collapse"
                        data-target=".sidebar"
                        title="">
                        <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <li class="divider"></li>
                <li class="hidden-xs"><a href="/logout"><i class="glyphicon glyphicon-off"></i></a></li>
            </ul>
                <!-- <form id="search-form" class="navbar-form pull-right" role="search">
                    <input type="search" class="form-control search-query" placeholder="Cari">
                </form> -->
            </div>
        </header>
        <div class="content container">
         @yield('content')
        <footer class="content-footer" style="color: black;">
            TOMMAN
        </footer>
        </div>
        <div class="loader-wrap hiding hide">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>
</div>
<!-- common libraries. required for every page-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/bower_component/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="/bower_component/underscore/underscore.js"></script>

<!-- common application js -->
<script src="/js/app.js"></script>
<script src="/js/settings.js"></script>
<!-- page specific scripts -->
<!-- page libs -->
<script src="/bower_component/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="/bower_component/parsleyjs/dist/parsley.min.js"></script>
<!-- page application js -->
@yield('footerS')
<script type="text/javascript">
    function setNavigation() {
        var path = window.location.pathname;
        path = path.replace(/\/$/, "");
        path = decodeURIComponent(path);
        $(".side-nav a").each(function () {
            var href = $(this).attr('href');
            if (path.substring(0, href.length) === href) {
                $('.side-nav li').removeClass("active");
                $(this).closest('li').addClass('active');
                var find = $(this).parent().closest('ul').hasClass('panel-collapse')
                if(find){
                    $(this).parent().parent().parent().addClass('active')
                    $(this).parent().parent().prev().click();
                }else{
                    $('.accordion-toggle').addClass('collapsed');
                }
            }
        });
    }

    $(function () {
        setNavigation();
    });

</script>
</body>
</html>